# IF 4073 Interpretasi dan Pengolahan Citra

## Tugas 1 : Manipulasi Histogram pada Gambar Digital

Dikerjakan Oleh Kelompok 5:

|Nama | NIM |
|---|---|
|Muhammad Diaztanto Haryaputra | 13514002 |
|Richard Wellianto | 13514051 |
|Muhammad Umar Fariz Tumbuan | 13515050 |

## Pendahuluan
Repo ini berisi source code untuk aplikasi Android yang memiliki beberapa fitur sebagai berikut: 

Tugas 1 dan 2:

- *Histogram Equalization*
- *Linear Contrast Stretching*

Tugas 3:

- *Histogram Specification Matching*

Di dalam repo ini juga sudah disediakan beberapa foto sampel yang dapat digunakan untuk mengetes hasil implementasi, namun aplikasi juga memiliki fitur untuk memanipulasi gambar yang diambil menggunakan kamera ponsel. Aplikasi dapat dijalankan pada emulator atau ponsel Android dengan dibuka melalui Android Studio terlebih dahulu.

## Cara Pemakaian Aplikasi

<img src="resources/main_menu_2.png"  width="150">

Gambar di atas adalah halaman pertama dari aplikasi. Untuk memulai, pilih salah satu fitur yang ingin digunakan.

1. *Histogram Equalization*

Kelas implementasi: 

- com.tanto.imagehistogram.HistogramUtils.HistogramEqualizer
- com.tanto.imagehistogram.HistogramUtils.HistrogramStretcher

<img src="resources/start_page.png"  width="150">
<img src="resources/select_photo.png"  width="150">

Gambar di atas adalah halaman untuk melakukan *Histogram Equalization*. Untuk memulai, pilih salah satu item di dropdown "*Select Image*". Untuk mengambil gambar menggunakan kamera, pilih "*Take from camera*."

Catatan: Slider di tengah halaman belum memiliki implementasi fungsi

<img src="resources/compare_picture.png"  width="150">
<img src="resources/select_method.png"  width="150">

Hasil manipulasi dari gambar yang dipilih akan ditampilkan berdampingan agar observasi perubahan lebih mudah dilakukan. Gambar di sebelah kiri adalah gambar sebelum manipulasi dan gambar di kanan adalah gambar setelah manipulasi. Aplikasi dapat menerapkan 3 jenis metode manipulasi histogram. Metode yang digunakan dapat dipilih dengan memilih item pada menu "*Method*". (Nama metode disingkat agar tidak memenuhi layar ponsel).

- Eq-Avg : *Cumulative Histogram Equalization* dengan menggunakan rata-rata frekuensi dari channel RGB
- Eq-RGB : *Cumulative Histogram Equalization* yang diterapkan pada tiap channel RGB secara individual
- Linear s. : *Linear Contrast Stretching*

<img src="resources/compare_graphs.png"  width="150">
<img src="resources/select_channel.png"  width="150">
<img src="resources/green_channel.png"  width="150">

Histogram sebelum dan sesudah manipulasi ditampilkan di bagian bawah gambar. Histogram bagian atas adalah histogram sebelum manipulasi, dan yang dibawah adalah setelah. Secara *default* histogram yang ditampilkan adalah histogram *grayscale* (berkode *K*). Histogram untuk channel lain dapat dipilih menggunakan dropdown yang ada di bagian kiri halaman.

2. *Histogram Matching*

Kelas implementasi:

- com.tanto.imagehistogram.HistogramUtils.HistogramMatcher

<img src="resources/hist_matching_input.png"  width="150">

Halaman di atas adalah halaman tempat memasukkan input gambar dan target histogram yang diinginkan untuk setiap channel. Tiap channel memiliki tiga titik intensitas (0, 127, dan 255) yang bisa diatur nilai kemunculannya. Dari ketiga titik tersebut akan dibuat dua garis linier yang akan merepresentasikan target histogram yang diinginkan. Setelah selesai mengatur target histogram klik tombol **apply**.

<img src="resources/hist_matching_result.png"  width="150">

Aplikasi akan berpindah ke halaman hasil penerapan *Histogram Matching*. Pada ilustrasi di atas, nilai intensitas channel warna merah dilemahkan sedangkan hijau dan biru dikuatkan. Gambar yang dihasilkan memiliki warna cyan yang merupakan ekspektasi dari kombinasi warna yang dihasilkan. Untuk kembali ke halaman input, tekan tombol back pada device.

## Penjelasan Metode Manipulasi Histogram

### 1. *Histogram Equalization* (HE)

Metode HE memanfaatkan fungsi distribusi kumulatif (cdf) untuk memanipulasi histogram dari gambar digital. Setelah mendapatkan histogram untuk frekuensi kemunculan tiap intensitas warna yang mungkin, cdf dari histogram tersebut ditentukan. Metode HE bertujuan untuk membuat histogram baru yang memiliki cdf dengan tren linier. Dengan begitu, harapannya histogram dapat ter-normalisasi lebih merata.

Rumus untuk memetakan nilai intensitas sebuah pixel adalah sebagai berikut:

<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/49e7d6c2a0e08b6c363dc7df0c4acd6629d8e150"  width="350">

di mana:

- *v* adalah nilai intensitas warna pixel
- *cdf(v)* adalah distribusi kumulatif untuk nilai *v*
- *cdfmin* adalah nilai kumulatif terkecil
- *M x N* adalah jumlah pixel dari gambar (panjang x lebar)
- *L* adalah batas maksimum nilai *v* (dalam kasus ini *L* = 256)

*Sumber: https://en.wikipedia.org/wiki/Histogram_equalization*

Rumus di atas dapat langsung diterapkan ke sebuah gambar *graysacle* yang hanya memiliki satu channel warna. Untuk gambar RGB, ada beberapa pendekatan yang dapat digunakan (aplikasi menggunakan pendekatan 1 dan 2):

1. Menerapkan HE pada tiap channel RGB
2. Menerapkan HE mengunnakan histogram rata-rata dari dari tiap channel
3. Mengkonversi histogram RGB ke *color-space* (contoh: HSL/HSV, YCbCr) yang berbeda dan menerapkan HE pada hasil konversi

Metode HE memiliki kelebihan yaitu cdf hasil manipulasi dapat dimanipulasi lebih lanjut karena bersifat non-linier. Kekurangan dari HE adalah cenderung menghasilkan efek yang tidak realistis pada gambar.

### 2. *Linear Contrast Stretching* (LSE) / *Normalization*

Metode LSE bertujuan untuk memetakan nilai intensitas pixel sedemikian rupa sehingga gambar yang dihasilkan gambar yang memanfaatkan sebanyak mungkin nilai intensitas yang mungkin di antara batas bawah dan batas atas warna. Berbeda dengan metode HE, pemetaan yang dihasilkan LSE bersifat diskrit. Hal ini memiliki dampak positif yaitu gambar dapat dikembalikan menjadi semula. Namun dampak negatifnya adalah gambar tidak dapat dimanipulasi lebih lanjut.

Rumus untuk memetakan pixel ke nilai baru adalah sebagai berikut:

<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/ad23419556331501d554ed0685b13a526d99d446"  width="350">

*Sumber: https://wikimedia.org/api/rest_v1/media/math/render/svg/ad23419556331501d554ed0685b13a526d99d446*

di mana:

- *In* adalah nilai intensitas baru
- *I* adalah nilai intensitas saat ini
- *newMax* adalah batas atas yang diinginkan dari range warna gambar baru (dalam kasus ini 256)
- *newMin* adalah batas bawah yang diinginkan dari range warna gambar baru (dalam kasus ini 0)
- *Max* adalah batas atas dari range warna yang muncul pada gambar semula
- *Min* adalah batas bawah dari range warna yang muncul pada gambar semula

Hanya menggunakan rumus di atas tidak cukup untuk mengimplementasi LSE secara optimal. Ada kemungkinan untuk sebuah kasus di mana terdapat beberapa pixel *outlier* dengan nilai ekstrim berlawanan yang berlawanan dengan mayoritas pixel, sehingga membuat hasil LSE minimum. (**Aplikasi ini belum meng-handle kasus tersebut, tetapi dapat dilihat bahwa implementasi LSE sudah benar dengan menggunakan foto ***"sampel 2*"). Ada beberapa ide untuk meng-handle kasus ini:

1. Menentukan sebuah nilai batas minimum frekuensi sebagai syarat sebuah pixel dimasukkan ke dalam range gambar semula.
2. Menentukan range di sekitar titik puncak histogram

### 3. *Histogram Specification Matching*

Teknik ini mencoba untuk memetakan persebaran nilai kemunculan intensitas pada histogram gambar semula ke histogram lain yang disesuaikan dengan keinginan pengguna. Pertama histogram gambar semula diekualisasi sehingga didapatkan ALU untuk gambar semula berdasarkan fungsi densitas kumulatif. Histogram target kemudian diterapkan dengan hal yang sama untuk mendapatkan ALU target. Nilai intensitas kemudian dipetakan ke nilai intensitas dari histogram target dengan mencari nilai intensitas yang memiliki nilai fungsi distribusi kumulatif pertama yang terdekat.

### 4. *Freeman Chain Code*

Teknik ini digunakan untuk mencari batas-batas objek dalam gambar dengan menelusuri pixel yang termasuk dalam gambar. Antar pixel yang ditelusuri saling berhubungan, membentuk rantai dengan vektor/arah sebagai penghubungnya. Data vektor/arah itulah yang digunakan untuk mengidentifikasi objek dalam gambar tersebut.

## Referensi

http://spatial-analyst.net/ILWIS/htm/ilwisapp/stretch_algorithm.htm

http://myjavacodeworld.blogspot.com/2012/10/how-to-linear-stretch-any.html

https://en.wikipedia.org/wiki/Normalization_(image_processing)

https://en.wikipedia.org/wiki/Histogram_equalization

https://en.wikipedia.org/wiki/Histogram_matching

fourier.eng.hmc.edu/e161/lectures/contrast_transform/node3.html






















