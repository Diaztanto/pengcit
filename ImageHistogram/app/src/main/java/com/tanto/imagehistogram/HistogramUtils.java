//Reference :
//  https://hypjudy.github.io/2017/03/19/dip-histogram-equalization/

package com.tanto.imagehistogram;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

public class HistogramUtils {
    private HistogramUtils(){}

    /* Fungsi untuk mendapatkan data histogram dari gambar
    *  I.S Masukan gambar yang akan dicari data histogramnya
    *  F.S Data Histogram gambar dalam bentuk kelas HistogramData
    */
    public static HistogramData generateBitmapHistogramData(Bitmap bd, int colorBoundaries) {
        int height = bd.getHeight();
        int width = bd.getWidth();

        int[] redPixel = new int[colorBoundaries];
        int[] greenPixel = new int[colorBoundaries];
        int[] bluePixel = new int[colorBoundaries];
        int[] grayPixel = new int[colorBoundaries];

        //Check all pixels
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                int pixel = bd.getPixel(j, i);
                //Get the colors
                int red = Color.red(pixel);
                int green = Color.green(pixel);
                int blue = Color.blue(pixel);
                //Get the intensity
                int gray = getPixelIntensity(pixel);
                //Frequent count up
                redPixel[red]++;
                greenPixel[green]++;
                bluePixel[blue]++;
                grayPixel[gray]++;
            }
        }
        return new HistogramData(redPixel, greenPixel, bluePixel, grayPixel);
    }

    private static int getFirstNonZeroCdfValue(int[] arr) {
        int i = 0;
        while(arr[i] == 0 && i < arr.length) {
            i++;
        }
        return arr[i];
    }

    /* Fungsi untuk mendapatkan fungsi distribusi kumulatif setiap data histogram */
    private static int[] getCumulativeDistributionFunction(int[] histogramData) {
        int[] cdf = new int[histogramData.length];
        int currentSum = 0;

        for (int i = 0; i < histogramData.length; i++) {
            currentSum += histogramData[i];
            cdf[i] = currentSum;
        }
        return cdf;
    }

    /* Fungsi untuk mendapatkan intensitas warna/skala abu-abu sebuah pixel */
    private static int getPixelIntensity(int pixel) {
        return (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3 % 256;
    }

    /* Fungsi untuk mendapatkan batas bawah derajat data histogram yang muncul dalam gambar */
    private static int getLowerBoundaryOf(int[] histogramData) {
        int i = 0;
        while (i < histogramData.length && histogramData[i] == 0) {
            i++;
        }
        return i;
    }

    /* Fungsi untuk mendapatkan batas atas derajat data histogram yang muncul dalam gambar */
    private static int getUpperBoundaryOf(int[] histogramData) {
        int i = histogramData.length - 1;
        while (i > 0 && histogramData[i] == 0) {
            i--;
        }
        return i;
    }

    /* Mencari nilai fungsi linear y = mx + c dengan masukan titik ujung garis
    *  I.S 2 pasang nilai absis dan ordinat ujung garis dan input untuk x, yaitu xInput
    *  F.S nilai y dari fungsi linear y = mx + c dengan x = xInput
    */
    private static float linearFunction(int x1, int y1, int x2, int y2, int xInput) {
        float m = (float) (y2 - y1) / (x2 - x1);
        float c = y1 - m * x1;
        return m * xInput + c;
    }

    // Histogram Equalizer Implementation
    public static class HistogramEqualizer {
        private HistogramEqualizer() {}

        /* Fungsi untuk mendapatkan hasil ekualisasi rata-rata dari gambar
        *  I.S gambar, batasan warna, dan besar ekualisasi dari weightBar
        *  F.S gambar baru hasil ekualisasi rata-rata
        */
        public static Bitmap averageEqualization(Bitmap inputBitmap, int colorBoundaries, float w) {
            int width = inputBitmap.getWidth();
            int height = inputBitmap.getHeight();
            int numOfPixels = width * height;
            Bitmap resultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            int[] newPixels = new int[width * height];

            HistogramData histogramData = generateBitmapHistogramData(inputBitmap, colorBoundaries);
            int[] redHist = histogramData.getRedPixelHistogram();
            int[] greenHist = histogramData.getGreenPixelHistogram();
            int[] blueHist = histogramData.getBluePixelHistogram();
            int[] rgbHist = new int[redHist.length];
            for (int i = 0; i < rgbHist.length; i++) {
                rgbHist[i] = (redHist[i] + blueHist[i] + greenHist[i]) / 3;
            }

            int[] avgAlu = generateAlu(rgbHist, numOfPixels, colorBoundaries, w);
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int pixel = inputBitmap.getPixel(x,y);
                    int oldRed = Color.red(pixel);
                    int oldBlue = Color.blue(pixel);
                    int oldGreen = Color.green(pixel);

                    int newRed = avgAlu[oldRed];
                    int newGreen = avgAlu[oldGreen];
                    int newBlue = avgAlu[oldBlue];

                    int newPixel = Color.argb(255, newRed, newGreen, newBlue);
                    newPixels[x + y * width] = newPixel;
                }
            }
            resultBitmap.setPixels(newPixels, 0, width, 0, 0, width, height);
            return resultBitmap;
        }

        /* Fungsi untuk mendapatkan hasil ekualisasi rgb dari gambar
         *  I.S gambar, batasan warna, dan besar ekualisasi dari weightBar
         *  F.S gambar baru hasil ekualisasi rgb
         */
        public static Bitmap rgbEqualization(Bitmap inputBitmap, int colorBoundaries, float w) {
            int width = inputBitmap.getWidth();
            int height = inputBitmap.getHeight();
            int numOfPixels = width * height;
            Bitmap resultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            int[] newPixels = new int[width * height];

            HistogramData histogramData = generateBitmapHistogramData(inputBitmap, colorBoundaries);
            int[] redHist = histogramData.getRedPixelHistogram();
            int[] greenHist = histogramData.getGreenPixelHistogram();
            int[] blueHist = histogramData.getBluePixelHistogram();

            int[] redAlu = generateAlu(redHist, numOfPixels, colorBoundaries, w);
            int[] greenAlu = generateAlu(greenHist, numOfPixels, colorBoundaries, w);
            int[] blueAlu = generateAlu(blueHist, numOfPixels, colorBoundaries, w);

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int pixel = inputBitmap.getPixel(x,y);
                    int oldRed = Color.red(pixel);
                    int oldBlue = Color.blue(pixel);
                    int oldGreen = Color.green(pixel);

                    int newRed = redAlu[oldRed];
                    int newGreen = greenAlu[oldGreen];
                    int newBlue = blueAlu[oldBlue];

                    int newPixel = Color.argb(255, newRed, newGreen, newBlue);
                    newPixels[x + y * width] = newPixel;
                }
            }
            resultBitmap.setPixels(newPixels, 0, width, 0, 0, width, height);
            return resultBitmap;
        }

        /* Fungsi untuk membuat Histogram baru dari Histogram lama
        *  I.S Histogram lama, banyak pixel dalam gambar (panjang x lebar gambar),
        *      batasan warna, besar ekualisasi dari weightBar
        *  F.S Histogram baru hasil ekualisasi gambar
        * */
        public static int[] generateAlu(int[] originalHist,
                                        int numOfPixels,
                                        int colorBoundaries,
                                        float w) {
            int[] alu = new int[colorBoundaries];
            int[] cdf = getCumulativeDistributionFunction(originalHist);

            for (int i = 0; i < cdf.length; i++) {
                cdf[i] *= w;
            }

            int cdfMin = getFirstNonZeroCdfValue(cdf);

            for (int i = 0; i < colorBoundaries; i++) {
                alu[i] = equalizePixel(cdf[i], cdfMin, numOfPixels, colorBoundaries);
            }
            return alu;
        }

        /* Fungsi untuk mendapatkan nilai ekualisasi dalam satu derajat warna */
        private static int equalizePixel(int cdfValue, int cdfMin, int numOfPixels,
                                             int colorBoundaries) {
            float result = (float)(cdfValue - cdfMin)/(numOfPixels - cdfMin) * (colorBoundaries - 1);
            return (int)result;
        }
    }

    // Histogram Stretching Implementation
    public static class HistogramStretcher {
        private HistogramStretcher() {}

        /* Fungsi untuk mendapatkan gambar hasil Linear Constrant Stretching */
        public static Bitmap stretchHistogram(Bitmap inputBitmap, int colorBoundaries) {
    //        reference:
    //        http://spatial-analyst.net/ILWIS/htm/ilwisapp/stretch_algorithm.htm
    //        http://myjavacodeworld.blogspot.com/2012/10/how-to-linear-stretch-any.html

            int width = inputBitmap.getWidth();
            int height = inputBitmap.getHeight();
            Bitmap resultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            int[] newPixels = new int[width * height];

            /* Mendapatkan data histogram gambar */
            HistogramData histogramData = generateBitmapHistogramData(inputBitmap, colorBoundaries);
            int[] redHist = histogramData.getRedPixelHistogram();
            int[] greenHist = histogramData.getGreenPixelHistogram();
            int[] blueHist = histogramData.getBluePixelHistogram();

            /* Mencari batas bawah dan batas atas data histogram setiap warna
            *  untuk selanjutnya digunakan dalam rumus pemetaan nilai pixel ke nilai baru */
            int redLow = getLowerBoundaryOf(redHist);
            int redUp = getUpperBoundaryOf(redHist);
            int greenLow = getLowerBoundaryOf(greenHist);
            int greenUp = getUpperBoundaryOf(greenHist);
            int blueLow = getLowerBoundaryOf(blueHist);
            int blueUp = getUpperBoundaryOf(blueHist);

            int outputLow = 0;
            int outputUp = colorBoundaries - 1;

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int pixel = inputBitmap.getPixel(x,y);
                    int oldRed = Color.red(pixel);
                    int oldBlue = Color.blue(pixel);
                    int oldGreen = Color.green(pixel);

                    int newRed = stretchPixel(oldRed, redLow, redUp, outputLow, outputUp);
                    int newGreen = stretchPixel(oldGreen, greenLow, greenUp, outputLow, outputUp);
                    int newBlue = stretchPixel(oldBlue, blueLow, blueUp, outputLow, outputUp);

                    // Make sure new pixel value stay inside color boundary
                    newRed = (newRed > outputUp) ? outputUp : newRed;
                    newRed = (newRed < outputLow) ? outputLow : newRed;
                    newGreen = (newGreen > outputUp) ? outputUp : newGreen;
                    newGreen = (newGreen < outputLow) ? outputLow : newGreen;
                    newBlue = (newBlue > outputUp) ? outputUp : newBlue;
                    newBlue = (newBlue < outputLow) ? outputLow : newBlue;

                    int newPixel = Color.argb(255, newRed, newGreen, newBlue);
                    newPixels[x + y * width] = newPixel;
                }
            }
            resultBitmap.setPixels(newPixels, 0, width, 0, 0, width, height);
            return resultBitmap;
        }

        /* Fungsi untuk mendapatkan nilai pixel hasil Linear Constrant Stretching */
        private static int stretchPixel(int inputVal, int inputLow, int inputUp, int outputLow, int outputUp) {
            return (int)((inputVal - inputLow) * ((float)(outputUp - outputLow)/(inputUp - inputLow)) + outputLow);
        }
    }

    // Histogram Specification Matching Implementation
    public static class HistogramMatcher {
        private HistogramMatcher() {}

        /* Fungsi untuk mendapatkan gambar hasil Image Specification Matching
        *  targetY memiliki 9 elemen, terdiri atas R, G, B dan masing-masing warna
        *  memiliki 3 nilai
        * */
        public static Bitmap matchHistogram(Bitmap inputBitmap, int[] targetY, int colorBoundaries) {
            int width = inputBitmap.getWidth();
            int height = inputBitmap.getHeight();
            int numOfPixels = width * height;
            Bitmap resultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            int[] newPixels = new int[width * height];

            HistogramData histogramData = generateBitmapHistogramData(inputBitmap, colorBoundaries);
            int[] redHist = histogramData.getRedPixelHistogram();
            int[] greenHist = histogramData.getGreenPixelHistogram();
            int[] blueHist = histogramData.getBluePixelHistogram();

//            Currently assumes targetY array always has 9 elements, RGB with each having 3 values
            int[] redTargetHist;
            int[] greenTargetHist;
            int[] blueTargetHist;
            boolean matchRed = !(targetY[0] < 0 || targetY[1] < 0 || targetY[2] < 0);
            boolean matchGreen = !(targetY[3] < 0 || targetY[4] < 0 || targetY[5] < 0);
            boolean matchBlue = !(targetY[6] < 0 || targetY[7] < 0 || targetY[8] < 0);

            /* Check apakah match di warna merah, jika tidak gunakan data histogram lama */
            if (targetY[0] < 0 || targetY[1] < 0 || targetY[2] < 0) {
                redTargetHist = redHist.clone();
            } else {
                redTargetHist = generateTargetHist(targetY[0], targetY[1], targetY[2]);
            }

            /* Check apakah match di warna hijau, jika tidak gunakan data histogram lama */
            if (targetY[3] < 0 || targetY[4] < 0 || targetY[5] < 0) {
                greenTargetHist = greenHist.clone();
            } else {
                greenTargetHist = generateTargetHist(targetY[3], targetY[4], targetY[5]);
            }

            /* Check apakah match di warna biru, jika tidak gunakan data histogram lama */
            if (targetY[6] < 0 || targetY[7] < 0 || targetY[8] < 0) {
                blueTargetHist = blueHist.clone();
            } else {
                blueTargetHist = generateTargetHist(targetY[6], targetY[7], targetY[8]);
            }

            int[] redMatchedAlu = generateMatchedAlu(redHist, redTargetHist);
            int[] greenMatchedAlu = generateMatchedAlu(greenHist, greenTargetHist);
            int[] blueMatchedAlu = generateMatchedAlu(blueHist, blueTargetHist);

            /* Membuat pixel baru dari ALU yang sudah di-generate */
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int pixel = inputBitmap.getPixel(x,y);
                    int oldRed = Color.red(pixel);
                    int oldBlue = Color.blue(pixel);
                    int oldGreen = Color.green(pixel);

                    int newRed= (matchRed) ? redMatchedAlu[oldRed] : oldRed;
                    int newGreen= (matchGreen) ? greenMatchedAlu[oldRed] : oldGreen;
                    int newBlue= (matchBlue) ? blueMatchedAlu[oldRed] : oldBlue;

                    int newPixel = Color.argb(255, newRed, newGreen, newBlue);
                    newPixels[x + y * width] = newPixel;
                }
            }
            resultBitmap.setPixels(newPixels, 0, width, 0, 0, width, height);
            return resultBitmap;
        }

        /* Fungsi untuk membuat histogram target */
        private static int[] generateTargetHist(int yA, int yB, int yC) {
            final int xA = 0;
            final int xB = 127;
            final int xC = 255;
            int[] result = new int[256];

            // First line
            for (int i=xA; i < xB; i++) {
                result[i] = (int)linearFunction(xA, yA, xB, yB, i);
            }
            // Second line
            for (int i=xB; i <= xC; i++) {
                result[i] = (int)linearFunction(xB, yB, xC, yC, i);
            }
            return result;
        }

        /* Fungsi untuk membuat ALU yang match dengan histogram awal dan histogram target */
        private static int[] generateMatchedAlu(int[] originalHist, int[] targetHist) {
            int[] lookupTable = new int[256];
            int[] originalCdf = getCumulativeDistributionFunction(originalHist);
            int originalMax = originalCdf[255];
            int[] targetCdf = getCumulativeDistributionFunction(targetHist);
            int targetMax = targetCdf[255];
            for (int i = 0; i < 256; i++) {
                float currentDiff = 1;
                int currentJ = 0;
                //float originalPdf = (float)originalHist[i]/(float)originalMax;
                float originalPdf = (float)originalCdf[i]/(float)originalMax;
                for (int j = 0; j < 256; j++) {
                    //float targetPdf = (float)targetHist[j]/(float)targetMax;
                    float targetPdf = (float)targetCdf[j]/(float)targetMax;
                    float testDiff = Math.abs(originalPdf - targetPdf);
                    if (testDiff < currentDiff) {
                        currentDiff = testDiff;
                        currentJ = j;
                    }
                }
                lookupTable[i] = currentJ;
            }
            return lookupTable;
        }
    }
}
