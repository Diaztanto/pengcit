package com.tanto.imagehistogram;

/* Kelas tipe data Histogram untuk setiap pixel gambar */
public class HistogramData {
    /* Total kemunculan setiap derajat warna untuk setiap warna dalam gambar
    *  Warna yang dimaksud adalah warna merah, hijau, biru.*/
    private int[] redPixel;
    private int[] greenPixel;
    private int[] bluePixel;
    // Total kemunculan setiap derajat intensitas dalam gambar
    private int[] grayPixel;

    // Batas derajat warna, yaitu 0 s/d 255
    private final int originalColorBoundaries;

    public HistogramData(int[] redPixel, int[] greenPixel, int[] bluePixel, int[] grayPixel) {
        this.redPixel = redPixel.clone();
        this.greenPixel = greenPixel.clone();
        this.bluePixel = bluePixel.clone();
        this.grayPixel = grayPixel.clone();
        this.originalColorBoundaries = redPixel.length;
    }

    public int[] getRedPixelHistogram() {
        return redPixel;
    }

    public int[] getGreenPixelHistogram() {
        return greenPixel;
    }

    public int[] getBluePixelHistogram() {
        return bluePixel;
    }

    public int[] getGrayPixelHistogram() {
        return grayPixel;
    }

    public int getColorBoundariesValue() {
        return originalColorBoundaries;
    }

    public void setRedPixelHistogram(int[] redPixel) {
        this.redPixel = redPixel;
    }

    public void setGreenPixelHistogram(int[] greenPixel) {
        this.greenPixel = greenPixel;
    }

    public void setBluePixelHistogram(int[] bluePixel) {
        this.bluePixel = bluePixel;
    }

    public void setGrayPixelHistogram(int[] grayPixel) {
        this.grayPixel = grayPixel;
    }
}