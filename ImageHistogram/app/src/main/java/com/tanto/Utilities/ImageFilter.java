package com.tanto.Utilities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import java.io.ByteArrayOutputStream;

public class ImageFilter {
    public static Bitmap rescaleBitmap(Bitmap original, double scale) {
        int desthWidth = (int)((float)original.getWidth() * scale);
        int destHeight = (int)((float)original.getHeight() * scale);
        Bitmap result = Bitmap.createScaledBitmap(original, desthWidth, destHeight, false);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        result.compress(Bitmap.CompressFormat.PNG, 70, outStream);
        return result;
    }
    public static Bitmap convertToGrayScale(Bitmap bitmap) {
//        https://www.johndcook.com/blog/2009/08/24/algorithms-convert-color-grayscale/
//        stackoverflow how to manipulate pixels in bitmap
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap resultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(resultBitmap);
        Paint p = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        p.setColorFilter(f);
        c.drawBitmap(bitmap, 0, 0, p);

        return resultBitmap;
    }

    public static Bitmap toMonoBlackAndWhite(Bitmap inputBitmap) {
        int width = inputBitmap.getWidth();
        int height = inputBitmap.getHeight();
        Bitmap resultBitmap = convertToGrayScale(inputBitmap);
        int[] newPixels = new int[width * height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int pixel = resultBitmap.getPixel(x,y);
                int intensity = Color.red(pixel);
                int newPixel = (intensity < 255 /2) ? Color.BLACK : Color.WHITE;
                newPixels[x + y * width] = newPixel;
            }
        }
        resultBitmap.setPixels(newPixels, 0, width, 0, 0, width, height);
        return resultBitmap;
    }
}
