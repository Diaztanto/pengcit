/*
    Adapted from:
    https://rosettacode.org/wiki/Image_convolution#Java
    https://www.cs.rutgers.edu/~elgammal/classes/cs334/Filters.pdf
 */

package com.tanto.kernel;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.tanto.Utilities.ImageFilter;

import java.util.ArrayList;
import java.util.Collections;

public class Kernel {
    /*
    EDGE DETECTION A, SHARPEN KERNEL
     */
    private static double FC_EDGE_CONS = 1.0/(2*Math.sqrt(2));
    final public static double[][] IDENTITY_KERNEL = {
            {0, 0, 0},
            {0, 1, 0},
            {0, 0, 0}
    };
    final public static double[][] EDGE_DETECTION__KERNEL_A = {
            {1, 0, -1},
            {0, 0, 0},
            {-1, 0, 1}
    };
    final public static double[][] EDGE_DETECTION__KERNEL_B = {
            {0, 1, 0},
            {1, -4, 1},
            {0, 1, 0}
    };
    final public static double[][] EDGE_DETECTION__KERNEL_C = {
            {-1, -1, -1},
            {-1, 8, -1},
            {-1, -1, -1}
    };
    final public static double[][] SHARPEN_KERNEL = {
            {0, -1, 0},
            {-1, 5, -1},
            {0, -1, 0}
    };
    final public static double[][] BOX_BLUR_KERNEL = {
            {1.0/9, 1.0/9, 1.0/9},
            {1.0/9, 1.0/9, 1.0/9},
            {1.0/9, 1.0/9, 1.0/9}
    };
    final public static double[][] GAUSSIAN_BLUR_KERNEL = {
            {1.0/16, 2.0/16, 1.0/16},
            {2.0/16, 4.0/16, 2.0/16},
            {1.0/16, 2.0/16, 1.0/16}
    };
    final public static double[][] SOBEL_X_KERNEL = {
            {1, 0, -1},
            {2, 0, -2},
            {1, 0, -1}
    };
    final public static double[][] SOBEL_Y_KERNEL = {
            {1, 2, 1},
            {0, 0, 0},
            {-1, -2, -1}
    };
    final public static double[][] PREWITT_X_KERNEL = {
            {1, 0, -1},
            {1, 0, -1},
            {1, 0, -1}
    };
    final public static double[][] PREWITT_Y_KERNEL = {
            {1, 1, 1},
            {0, 0, 0},
            {-1, -1, -1}
    };
    final public static double[][] ROBERT_X_KERNEL = {
            {0, 0, 0},
            {0, 1, 0},
            {0, 0, -1}
    };
    final public static double[][] ROBERT_Y_KERNEL = {
            {0, 0, 0},
            {0, 0, 1},
            {0, -1, 0}
    };
    final public static double[][][] FREI_CHEN_EDGE_KERNELS = {
            {
                {1.0*FC_EDGE_CONS, Math.sqrt(2)*FC_EDGE_CONS, 1.0*FC_EDGE_CONS},
                {0, 0, 0},
                {-1.0*FC_EDGE_CONS, -Math.sqrt(2)*FC_EDGE_CONS, -1.0*FC_EDGE_CONS}
            },
            {
                {1.0*FC_EDGE_CONS, 0, -1.0*FC_EDGE_CONS},
                {Math.sqrt(2)*FC_EDGE_CONS, 0, -Math.sqrt(2)*FC_EDGE_CONS},
                {1.0*FC_EDGE_CONS, 0, -1.0*FC_EDGE_CONS}
            },
            {
                {0, -1.0*FC_EDGE_CONS, Math.sqrt(2)*FC_EDGE_CONS},
                {1.0*FC_EDGE_CONS, 0, -1.0*FC_EDGE_CONS},
                {-Math.sqrt(2)*FC_EDGE_CONS, 1.0*FC_EDGE_CONS, 0}
            },
            {
                {Math.sqrt(2)*FC_EDGE_CONS, -1.0*FC_EDGE_CONS, 0},
                {-1.0*FC_EDGE_CONS, 0, 1.0*FC_EDGE_CONS},
                {0, 1.0*FC_EDGE_CONS, -Math.sqrt(2)*FC_EDGE_CONS}
            }
    };
    final public static double[][][] FREI_CHEN_LINE_KERNELS = {
            {
                    {0, 0.5, 0},
                    {-0.5, 0, -0.5},
                    {0, 0.5, 0}
            },
            {
                    {-0.5, 0, 0.5},
                    {0, 0, 0},
                    {0.5, 0, -0.5}
            },
            {
                    {1.0/6, -2.0/6, 1.0/6},
                    {-2.0/6, 4.0/6, -2.0/6},
                    {1.0/6, -2.0/6, 1.0/6}
            },
            {
                    {-2.0/6, 1.0/6, -2.0/6},
                    {1.0/6, 4.0/6, 1.0/6},
                    {-2.0/6, 1.0/6, -2.0/6}
            }
    };
    final public static double[][] FREI_CHEN_AVG = {
            {1.0/3, 1.0/3, 1.0/3},
            {1.0/3, 1.0/3, 1.0/3},
            {1.0/3, 1.0/3, 1.0/3}
    };



    public Kernel() {}

//    Makes sure the pixel color value is within the range of 0 - endIndex (e.g. 256)
    private static int bound(int value, int endIndex) {
        if (value < 0) {
            return 0;
        }
        if (value < endIndex) {
            return value;
        }
        return endIndex - 1;
    }

    public int applyHomogenGradientToPixel(Bitmap inputImage, int x, int y) {
        int oldRedValue = Color.red(inputImage.getPixel(x, y));
        int oldGreenValue = Color.green(inputImage.getPixel(x, y));
        int oldBlueValue = Color.blue(inputImage.getPixel(x, y));

        int newRedValue = 0;
        int newGreenValue = 0;
        int newBlueValue = 0;
        for (int row = y-1; row <= y+1; row++) {
            for (int column = x-1; column <= x+1; column++) {
                if (column == x && row == y) {
                    continue;
                }
                int tempRed = 0;
                int tempGreen = 0;
                int tempBlue = 0;
                try {
                    tempRed = Math.abs(oldRedValue - Color.red(inputImage.getPixel(column, row)));
                    tempGreen = Math.abs(oldGreenValue - Color.green(inputImage.getPixel(column, row)));
                    tempBlue = Math.abs(oldBlueValue - Color.blue(inputImage.getPixel(column, row)));
                } catch (IllegalArgumentException e) {
                    tempRed = Math.abs(oldRedValue - 0);
                    tempGreen = Math.abs(oldGreenValue - 0);
                    tempBlue = Math.abs(oldBlueValue - 0);
                }
                newRedValue = (newRedValue < tempRed ? tempRed : newRedValue);
                newGreenValue = (newGreenValue < tempGreen ? tempGreen : newGreenValue);
                newBlueValue = (newBlueValue < tempBlue ? tempBlue : newBlueValue);
            }
        }
        int newPixelValue = Color.argb(255, newRedValue, newGreenValue, newBlueValue);
        return newPixelValue;
    }

    public Bitmap getHomogenGradientBitmap(Bitmap inputImage) {
        int inputWidth = inputImage.getWidth();
        int inputHeight = inputImage.getHeight();
        Bitmap outputImage = Bitmap.createBitmap(inputWidth, inputHeight, Bitmap.Config.ARGB_8888);
        Bitmap grayScaledInputImage = ImageFilter.convertToGrayScale(inputImage);
        for (int y = 0; y < inputHeight; y++) {
            for (int x = 0; x < inputWidth; x++) {
                int newPixelValue = applyHomogenGradientToPixel(grayScaledInputImage, x, y);
                outputImage.setPixel(x, y, newPixelValue);
            }
        }
        return outputImage;
    }

    public int applyDifferenceGradientToPixel(Bitmap inputImage, int x, int y) {
        int newRedValue = 0;
        int newGreenValue = 0;
        int newBlueValue = 0;
        int rowOffset = 2;
        for (int row = y-1; row <= y+1; row++) {
             int columnOffset = 2;
            for (int column = x-1; column <= x+1; column++) {
                if (column == x && row == y) {
                    continue;
                }
                int tempRedA = 0;
                int tempGreenA = 0;
                int tempBlueA = 0;
                int tempRedB = 0;
                int tempGreenB = 0;
                int tempBlueB = 0;

                try {
                    tempRedA = Color.red(inputImage.getPixel(column, row));
                    tempGreenA = Color.green(inputImage.getPixel(column, row));
                    tempBlueA = Color.blue(inputImage.getPixel(column, row));
                } catch (IllegalArgumentException e) {
                    tempRedA = 0;
                    tempGreenA = 0;
                    tempBlueA =  0;
                }
                try {
                    tempRedB = Color.red(inputImage.getPixel(column + columnOffset, row + rowOffset));
                    tempGreenB = Color.green(inputImage.getPixel(column + columnOffset, row + rowOffset));
                    tempBlueB = Color.blue(inputImage.getPixel(column + columnOffset, row + rowOffset));
                } catch (IllegalArgumentException e) {
                    tempRedB = 0;
                    tempGreenB = 0;
                    tempBlueB =  0;
                }
                int tempNewRed = Math.abs(tempRedA - tempRedB);
                int tempNewGreen = Math.abs(tempGreenA - tempGreenB);
                int tempNewBlue = Math.abs(tempBlueA - tempBlueB);
                newRedValue = (newRedValue < tempNewRed ? tempNewRed : newRedValue);
                newGreenValue = (newGreenValue < tempNewGreen ? tempNewGreen : newGreenValue);
                newBlueValue = (newBlueValue < tempNewBlue ? tempNewBlue : newBlueValue);
                columnOffset -= 2;
            }
            rowOffset -= 2;
        }
        int newPixelValue = Color.argb(255, newRedValue, newGreenValue, newBlueValue);
        return newPixelValue;
    }

    public Bitmap getDifferenceGradientBitmap(Bitmap inputImage) {
        int inputWidth = inputImage.getWidth();
        int inputHeight = inputImage.getHeight();
        Bitmap outputImage = Bitmap.createBitmap(inputWidth, inputHeight, Bitmap.Config.ARGB_8888);
        Bitmap grayScaledInputImage = ImageFilter.convertToGrayScale(inputImage);
        for (int y = 0; y < inputHeight; y++) {
            for (int x = 0; x < inputWidth; x++) {
                int newPixelValue = applyDifferenceGradientToPixel(grayScaledInputImage, x, y);
                outputImage.setPixel(x, y, newPixelValue);
            }
        }
        return outputImage;
    }

    public int applySmoothingToPixel_8(Bitmap inputImage, int x, int y) {
        ArrayList<Integer> neighborRedValue = new ArrayList<>();
        ArrayList<Integer> neighborGreenValue = new ArrayList<>();
        ArrayList<Integer> neighborBlueValue = new ArrayList<>();

        for (int row = y-1; row <= y+1; row++) {
            for (int column = x-1; column <= x+1; column++) {
                if (column == x && row == y) {
                    continue;
                }
                try {
                    neighborRedValue.add(Color.red(inputImage.getPixel(column, row)));
                    neighborGreenValue.add(Color.green(inputImage.getPixel(column, row)));
                    neighborBlueValue.add(Color.blue(inputImage.getPixel(column, row)));
                } catch (IllegalArgumentException e) {
                    continue;
                }
            }
        }
        Collections.sort(neighborRedValue);
        Collections.sort(neighborGreenValue);
        Collections.sort(neighborBlueValue);
        int medianIdx = (neighborRedValue.size()) / 2;
        int newPixelValue = Color.argb(255,
                neighborRedValue.get(medianIdx),
                neighborGreenValue.get(medianIdx),
                neighborBlueValue.get(medianIdx));
        return newPixelValue;
    }

    public Bitmap smoothImage(Bitmap inputImage) {
        int inputWidth = inputImage.getWidth();
        int inputHeight = inputImage.getHeight();
        Bitmap outputImage = Bitmap.createBitmap(inputWidth, inputHeight, Bitmap.Config.ARGB_8888);
        Bitmap grayScaledInputImage = ImageFilter.convertToGrayScale(inputImage);
        for (int y = 0; y < inputHeight; y++) {
            for (int x = 0; x < inputWidth; x++) {
                int newPixelValue = applySmoothingToPixel_8(inputImage, x, y);
                outputImage.setPixel(x, y, newPixelValue);
            }
        }
        return outputImage;
    }

    public Bitmap convolve(Bitmap inputImage, double[][] kernel, boolean convertGrayScale) {
        int inputWidth = inputImage.getWidth();
        int inputHeight = inputImage.getHeight();
        Bitmap outputImage = Bitmap.createBitmap(inputWidth, inputHeight, Bitmap.Config.ARGB_8888);
        // there should be kernel-size-is-odd check for IllegalArgumentException
        ImageData inputData;
        if (convertGrayScale) {
            inputData = new ImageData(ImageFilter.convertToGrayScale(inputImage));
        } else {
            inputData = new ImageData(inputImage);
        }
        for (int y = 0; y < inputHeight; y++) {
            for (int x = 0; x < inputWidth; x++) {
                applyKernelToPixel(outputImage, inputData, x, y, kernel);
            }
        }
        return outputImage;
    }

    public Bitmap convolveDouble (Bitmap inputImage, double[][] kernelX, double[][] kernelY, boolean convertGrayScale) {
        int inputWidth = inputImage.getWidth();
        int inputHeight = inputImage.getHeight();
        Bitmap outputImage = Bitmap.createBitmap(inputWidth, inputHeight, Bitmap.Config.ARGB_8888);
        // there should be kernel-size-is-odd check for IllegalArgumentException
        ImageData inputData;
        if (convertGrayScale) {
            inputData = new ImageData(ImageFilter.convertToGrayScale(inputImage));
        } else {
            inputData = new ImageData(inputImage);
        }
        for (int y = 0; y < inputHeight; y++) {
            for (int x = 0; x < inputWidth; x++) {
                applyXYKernelToPixel(outputImage, inputData, x, y, kernelX, kernelY);
            }
        }
        return outputImage;
    }

    public Bitmap FreiChenEdgeDetection (Bitmap inputImage, boolean convertGrayScale) {
        int inputWidth = inputImage.getWidth();
        int inputHeight = inputImage.getHeight();
        Bitmap outputImage = Bitmap.createBitmap(inputWidth, inputHeight, Bitmap.Config.ARGB_8888);
        // there should be kernel-size-is-odd check for IllegalArgumentException
        ImageData inputData;
        if (convertGrayScale) {
            inputData = new ImageData(ImageFilter.convertToGrayScale(inputImage));
        } else {
            inputData = new ImageData(inputImage);
        }
        for (int y = 0; y < inputHeight; y++) {
            for (int x = 0; x < inputWidth; x++) {
                applyFreiChenEdgeKernelToPixel(outputImage, inputData, x, y);
            }
        }
        return outputImage;
    }

    private void applyKernelToPixel(Bitmap outputImage, ImageData inputData, int x, int y, double[][] kernel) {
        int row = y;
        int column = x;
        double kernelElementSum = getKernelElementSum(inputData, x, y, kernel);
        double newRedValue = 0;
        double newGreenValue = 0;
        double newBlueValue = 0;
        for (int i = kernel.length-1; i >= 0; i--) {
            int columnOffset = -(kernel.length - 1) + i;
            for (int j = kernel[i].length-1; j >= 0; j--) {
                int rowOffset = -(kernel[i].length - 1) + i;
                try {
                    newRedValue += (float) inputData.red[row+rowOffset][column+columnOffset] * kernel[i][j] / kernelElementSum;
                    newGreenValue += (float) inputData.green[row+rowOffset][column+columnOffset] * kernel[i][j] / kernelElementSum;
                    newBlueValue += (float) inputData.blue[row+rowOffset][column+columnOffset] * kernel[i][j] / kernelElementSum;
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue;
                }
            }
        }
        int r = bound((int) Math.round(newRedValue), 256);
        int g = bound((int) Math.round(newGreenValue), 256);
        int b = bound((int) Math.round(newBlueValue), 256);
        int newPixelValue = Color.argb(255, r, g, b);
        outputImage.setPixel(x, y, newPixelValue);
    }

    private void applyXYKernelToPixel(Bitmap outputImage, ImageData inputData, int x, int y, double[][] kernelX, double[][] kernelY) {
        int row = y;
        int column = x;
        //double kernelElementSum = getKernelElementSum(inputData, x, y, kernel);
        double newXRedValue = 0;
        double newXGreenValue = 0;
        double newXBlueValue = 0;
        double newYRedValue = 0;
        double newYGreenValue = 0;
        double newYBlueValue = 0;
        for (int i = kernelX.length-1; i >= 0; i--) {
            int columnOffset = -(kernelX.length - 1) + i;
            for (int j = kernelX[i].length-1; j >= 0; j--) {
                int rowOffset = -(kernelX[i].length - 1) + i;
                try {
                    newXRedValue += inputData.red[row+rowOffset][column+columnOffset] * kernelX[i][j];
                    newXGreenValue += inputData.green[row+rowOffset][column+columnOffset] * kernelX[i][j];
                    newXBlueValue += inputData.blue[row+rowOffset][column+columnOffset] * kernelX[i][j];
                    newYRedValue += inputData.red[row+rowOffset][column+columnOffset] * kernelY[i][j];
                    newYGreenValue += inputData.green[row+rowOffset][column+columnOffset] * kernelY[i][j];
                    newYBlueValue += inputData.blue[row+rowOffset][column+columnOffset] * kernelY[i][j];
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue;
                }
            }
        }
        int r = bound((int)(Math.sqrt(Math.pow(newXRedValue,2)+Math.pow(newYRedValue,2))),256);
        int g = bound((int)(Math.sqrt(Math.pow(newXGreenValue,2)+Math.pow(newYGreenValue,2))),256);
        int b = bound((int)(Math.sqrt(Math.pow(newXBlueValue,2)+Math.pow(newYBlueValue,2))), 256);
        int newPixelValue = Color.argb(255, r, g, b);
        outputImage.setPixel(x, y, newPixelValue);
    }

    private void applyFreiChenEdgeKernelToPixel(Bitmap outputImage, ImageData inputData, int x, int y) {
        int row = y;
        int column = x;
        double[] redValues = new double[9];
        double[] greenValues = new double[9];
        double[] blueValues = new double[9];

        for (int edgeIdx = 0; edgeIdx < FREI_CHEN_EDGE_KERNELS.length; edgeIdx++) {
            double[][] kernel = FREI_CHEN_EDGE_KERNELS[edgeIdx];
            for (int i = 0; i < kernel.length; i++) {
                int rowOffset = i - 1;
                for (int j = 0; j < kernel[i].length; j++) {
                    int columnOffset = j - 1;
                    try {
                        redValues[edgeIdx] += ((double) inputData.red[row+rowOffset][column+columnOffset] * kernel[i][j]);
                        greenValues[edgeIdx] += ((double) inputData.green[row+rowOffset][column+columnOffset] * kernel[i][j]);
                        blueValues[edgeIdx] += ((double) inputData.blue[row+rowOffset][column+columnOffset] * kernel[i][j]);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        continue;
                    }
                }
            }
        }
        int offset = FREI_CHEN_EDGE_KERNELS.length;
        for (int edgeIdx = 0; edgeIdx < FREI_CHEN_LINE_KERNELS.length; edgeIdx++) {
            double[][] kernel = FREI_CHEN_LINE_KERNELS[edgeIdx];
            for (int i = 0; i < kernel.length; i++) {
                int rowOffset = i - 1;
                for (int j = 0; j < kernel[i].length; j++) {
                    int columnOffset = j - 1;
                    try {
                        redValues[edgeIdx+offset] += ((double) inputData.red[row+rowOffset][column+columnOffset] * kernel[i][j]);
                        greenValues[edgeIdx+offset] += ((double) inputData.green[row+rowOffset][column+columnOffset] * kernel[i][j]);
                        blueValues[edgeIdx+offset] += ((double) inputData.blue[row+rowOffset][column+columnOffset] * kernel[i][j]);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        continue;
                    }
                }
            }
        }
        for (int i = 0; i < FREI_CHEN_AVG.length; i++) {
            int rowOffset = i - 1;
            for (int j = 0; j < FREI_CHEN_AVG[i].length; j++) {
                int columnOffset = j - 1;
                try {
                    redValues[redValues.length-1] += (double) inputData.red[row+rowOffset][column+columnOffset] * FREI_CHEN_AVG[i][j];
                    greenValues[redValues.length-1] += (double) inputData.green[row+rowOffset][column+columnOffset] * FREI_CHEN_AVG[i][j];
                    blueValues[redValues.length-1] += (double) inputData.blue[row+rowOffset][column+columnOffset] * FREI_CHEN_AVG[i][j];
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue;
                }
            }
        }
        double redEdge = 0;
        double greenEdge = 0;
        double blueEdge = 0;
        for (int i = 0; i < FREI_CHEN_EDGE_KERNELS.length; i++) {
            redEdge += Math.pow(redValues[i], 2);
            greenEdge += Math.pow(greenValues[i], 2);
            blueEdge += Math.pow(blueValues[i], 2);
        }
        double redSum = 0;
        double greenSum = 0;
        double blueSum = 0;
        for (int i = 0; i < redValues.length - 1; i++) {
            redSum += Math.pow(redValues[i], 2);
            greenSum += Math.pow(greenValues[i], 2);
            blueSum += Math.pow(blueValues[i], 2);
        }
        double redCos = Math.sqrt(redEdge/redSum);
        double greenCos = Math.sqrt(greenEdge/greenSum);
        double blueCos = Math.sqrt(blueEdge/blueSum);
//        int r = bound((int) redCos, 256);
//        int g = bound((int) greenCos, 256);
//        int b = bound((int) blueCos, 256);

        // Ini hanya pake edge mask
//        int r = bound((int) Math.sqrt(redEdge), 256);
//        int g = bound((int) Math.sqrt(greenEdge), 256);
//        int b = bound((int) Math.sqrt(blueEdge), 256);

        // Ini pake edge + line mask
        int r = bound((int) Math.sqrt(redSum), 256);
        int g = bound((int) Math.sqrt(greenSum), 256);
        int b = bound((int) Math.sqrt(blueSum), 256);
        int newPixelValue = Color.argb(255, r, g, b);
        outputImage.setPixel(x, y, newPixelValue);
    }

    private double getKernelElementSum(ImageData inputData, int x, int y, double[][] kernel) {
        // TODO: tinker this function; search for normalization
        int row = y;
        int column = x;
        boolean isNormalizedRequired = false;
        double kernelElementSum = 0;
        for (int i = kernel.length-1; i >= 0; i--) {
            int columnOffset = -(kernel.length - 1) + i;
            for (int j = kernel[i].length-1; j >= 0; j--) {
                int rowOffset = -(kernel[i].length - 1) + i;
                try {
                    int t = inputData.red[row+rowOffset][column+columnOffset];
                    kernelElementSum += kernel[i][j];
                } catch (ArrayIndexOutOfBoundsException e) {
                    isNormalizedRequired = true;
                }
            }
        }
        return (isNormalizedRequired ? kernelElementSum : 1);
    }

    private class ImageData {
        public final int[][] red;
        public final int[][] green;
        public final int[][] blue;
        public final int width;
        public final int height;

        public ImageData(int width, int height) {
            this.width = width;
            this.height = height;
            red = new int[height][width];
            green = new int[height][width];
            blue = new int[height][width];
        }

        public ImageData(Bitmap inputImage) {
            width = inputImage.getWidth();
            height = inputImage.getHeight();
            red = new int[height][width];
            green = new int[height][width];
            blue = new int[height][width];
            for (int row = 0; row < height; row++) {
                int y = row;
                for (int column = 0; column < width; column++) {
                    int x = column;
                    red[row][column] = Color.red(inputImage.getPixel(x, y));
                    green [row][column] = Color.green(inputImage.getPixel(x, y));
                    blue [row][column] = Color.blue(inputImage.getPixel(x, y));
                }
            }
        }
        public int get(char channel, int x, int y) {
            int row = y;
            int column = x;
            switch(channel) {
                case 'r':
                    return red[row][column];
                case 'g':
                    return green[row][column];
                case 'b':
                    return blue[row][column];
                default:
                    return -1;
            }
        }
        public void set (char channel, int x, int y, int value) {
            int row = y;
            int column = x;
            switch(channel) {
                case 'r':
                    red[row][column] = value;
                    break;
                case 'g':
                    green[row][column] = value;
                    break;
                case 'b':
                    blue[row][column] = value;
                    break;
                default:
                    return;
            }
        }
    }
}
