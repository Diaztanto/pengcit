package com.tanto.BoundaryDetection;

public class FreemanRecognizer {
    private ChaincodeHistDatabase fccArr;

    public FreemanRecognizer() {
        fccArr = new ChaincodeHistDatabase();
    }

    // calculate "distance" from processed chain code and database chain codes
    // distance = difference of each code (0-7) occurence
    // asumsi setiap chain code udah punya codeOccurence
    public float[] getCodeOccurenceDifference(FreemanChainCode fcc) {
        float[] results = new float[fccArr.getOccurrenceHistograms().size()];

        for (int i = 0; i < results.length; i++) {
            results[i] = getDistance(i, fcc.getCodeOccurences());
        }
        return results;
    }

    public float getDistance(int dbIndex, float[] fccHistogram) {
        float distance = 0;
        float[] dbHist = fccArr.getOccurrenceHistograms().get(dbIndex).clone();
        for (int i = 0; i < 8; i++) {
            distance += Math.abs(dbHist[i] - fccHistogram[i]);
        }

        return distance;
    }
}
