package com.tanto.BoundaryDetection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;

import java.util.ArrayList;

public class FreemanChainCode {
    private ArrayList<Integer> chainCode;

    public Point startingPoint;
    //public int [] codeOccurences;
    public float [] codeOccurences;
    public int sum;

    public FreemanChainCode() {
        chainCode = new ArrayList<>();
        startingPoint = new Point();
        codeOccurences = new float[8];
        for (int i = 0; i < codeOccurences.length; i++) {
            codeOccurences[i] = 0;
        }
    }

    public ArrayList<Integer> getChainCode() {
        return chainCode;
    }

    /* Fungsi untuk mencari nilai Greyscale suatu pixel */
    public int calculateGreyScale(int pixel) {
        int R = (pixel >> 16) & 0xff;
        int G = (pixel >> 8) & 0xff;
        int B = pixel & 0xff;

        return calculateGreyScale(R, G, B);
    }

    /* Fungsi untuk mencari rata-rata nilai RGB */
    public int calculateGreyScale(int red, int green, int blue) {
        return (red + green + blue)/3;
    }

    /* Fungsi mengubah gambar menjadi hitam putih
    * I.S Gambar beserta input threshold dari input seekbar
    * F.S Gambar yang pixelnya diubah menjadi hitam atau putih, bergantung pada threshold
    */
    public Bitmap turnIntoBW(Bitmap origin, int threshold) {
        Bitmap result = origin.copy(origin.getConfig(), true);
        int currentPixelGreyScale;

        for (int i = 0; i < result.getWidth(); i++) {
            for (int j = 0; j < result.getHeight(); j++) {
                currentPixelGreyScale = calculateGreyScale(result.getPixel(i, j));

                if (currentPixelGreyScale > threshold) {
                    result.setPixel(i, j, Color.WHITE);
                } else {
                    result.setPixel(i, j, Color.BLACK);
                }
            }
        }

        return result;
    }

    /* Fungsi untuk melakukan ekstrasi Chain Code dari gambar
    * I.S Gambar hitam putih, hanya ada satu objek dalam gambar, dalam tugas ini adalah angka
    * F.S ChainCode terisi oleh informasi objek dalam gambar
    */
    public void extractChainCode(Bitmap bitmap) {
        chainCode.clear();
        for (int i = 0; i < codeOccurences.length; i++) {
            codeOccurences[i] = 0;
        }
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        int i = 0;
        int j = 0;
        int dir = 7;
        int next = 0;

        /* KAMUS NILAI DIR dan NEXT
        0: EAST
        1: NORTHEAST
        2: NORTH
        3: NORTHWEST
        4: WEST
        5: SOUTHWEST
        6: SOUTH
        7: SOUTHEAST
         */

        //Pencarian pixel yang berada dalam gambar(angka)
        while (i < height && bitmap.getPixel(j, i) != Color.BLACK) {
            while (j < width && bitmap.getPixel(j, i) != Color.BLACK) {
                j++;
            }
            if (j >= width) {
                i++;
                j = 0;
            }
        }

        int examineCount;
        int loopCount = 0;
        Point firstPoint = new Point();
        Point secondPoint = new Point();
        Point previousPoint = new Point();
        Point currentPoint = new Point();
        Point examinedPoint = new Point();

        firstPoint.set(j, i);
        currentPoint.set(firstPoint.x, firstPoint.y);

        startingPoint.set(firstPoint.x, firstPoint.y);

        boolean found = false;
        boolean done = false;

        do {
            //Freeman Chain Code Theory
            if (dir % 2 == 0) {
                next = (dir + 7) % 8;
            } else {
                next = (dir + 6) % 8;
            }

            examineCount = 0;
            found = false;

            //Menentukan titik yang diperiksa
            while (examineCount < 8 && !found) {
                examinedPoint.set(currentPoint.x, currentPoint.y);
                switch (next) {
                    case 0  :
                        examinedPoint.x++;
                        break;
                    case 1  :
                        examinedPoint.x++;
                        examinedPoint.y--;
                        break;
                    case 2  :
                        examinedPoint.y--;
                        break;
                    case 3  :
                        examinedPoint.x--;
                        examinedPoint.y--;
                        break;
                    case 4  :
                        examinedPoint.x--;
                        break;
                    case 5  :
                        examinedPoint.x--;
                        examinedPoint.y++;
                        break;
                    case 6  :
                        examinedPoint.y++;
                        break;
                    default :
                        examinedPoint.x++;
                        examinedPoint.y++;
                        break;
                }

                if (examinedPoint.x >= 0 && examinedPoint.y >= 0 && examinedPoint.x < width && examinedPoint.y < height && bitmap.getPixel(examinedPoint.x,examinedPoint.y) == Color.BLACK) {
                    /* Penelusuran memberikan hasil titik berada di dalam objek
                    * Penelusuran dilanjutkan dari titik yang diperiksa
                    * Vektor antara titik saat ini dengan titik yang diperiksa disimpan untuk kemduian
                    * dibandingkan dengan kumpulan vektor dalam kamus vektor objek
                    */
                    found = true;
                    dir = next;
                    chainCode.add(next);
                    codeOccurences[next]++;
                } else {
                    /* Penentuan arah penelusuran selanjutnya
                    * Penelusuran memberikan hasil titik berada di luar objek
                    * Penelusuran dilakukan pada titik yang sama, dengan arah yang berbeda
                    * Arah selanjutnya ditentukan secara counter-clcokwise
                    */
                    next = (next + 1) % 8;
                    examineCount++;
                }
            }

            //Salah satu variabel pemberhenti loop
            loopCount++;

            if (found) {
                previousPoint.set(currentPoint.x, currentPoint.y);
                currentPoint.set(examinedPoint.x, examinedPoint.y);

                if (loopCount == 1) {
                    secondPoint.set(currentPoint.x, currentPoint.y);
                }
            }

            /* Pengecekan Kondisi Done
            * Kondisi Done adalah ketika currentPoint = SecondPoint dan PreviousPoint = FirstPoint
            * Kondisi ini menandakan penelusuran telah kembali ke titik awal dengan arah yang sama
            * Artinya, seluruh gambar angka telah ditelusuri sampai tuntas
            * */
            if (currentPoint.equals(secondPoint) && previousPoint.equals(firstPoint) && loopCount > 1) {
            //if (loopCount == 65) {
                done = true;
            }
        } while (!done);

        /* Pencarian persentase kemunculan vektor dibandingkan dengan banyak vektor yang ada dalam gambar
        * Selanjutnya akan dibandingkan dengan persentase kemunculan vektor kamus obyek
        * */
        for (int k = 0; k < codeOccurences.length; k++) {
            codeOccurences[k] = codeOccurences[k] / (float) chainCode.size();
        }
    }

    /* Fungsi untuk menggambar sesuai informasi ChainCode
    * I.S ChainCode yang sudah terisi, entah dari fungsi extractChainCode atau ChainCode yang sudah disediakan
    * F.S Gambar yang terbentuk dari informasi ChainCode, hanya dapat membentuk permukaan luar gambar
    */
    public Bitmap drawBitmapFromChainCode(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap(w, h, conf); // this creates a MUTABLE bitmap
        
        Point currentPoint = new Point(startingPoint);
        
        for (int i = 0; i < chainCode.size(); i++) {
            bmp.setPixel(currentPoint.x, currentPoint.y, Color.BLACK);

            /* KAMUS NILAI ISI CHAIN CODE
            0: EAST
            1: NORTHEAST
            2: NORTH
            3: NORTHWEST
            4: WEST
            5: SOUTHWEST
            6: SOUTH
            7: SOUTHEAST
             */

            switch (chainCode.get(i)) {
                case 0  :
                    currentPoint.x++;
                    break;
                case 1  :
                    currentPoint.x++;
                    currentPoint.y--;
                    break;
                case 2  :
                    currentPoint.y--;
                    break;
                case 3  :
                    currentPoint.x--;
                    currentPoint.y--;
                    break;
                case 4  :
                    currentPoint.x--;
                    break;
                case 5  :
                    currentPoint.x--;
                    currentPoint.y++;
                    break;
                case 6  :
                    currentPoint.y++;
                    break;
                default :
                    currentPoint.x++;
                    currentPoint.y++;
                    break;
            }
        }

        return bmp;
    }

    public float[] getCodeOccurences() {
        return codeOccurences;
    }
}
