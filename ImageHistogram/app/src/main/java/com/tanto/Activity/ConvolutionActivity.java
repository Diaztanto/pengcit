package com.tanto.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.tanto.imagehistogram.R;
import com.tanto.kernel.Kernel;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class ConvolutionActivity extends AppCompatActivity {
    private final int PICK_IMAGE = 0;

    private RadioButton homogenGradientRadioButton;
    private RadioButton gradientDifferenceRadioButton;
    private RadioButton smoothingRadioButton;

    private ImageView ivBefore;
    private ImageView ivAfter;

    private Button galleryButton;

    private Bitmap currentBitmap;

    private Kernel kernel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convolution);

        homogenGradientRadioButton  = (RadioButton) findViewById(R.id.radio_homogen_gradient);
        gradientDifferenceRadioButton = (RadioButton) findViewById(R.id.radio_gradient_difference);
        smoothingRadioButton  = (RadioButton) findViewById(R.id.radio_smoothing);

        ivBefore = (ImageView) findViewById(R.id.iv_convo_before);
        ivAfter = (ImageView) findViewById(R.id.iv_convo_after);

        galleryButton = (Button) findViewById(R.id.button_gallery_convo);

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });

        homogenGradientRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentBitmap != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final Bitmap homogenGradientBitmap = kernel.getHomogenGradientBitmap(currentBitmap);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ivAfter.setImageBitmap(homogenGradientBitmap);

                                }
                            });
                        }
                    }).start();
                }
            }
        });

        gradientDifferenceRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentBitmap != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final Bitmap diffGradientBitmap = kernel.getDifferenceGradientBitmap(currentBitmap);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ivAfter.setImageBitmap(diffGradientBitmap);
                                }
                            });
                        }
                    }).start();
                }
            }
        });

        smoothingRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentBitmap != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final Bitmap smoothBitmap = kernel.smoothImage(currentBitmap);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ivAfter.setImageBitmap(smoothBitmap);
                                }
                            });
                        }
                    }).start();
                }
            }
        });

        kernel = new Kernel();
        currentBitmap = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                currentBitmap = BitmapFactory.decodeStream(imageStream);
                ivBefore.setImageBitmap(currentBitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
