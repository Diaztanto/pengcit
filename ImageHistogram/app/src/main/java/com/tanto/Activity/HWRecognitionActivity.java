package com.tanto.Activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tanto.HandwritingDetection.HandwritingClassifier;
import com.tanto.imagehistogram.R;
import com.tanto.SkeletonDetection.Skeleton;
import com.tanto.HandwritingDetection.SymbolFeatureVector;
import com.tanto.SkeletonDetection.Thinning;

import java.io.FileInputStream;
import java.util.ArrayList;

public class HWRecognitionActivity extends AppCompatActivity {

    private ImageView iv_processed_hw;
    private TextView prefix_text;
    private TextView match_char;

    private ProgressBar progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hwrecognition);

        iv_processed_hw = (ImageView) findViewById(R.id.iv_hw_analyzed);
        prefix_text = (TextView) findViewById(R.id.hw_match_prefix_text);
        match_char = (TextView) findViewById(R.id.hw_match_text);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar_hw);

        Bitmap bmp = null;
        String filename = getIntent().getStringExtra("image");

        try {
            FileInputStream is = this.openFileInput(filename);
            bmp = BitmapFactory.decodeStream(is);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        iv_processed_hw.setImageBitmap(bmp);
        iv_processed_hw.setVisibility(View.GONE);
        prefix_text.setVisibility(View.GONE);
        match_char.setVisibility(View.GONE);
        progress_bar.setVisibility(View.VISIBLE);

        // proses di sini, di thread baru
        final Bitmap finalBmp = bmp.copy(bmp.getConfig(),true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<Skeleton> skeletons = Thinning.getSkeletonObject(finalBmp);
                SymbolFeatureVector sfv = new SymbolFeatureVector(skeletons.get(0)); // asumsi 0-9
                HandwritingClassifier hwClassifier = new HandwritingClassifier();
                final char result = hwClassifier.findMatch(sfv, 3);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        match_char.setText(String.valueOf(result));

                        iv_processed_hw.setVisibility(View.VISIBLE);
                        prefix_text.setVisibility(View.VISIBLE);
                        match_char.setVisibility(View.VISIBLE);
                        progress_bar.setVisibility(View.GONE);
                    }
                });
            }
        }).start();
    }
}
