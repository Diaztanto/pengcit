package com.tanto.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tanto.imagehistogram.R;

public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        Button linSandHeqButton = (Button) findViewById(R.id.button1);
        Button hSpecButton = (Button) findViewById(R.id.button2);
        Button fccButton = (Button) findViewById(R.id.button3);
        Button hwButton = (Button) findViewById(R.id.button4);
        Button cvButton = (Button) findViewById(R.id.button5);
        Button krButton = (Button) findViewById(R.id.button6);

        linSandHeqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LinSandHeqActivity.class);
                startActivity(intent);
            }
        });

        hSpecButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HSpecActivity.class);
                startActivity(intent);
            }
        });

        fccButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ObjectDetectActivity.class);
                startActivity(intent);
            }
        });

        hwButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HandwriteActivity.class);
                startActivity(intent);
            }
        });

        cvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ConvolutionActivity.class);
                startActivity(intent);
            }
        });

        krButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Kernel2Activity.class);
                startActivity(intent);
            }
        });
        /*FreemanChainCode fcc = new FreemanChainCode();

        fcc.extractChainCode(fcc.turnIntoBW(BitmapFactory.decodeResource(getResources(), R.drawable.lobster_9), 127));
        //fcc.extractChainCode(BitmapFactory.decodeResource(getResources(), R.drawable.extract_1));

        FreemanRecognizer numberMatcher = new FreemanRecognizer();
        int[] testResult = numberMatcher.getCodeOccurenceDifference(fcc);
        System.out.println(testResult);*/
    }
}
