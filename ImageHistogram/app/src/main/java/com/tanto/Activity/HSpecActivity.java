package com.tanto.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tanto.imagehistogram.R;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

public class HSpecActivity extends AppCompatActivity {

    private SeekBar [] bars;
    private TextView [] progresses;
    static final int REQUEST_TAKE_PHOTO = 1;
    String mCurrentPhotoPath;

    LinearLayout redSpec;
    LinearLayout greenSpec;
    LinearLayout blueSpec;

    CheckBox checkRed;
    CheckBox checkGreen;
    CheckBox checkBlue;

    ImageView imageView;
    Bitmap imageBitmap;
    
    int whichPhoto;

    class CustomOnTouchListener implements SeekBar.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event)
        {
            int action = event.getAction();
            switch (action)
            {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ScrollView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    break;

                case MotionEvent.ACTION_UP:
                    // Allow ScrollView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }

            // Handle Seekbar touch events.
            v.onTouchEvent(event);
            return true;
        }
    }

    class CustomOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        private TextView relatedText;

        public CustomOnSeekBarChangeListener(TextView textView) {
            super();
            relatedText = textView;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            relatedText.setText(String.valueOf(i));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hspec);

        // Elements initializations
        redSpec = (LinearLayout) findViewById(R.id.linearLayout3);
        greenSpec = (LinearLayout) findViewById(R.id.linearLayout4);
        blueSpec = (LinearLayout) findViewById(R.id.linearLayout5);

        final Spinner image_spinner = (Spinner) findViewById(R.id.spinner_image_hspec);

        checkRed = (CheckBox) findViewById(R.id.checkRed);
        checkGreen = (CheckBox) findViewById(R.id.checkGreen);
        checkBlue = (CheckBox) findViewById(R.id.checkBlue);

        bars = new SeekBar[9];
        bars[0] = (SeekBar) findViewById(R.id.seekbar_spec_red1);
        bars[1] = (SeekBar) findViewById(R.id.seekbar_spec_red2);
        bars[2] = (SeekBar) findViewById(R.id.seekbar_spec_red3);
        bars[3] = (SeekBar) findViewById(R.id.seekbar_spec_green1);
        bars[4] = (SeekBar) findViewById(R.id.seekbar_spec_green2);
        bars[5] = (SeekBar) findViewById(R.id.seekbar_spec_green3);
        bars[6] = (SeekBar) findViewById(R.id.seekbar_spec_blue1);
        bars[7] = (SeekBar) findViewById(R.id.seekbar_spec_blue2);
        bars[8] = (SeekBar) findViewById(R.id.seekbar_spec_blue3);

        progresses = new TextView[9];
        progresses[0] = (TextView) findViewById(R.id.textview_red_scale1);
        progresses[1] = (TextView) findViewById(R.id.textview_red_scale2);
        progresses[2] = (TextView) findViewById(R.id.textview_red_scale3);
        progresses[3] = (TextView) findViewById(R.id.textview_green_scale1);
        progresses[4] = (TextView) findViewById(R.id.textview_green_scale2);
        progresses[5] = (TextView) findViewById(R.id.textview_green_scale3);
        progresses[6] = (TextView) findViewById(R.id.textview_blue_scale1);
        progresses[7] = (TextView) findViewById(R.id.textview_blue_scale2);
        progresses[8] = (TextView) findViewById(R.id.textview_blue_scale3);

        final Button applyMatch = (Button) findViewById(R.id.button_apply_matching);

        imageView = (ImageView) findViewById(R.id.iv_photo_hspec);

        // Elements functionality
        bars[0].setOnTouchListener(new CustomOnTouchListener());
        bars[0].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[0]));
        bars[1].setOnTouchListener(new CustomOnTouchListener());
        bars[1].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[1]));
        bars[2].setOnTouchListener(new CustomOnTouchListener());
        bars[2].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[2]));
        bars[3].setOnTouchListener(new CustomOnTouchListener());
        bars[3].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[3]));
        bars[4].setOnTouchListener(new CustomOnTouchListener());
        bars[4].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[4]));
        bars[5].setOnTouchListener(new CustomOnTouchListener());
        bars[5].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[5]));
        bars[6].setOnTouchListener(new CustomOnTouchListener());
        bars[6].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[6]));
        bars[7].setOnTouchListener(new CustomOnTouchListener());
        bars[7].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[7]));
        bars[8].setOnTouchListener(new CustomOnTouchListener());
        bars[8].setOnSeekBarChangeListener(new CustomOnSeekBarChangeListener(progresses[8]));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.image_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        image_spinner.setAdapter(adapter);

        //Image Selection
        image_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    switch (i) {
                        case 1  :
                            whichPhoto = 1;
                            dispatchTakePictureIntent();
                            break;
                        case 2  :
                            whichPhoto = 2;
                            changeImage(R.drawable.photo_1);
                            break;
                        case 3  :
                            whichPhoto = 3;
                            changeImage(R.drawable.photo_2);
                            break;
                        case 4  :
                            whichPhoto = 4;
                            changeImage(R.drawable.photo_3);
                            break;
                        case 5  :
                            whichPhoto = 5;
                            changeImage(R.drawable.photo_4);
                            break;
                        case 6  :
                            whichPhoto = 6;
                            changeImage(R.drawable.photo_5);
                            break;
                        case 7  :
                            whichPhoto = 7;
                            changeImage(R.drawable.photo_6);
                            break;
                        case 8  :
                            whichPhoto = 8;
                            changeImage(R.drawable.photo_7);
                            break;
                        case 9  :
                            whichPhoto = 9;
                            changeImage(R.drawable.photo_8);
                            break;
                        case 10 :
                            whichPhoto = 10;
                            changeImage(R.drawable.sample_1);
                            break;
                        default:
                            whichPhoto = 11;
                            changeImage(R.drawable.sample_2);
                            break;
                    }

                    checkRed.setVisibility(View.VISIBLE);
                    checkGreen.setVisibility(View.VISIBLE);
                    checkBlue.setVisibility(View.VISIBLE);

                    applyMatch.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkRed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    redSpec.setVisibility(View.VISIBLE);
                } else {
                    redSpec.setVisibility(View.GONE);
                }
            }
        });

        checkGreen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    greenSpec.setVisibility(View.VISIBLE);
                } else {
                    greenSpec.setVisibility(View.GONE);
                }
            }
        });

        checkBlue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    blueSpec.setVisibility(View.VISIBLE);
                } else {
                    blueSpec.setVisibility(View.GONE);
                }
            }
        });

        applyMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent switchToResultIntent = new Intent(getBaseContext(), LinSandHeqActivity.class);

                int [] numArr = new int[9];

                if (!checkRed.isChecked()) {

                    numArr[0] = -1;
                    numArr[1] = -1;
                    numArr[2] = -1;

                } else {

                    numArr[0] = bars[0].getProgress();
                    numArr[1] = bars[1].getProgress();
                    numArr[2] = bars[2].getProgress();

                }

                if (!checkGreen.isChecked()) {

                    numArr[3] = -1;
                    numArr[4] = -1;
                    numArr[5] = -1;

                } else {

                    numArr[3] = bars[3].getProgress();
                    numArr[4] = bars[4].getProgress();
                    numArr[5] = bars[5].getProgress();

                }

                if (!checkBlue.isChecked()) {

                    numArr[6] = -1;
                    numArr[7] = -1;
                    numArr[8] = -1;

                } else {

                    numArr[6] = bars[6].getProgress();
                    numArr[7] = bars[7].getProgress();
                    numArr[8] = bars[8].getProgress();

                }

                switchToResultIntent.putExtra("specNumArr", numArr);
                switchToResultIntent.putExtra("imageID", whichPhoto);
                if (whichPhoto == 1) {
                    switchToResultIntent.putExtra("imageAddr", mCurrentPhotoPath);
                }

                startActivity(switchToResultIntent);
            }
        });

        // Elements visibility
        applyMatch.setVisibility(View.GONE);

        checkRed.setVisibility(View.GONE);
        checkGreen.setVisibility(View.GONE);
        checkBlue.setVisibility(View.GONE);

        redSpec.setVisibility(View.GONE);
        greenSpec.setVisibility(View.GONE);
        blueSpec.setVisibility(View.GONE);
    }

    //Taking image from resources
    private void changeImage(int newImageId) {
        imageBitmap = BitmapFactory.decodeResource(getResources(), newImageId);
        imageView.setImageBitmap(imageBitmap);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp =
                DateFormat.getDateTimeInstance().format(new Date());//new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //ex.printStackTrace();
                Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.tanto.imagehistogram.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            } else {
                Toast.makeText(this, "photoFile is null!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            galleryAddPic();

            imageBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            imageView.setImageBitmap(imageBitmap);
        }
    }
}
