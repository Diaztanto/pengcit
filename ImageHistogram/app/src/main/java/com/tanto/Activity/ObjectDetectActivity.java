package com.tanto.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tanto.BoundaryDetection.FreemanChainCode;
import com.tanto.BoundaryDetection.FreemanRecognizer;
import com.tanto.imagehistogram.R;
import com.tanto.SkeletonDetection.Skeleton;
import com.tanto.SkeletonDetection.SkeletonHeuristics;
import com.tanto.SkeletonDetection.Thinning;
import com.tanto.kernel.Kernel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ObjectDetectActivity extends AppCompatActivity {

    private Spinner photo_selection_spinner;
    private ImageView iv_photo;
    private ImageView iv_chaincode;
    private SeekBar threshold_seekbar;
    private Button start_analyze_button;
    private Button gallery_button;
    private TextView match_text;
    private TextView match_template;
    private ProgressBar progressBar;
    private RadioButton rb_boundary;
    private RadioButton rb_skeleton;

    private View iv_placeholder0;
    private View iv_placeholder1;

    private Bitmap currentBitmap;

    private String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int PICK_IMAGE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_detect);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar_fcc);
        photo_selection_spinner = (Spinner) findViewById(R.id.spinner_fcc);
        iv_photo = (ImageView) findViewById(R.id.iv_photo_fcc);
        iv_chaincode = (ImageView) findViewById(R.id.iv_chain_code);
        threshold_seekbar = (SeekBar) findViewById(R.id.threshold_seekbar);
        gallery_button = (Button) findViewById(R.id.button_gallery);
        start_analyze_button = (Button) findViewById(R.id.button_start_match);
        match_text = (TextView) findViewById(R.id.textView_match_code);
        match_template = (TextView) findViewById(R.id.textView_match_text);
        iv_placeholder0 = (View) findViewById(R.id.iv_placeholder0);
        iv_placeholder1 = (View) findViewById(R.id.iv_placeholder1);
        rb_boundary = (RadioButton) findViewById(R.id.radio_boundary);
        rb_skeleton = (RadioButton) findViewById(R.id.radio_skeleton);

        iv_placeholder0.setVisibility(View.VISIBLE);
        iv_placeholder1.setVisibility(View.VISIBLE);
        iv_chaincode.setVisibility(View.GONE);
        match_template.setVisibility(View.GONE);
        //showLoading();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.font_sample_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        photo_selection_spinner.setAdapter(adapter);

        photo_selection_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                iv_placeholder0.setVisibility(View.VISIBLE);
                iv_placeholder1.setVisibility(View.VISIBLE);
                iv_chaincode.setVisibility(View.GONE);
                match_text.setText("");
                switch (i) {
                    case 0:
                        break;
                    case 1:
                        dispatchTakePictureIntent();
                        break;
                    case 2:
                        changeImage(R.drawable._33);
                        break;
                    case 3:
                        changeImage(R.drawable._34);
                        break;
                    case 4:
                        changeImage(R.drawable._35);
                        break;
                    case 5:
                        changeImage(R.drawable._36);
                        break;
                    case 6:
                        changeImage(R.drawable._37);
                        break;
                    case 7:
                        changeImage(R.drawable._38);
                        break;
                    case 8:
                        changeImage(R.drawable._39);
                        break;
                    case 9:
                        changeImage(R.drawable._40);
                        break;
                    case 10:
                        changeImage(R.drawable._41);
                        break;
                    case 11:
                        changeImage(R.drawable._42);
                        break;
                    case 12:
                        changeImage(R.drawable._43);
                        break;
                    case 13:
                        changeImage(R.drawable._44);
                        break;
                    case 14:
                        changeImage(R.drawable._45);
                        break;
                    case 15:
                        changeImage(R.drawable._46);
                        break;
                    case 16:
                        changeImage(R.drawable._47);
                        break;
                    case 17:
                        changeImage(R.drawable._48);
                        break;
                    case 18:
                        changeImage(R.drawable._49);
                        break;
                    case 19:
                        changeImage(R.drawable._50);
                        break;
                    case 20:
                        changeImage(R.drawable._51);
                        break;
                    case 21:
                        changeImage(R.drawable._52);
                        break;
                    case 22:
                        changeImage(R.drawable._53);
                        break;
                    case 23:
                        changeImage(R.drawable._54);
                        break;
                    case 24:
                        changeImage(R.drawable._55);
                        break;
                    case 25:
                        changeImage(R.drawable._56);
                        break;
                    case 26:
                        changeImage(R.drawable._57);
                        break;
                    case 27:
                        changeImage(R.drawable._58);
                        break;
                    case 28:
                        changeImage(R.drawable._59);
                        break;
                    case 29:
                        changeImage(R.drawable._60);
                        break;
                    case 30:
                        changeImage(R.drawable._61);
                        break;
                    case 31:
                        changeImage(R.drawable._62);
                        break;
                    case 32:
                        changeImage(R.drawable._63);
                        break;
                    case 33:
                        changeImage(R.drawable._64);
                        break;
                    case 34:
                        changeImage(R.drawable._65);
                        break;
                    case 35:
                        changeImage(R.drawable._66);
                        break;
                    case 36:
                        changeImage(R.drawable._67);
                        break;
                    case 37:
                        changeImage(R.drawable._68);
                        break;
                    case 38:
                        changeImage(R.drawable._69);
                        break;
                    case 39:
                        changeImage(R.drawable._70);
                        break;
                    case 40:
                        changeImage(R.drawable._71);
                        break;
                    case 41:
                        changeImage(R.drawable._72);
                        break;
                    case 42:
                        changeImage(R.drawable._73);
                        break;
                    case 43:
                        changeImage(R.drawable._74);
                        break;
                    case 44:
                        changeImage(R.drawable._75);
                        break;
                    case 45:
                        changeImage(R.drawable._76);
                        break;
                    case 46:
                        changeImage(R.drawable._77);
                        break;
                    case 47:
                        changeImage(R.drawable._78);
                        break;
                    case 48:
                        changeImage(R.drawable._79);
                        break;
                    case 49:
                        changeImage(R.drawable._80);
                        break;
                    case 50:
                        changeImage(R.drawable._81);
                        break;
                    case 51:
                        changeImage(R.drawable._82);
                        break;
                    case 52:
                        changeImage(R.drawable._83);
                        break;
                    case 53:
                        changeImage(R.drawable._84);
                        break;
                    case 54:
                        changeImage(R.drawable._85);
                        break;
                    case 55:
                        changeImage(R.drawable._86);
                        break;
                    case 56:
                        changeImage(R.drawable._87);
                        break;
                    case 57:
                        changeImage(R.drawable._88);
                        break;
                    case 58:
                        changeImage(R.drawable._89);
                        break;
                    case 59:
                        changeImage(R.drawable._90);
                        break;
                    case 60:
                        changeImage(R.drawable._91);
                        break;
                    case 61:
                        changeImage(R.drawable._92);
                        break;
                    case 62:
                        changeImage(R.drawable._93);
                        break;
                    case 63:
                        changeImage(R.drawable._94);
                        break;
                    case 64:
                        changeImage(R.drawable._95);
                        break;
                    case 65:
                        changeImage(R.drawable._96);
                        break;
                    case 66:
                        changeImage(R.drawable._97);
                        break;
                    case 67:
                        changeImage(R.drawable._98);
                        break;
                    case 68:
                        changeImage(R.drawable._99);
                        break;
                    case 69:
                        changeImage(R.drawable._100);
                        break;
                    case 70:
                        changeImage(R.drawable._101);
                        break;
                    case 71:
                        changeImage(R.drawable._102);
                        break;
                    case 72:
                        changeImage(R.drawable._103);
                        break;
                    case 73:
                        changeImage(R.drawable._104);
                        break;
                    case 74:
                        changeImage(R.drawable._105);
                        break;
                    case 75:
                        changeImage(R.drawable._106);
                        break;
                    case 76:
                        changeImage(R.drawable._107);
                        break;
                    case 77:
                        changeImage(R.drawable._108);
                        break;
                    case 78:
                        changeImage(R.drawable._109);
                        break;
                    case 79:
                        changeImage(R.drawable._110);
                        break;
                    case 80:
                        changeImage(R.drawable._111);
                        break;
                    case 81:
                        changeImage(R.drawable._112);
                        break;
                    case 82:
                        changeImage(R.drawable._113);
                        break;
                    case 83:
                        changeImage(R.drawable._114);
                        break;
                    case 84:
                        changeImage(R.drawable._115);
                        break;
                    case 85:
                        changeImage(R.drawable._116);
                        break;
                    case 86:
                        changeImage(R.drawable._117);
                        break;
                    case 87:
                        changeImage(R.drawable._118);
                        break;
                    case 88:
                        changeImage(R.drawable._119);
                        break;
                    case 89:
                        changeImage(R.drawable._120);
                        break;
                    case 90:
                        changeImage(R.drawable._121);
                        break;
                    case 91:
                        changeImage(R.drawable._122);
                        break;
                    case 92:
                        changeImage(R.drawable._123);
                        break;
                    case 93:
                        changeImage(R.drawable._124);
                        break;
                    case 94:
                        changeImage(R.drawable._125);
                        break;
                    case 95:
                        changeImage(R.drawable._126);
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        start_analyze_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photo_selection_spinner.getSelectedItemPosition() != 0) {
                    showLoading();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // proses
                            final FreemanChainCode fcc = new FreemanChainCode();

                            if (rb_boundary.isChecked()) {
                                fcc.extractChainCode(fcc.turnIntoBW(currentBitmap, threshold_seekbar.getProgress() * 255 / 100));
                                FreemanRecognizer numberMatcher = new FreemanRecognizer();
                                float[] testResult = numberMatcher.getCodeOccurenceDifference(fcc);

                                int min = 0;
                                float minVal = testResult[0];

                                for (int i = 1; i < testResult.length; i++) {
                                    if (testResult[i] < minVal) {
                                        min = i;
                                        minVal = testResult[i];
                                    }
                                }

                                final int min_text = min;

                                final Bitmap result = fcc.drawBitmapFromChainCode(currentBitmap);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        match_text.setText(String.valueOf(min_text));
                                        match_text.setVisibility(View.VISIBLE);
                                        match_template.setVisibility(View.VISIBLE);

                                        iv_chaincode.setImageBitmap(result);
                                        iv_placeholder0.setVisibility(View.GONE);
                                        iv_placeholder1.setVisibility(View.GONE);
                                        iv_chaincode.setVisibility(View.VISIBLE);

                                        hideLoading();
                                    }
                                });
                            } else {
                                ArrayList<Skeleton> skeletonizationResult = Thinning.getSkeletonObject(currentBitmap);
                                final Bitmap result = drawOver(currentBitmap, skeletonizationResult);
                                final char matchResult = SkeletonHeuristics.recognize(skeletonizationResult);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (matchResult == ' ') {
                                            match_text.setText("<space>");
                                        } else {
                                            match_text.setText(String.valueOf(matchResult));
                                        }
                                        match_text.setVisibility(View.VISIBLE);
                                        match_template.setVisibility(View.VISIBLE);
                                        iv_chaincode.setImageBitmap(result);
//                                        iv_chaincode.setImageBitmap(currentBitmap);
                                        iv_placeholder0.setVisibility(View.GONE);
                                        iv_placeholder1.setVisibility(View.GONE);
                                        iv_chaincode.setVisibility(View.VISIBLE);
                                        hideLoading();
                                    }
                                });
                            }
                        }
                    }).start();
                } else {
                    Toast.makeText(getApplicationContext(), "Please select which photo to analyze.", Toast.LENGTH_LONG).show();
                }
            }
        });

        gallery_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
    }

    private void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    private void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void changeImage(int newImageId) {
        Drawable newImage = ResourcesCompat.getDrawable(
                getResources(), newImageId, null);
        iv_photo.setImageDrawable(newImage);
        currentBitmap = BitmapFactory.decodeResource(getResources(), newImageId);
    }

    public Bitmap drawOver(Bitmap img, ArrayList<Skeleton> skeletons) {
        Bitmap newImg = img.copy(Bitmap.Config.ARGB_8888, true);
        int skeletonColor = Color.YELLOW;
        int endColor = Color.RED;
        int branchColor = Color.BLUE;

        for (Skeleton skeleton: skeletons) {
            for (Point p : skeleton.getSkeleton()) {
                try {
                    newImg.setPixel(p.x, p.y, skeletonColor);
                } catch (IllegalArgumentException e) {
                    continue;
                }
            }
            for (Point p : skeleton.getEndPoints()) {
                try {
                    newImg.setPixel(p.x, p.y, endColor);
                } catch (IllegalArgumentException e) {
                    continue;
                }
            }
            for (Point p : skeleton.getIntersections()) {
                try {
                    newImg.setPixel(p.x, p.y, branchColor);
                } catch (IllegalArgumentException e) {
                    continue;
                }
            }
        }

        return newImg;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp =
                DateFormat.getDateTimeInstance().format(new Date());//new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //ex.printStackTrace();
                Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.tanto.imagehistogram.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            } else {
                Toast.makeText(this, "photoFile is null!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            galleryAddPic();

            currentBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            iv_photo.setImageBitmap(currentBitmap);
        } else if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                currentBitmap = BitmapFactory.decodeStream(imageStream);
                iv_photo.setImageBitmap(currentBitmap);
                photo_selection_spinner.setSelection(96);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
