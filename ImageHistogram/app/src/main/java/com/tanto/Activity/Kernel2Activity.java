package com.tanto.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tanto.imagehistogram.R;
import com.tanto.kernel.Kernel;

import java.io.FileNotFoundException;
import java.io.InputStream;

import static android.view.View.GONE;
import static com.tanto.Activity.ObjectDetectActivity.PICK_IMAGE;

public class Kernel2Activity extends AppCompatActivity {

    private ImageView ivOrigin;
    private ImageView ivSobel;
    private ImageView ivPrewitt;
    private ImageView ivRoberts;
    private ImageView ivFreiChen;
    private ImageView ivCustom;

    private TextView tvOrigin;
    private TextView tvSobel;
    private TextView tvPrewitt;
    private TextView tvRoberts;
    private TextView tvFreiChen;
    private TextView tvCustom;

    private LinearLayout inputsLayout;
    private CheckBox checkBoxCustomKernel;

    private Kernel kernel;
    private Bitmap currentBitmap;
    final TextInputEditText[] kernelElements = new TextInputEditText[9];
    private double[][] workingKernel =  {
            {0,0,0},
            {0,0,0},
            {0,0,0}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kernel2);

        checkBoxCustomKernel = (CheckBox) findViewById(R.id.check_custom_kernel);
        checkBoxCustomKernel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkBoxCustomKernel.isChecked()) {
                    inputsLayout.setVisibility(View.GONE);
                } else {
                    inputsLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        inputsLayout = (LinearLayout) findViewById(R.id.kernel_inputs_layout);
        inputsLayout.setVisibility(GONE);

        tvOrigin = (TextView) findViewById(R.id.textview_ckernel_origin);
        tvSobel = (TextView) findViewById(R.id.textview_ckernel_sobel);
        tvPrewitt = (TextView) findViewById(R.id.textview_ckernel_prewitt);
        tvRoberts = (TextView) findViewById(R.id.textview_ckernel_Roberts);
        tvFreiChen = (TextView) findViewById(R.id.textview_ckernel_frei_chen);
        tvCustom = (TextView) findViewById(R.id.textview_ckernel_custom);

        tvOrigin.setVisibility(GONE);
        tvSobel.setVisibility(GONE);
        tvPrewitt.setVisibility(GONE);
        tvRoberts.setVisibility(GONE);
        tvFreiChen.setVisibility(GONE);
        tvCustom.setVisibility(GONE);
;
        kernelElements[0] = (TextInputEditText) findViewById(R.id.text_ckernel_1);
        kernelElements[1] = (TextInputEditText) findViewById(R.id.text_ckernel_2);
        kernelElements[2] = (TextInputEditText) findViewById(R.id.text_ckernel_3);
        kernelElements[3] = (TextInputEditText) findViewById(R.id.text_ckernel_4);
        kernelElements[4] = (TextInputEditText) findViewById(R.id.text_ckernel_5);
        kernelElements[5] = (TextInputEditText) findViewById(R.id.text_ckernel_6);
        kernelElements[6] = (TextInputEditText) findViewById(R.id.text_ckernel_7);
        kernelElements[7] = (TextInputEditText) findViewById(R.id.text_ckernel_8);
        kernelElements[8] = (TextInputEditText) findViewById(R.id.text_ckernel_9);

        final ProgressBar sobelProgress = (ProgressBar) findViewById(R.id.progress_bar_sobel);
        sobelProgress.setVisibility(GONE);

        final ProgressBar prewittProgress = (ProgressBar) findViewById(R.id.progress_bar_prewitt);
        prewittProgress.setVisibility(GONE);

        final ProgressBar robertsProgress = (ProgressBar) findViewById(R.id.progress_bar_roberts);
        robertsProgress.setVisibility(GONE);

        final ProgressBar freichenProgress = (ProgressBar) findViewById(R.id.progress_bar_frei_chen);
        freichenProgress.setVisibility(GONE);

        final ProgressBar customProgress = (ProgressBar) findViewById(R.id.progress_bar_ckernel);
        customProgress.setVisibility(GONE);

        Button clearKernelButton = (Button) findViewById(R.id.button_clear_ckernel);
        clearKernelButton.setVisibility(GONE);

        clearKernelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < kernelElements.length; i++) {
                    kernelElements[i].setText("");
                }
            }
        });

        Button galleryButton = (Button) findViewById(R.id.button_gallery_kernel);

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });

        Button applyButton = (Button) findViewById(R.id.button_apply_ckernel);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sobelProgress.setVisibility(View.VISIBLE);
                prewittProgress.setVisibility(View.VISIBLE);
                robertsProgress.setVisibility(View.VISIBLE);
                freichenProgress.setVisibility(View.VISIBLE);
                ivSobel.setVisibility(GONE);
                ivPrewitt.setVisibility(GONE);
                ivRoberts.setVisibility(GONE);
                ivFreiChen.setVisibility(GONE);
                ivCustom.setVisibility(GONE);

                tvSobel.setVisibility(View.VISIBLE);
                tvPrewitt.setVisibility(View.VISIBLE);
                tvRoberts.setVisibility(View.VISIBLE);
                tvFreiChen.setVisibility(View.VISIBLE);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO
                        final Bitmap sobelResult = kernel.convolveDouble(currentBitmap, Kernel.SOBEL_X_KERNEL, Kernel.SOBEL_Y_KERNEL, true);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ivSobel.setImageBitmap(sobelResult);
                                ivSobel.setVisibility(View.VISIBLE);
                                sobelProgress.setVisibility(View.GONE);
                            }
                        });
                    }
                }).start();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO
                        final Bitmap prewittResult = kernel.convolveDouble(currentBitmap, Kernel.PREWITT_X_KERNEL, Kernel.PREWITT_Y_KERNEL, true);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ivPrewitt.setImageBitmap(prewittResult);
                                ivPrewitt.setVisibility(View.VISIBLE);
                                prewittProgress.setVisibility(View.GONE);
                            }
                        });
                    }
                }).start();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO
                        final Bitmap robertsResult = kernel.convolveDouble(currentBitmap, Kernel.ROBERT_X_KERNEL, Kernel.ROBERT_Y_KERNEL, true);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ivRoberts.setImageBitmap(robertsResult);
                                ivRoberts.setVisibility(View.VISIBLE);
                                robertsProgress.setVisibility(View.GONE);
                            }
                        });
                    }
                }).start();


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO
                        final Bitmap freichenResult = kernel.FreiChenEdgeDetection(currentBitmap, true);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ivFreiChen.setImageBitmap(freichenResult);
                                ivFreiChen.setVisibility(View.VISIBLE);
                                freichenProgress.setVisibility(View.GONE);
                            }
                        });
                    }
                }).start();

                // custom
                if (checkBoxCustomKernel.isChecked()) {
                    // apply kernel from input
                    tvCustom.setVisibility(View.VISIBLE);
                    customProgress.setVisibility(View.VISIBLE);
                    for (int i = 0; i < workingKernel.length; i++) {
                        for (int j = 0; j < workingKernel[i].length; j++) {
                            workingKernel[i][j] = Double.valueOf(kernelElements[(i*3)+j].getText().toString());
                        }
                    }

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final Bitmap result = kernel.convolve(currentBitmap, workingKernel, false);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ivCustom.setImageBitmap(result);
                                    ivCustom.setVisibility(View.VISIBLE);
                                    customProgress.setVisibility(View.GONE);
                                }
                            });
                        }
                    }).start();
                }

            }
        });

        ivOrigin = (ImageView) findViewById(R.id.iv_kernel_origin);
        ivSobel = (ImageView) findViewById(R.id.iv_kernel_sobel);
        ivPrewitt = (ImageView) findViewById(R.id.iv_kernel_prewitt);
        ivRoberts = (ImageView) findViewById(R.id.iv_kernel_roberts);
        ivFreiChen = (ImageView) findViewById(R.id.iv_kernel_frei_chen);
        ivCustom = (ImageView) findViewById(R.id.iv_kernel_custom);

        ivSobel.setVisibility(GONE);
        ivPrewitt.setVisibility(GONE);
        ivRoberts.setVisibility(GONE);
        ivFreiChen.setVisibility(GONE);
        ivCustom.setVisibility(GONE);

        kernel = new Kernel();
        currentBitmap = null;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                currentBitmap = BitmapFactory.decodeStream(imageStream);

                if (currentBitmap.getWidth() > currentBitmap.getHeight()) {
                    if (currentBitmap.getWidth() > 500) {
                        currentBitmap = Bitmap.createScaledBitmap(currentBitmap,
                                currentBitmap.getWidth()/(currentBitmap.getWidth()/500),
                                currentBitmap.getHeight()/(currentBitmap.getWidth()/500),
                                true);
                    }
                } else {
                    if (currentBitmap.getHeight() > 500) {
                        currentBitmap = Bitmap.createScaledBitmap(currentBitmap,
                                currentBitmap.getWidth()/(currentBitmap.getHeight()/500),
                                currentBitmap.getHeight()/(currentBitmap.getHeight()/500),
                                true);
                    }
                }

                ivOrigin.setImageBitmap(currentBitmap);
                ivSobel.setVisibility(GONE);
                ivPrewitt.setVisibility(GONE);
                ivRoberts.setVisibility(GONE);
                ivFreiChen.setVisibility(GONE);
                ivCustom.setVisibility(GONE);

                tvOrigin.setVisibility(View.VISIBLE);
                tvSobel.setVisibility(GONE);
                tvPrewitt.setVisibility(GONE);
                tvRoberts.setVisibility(GONE);
                tvFreiChen.setVisibility(GONE);
                tvCustom.setVisibility(GONE);

                checkBoxCustomKernel.setChecked(false);
                inputsLayout.setVisibility(GONE);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
