package com.tanto.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tanto.imagehistogram.R;

import java.io.FileOutputStream;

public class HandwriteActivity extends AppCompatActivity {
    private DrawingView dv;
    private Button clearButton;
    private Button nextButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_handwrite);

        dv = (DrawingView) findViewById(R.id.draw_view);
        clearButton = (Button) findViewById(R.id.clear_button);
        nextButton = (Button) findViewById(R.id.next_button);

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dv.clearDrawing();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap image = dv.getmBitmap();

                Bitmap imageWithBG = Bitmap.createBitmap(image.getWidth(), image.getHeight(),image.getConfig());  // Create another image the same size
                imageWithBG.eraseColor(Color.WHITE);  // set its background to white, or whatever color you want
                Canvas canvas = new Canvas(imageWithBG);  // create a canvas to draw on the new image
                canvas.drawBitmap(image, 0f, 0f, null); // draw old image on the background
                image.recycle();  // clear out old image

                try {
                    //Write file
                    String filename = "bitmap.png";
                    FileOutputStream stream = getApplicationContext().openFileOutput(filename, Context.MODE_PRIVATE);
                    imageWithBG.compress(Bitmap.CompressFormat.PNG, 100, stream);

                    //Cleanup
                    stream.close();
                    imageWithBG.recycle();

                    //Pop intent
                    Intent intent = new Intent(getApplicationContext(), HWRecognitionActivity.class);
                    intent.putExtra("image", filename);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
