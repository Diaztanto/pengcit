package com.tanto.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.tanto.imagehistogram.HistogramData;
import com.tanto.imagehistogram.HistogramUtils;
import com.tanto.imagehistogram.R;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LinSandHeqActivity extends AppCompatActivity {

    private static final int COLOR_BOUNDARIES = 256;
    static final int REQUEST_TAKE_PHOTO = 1;
    String mCurrentPhotoPath;

    private ImageView imageView;
    private ImageView processedView;
    private SeekBar weightBar;

    private Button buttonApply;

    private BarChart beforeChart;
    private BarChart afterChart;
    private ProgressBar progressBar;

    private HistogramData originalHistogram;
    private HistogramData equalizedHistogram;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.image_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        final Spinner chart_spinner = (Spinner) findViewById(R.id.chart_spinner);

//        adapter = null;
        adapter = ArrayAdapter.createFromResource(this,
                R.array.chart_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chart_spinner.setAdapter(adapter);

        final Spinner method_spinner = (Spinner) findViewById(R.id.method_spinner);

//        adapter = null;
        adapter = ArrayAdapter.createFromResource(this,
                R.array.method_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        method_spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0  :
                        break;
                    case 1  :
                        dispatchTakePictureIntent();
                        break;
                    case 2  :
                        changeImage(R.drawable.photo_1);
                        break;
                    case 3  :
                        changeImage(R.drawable.photo_2);
                        break;
                    case 4  :
                        changeImage(R.drawable.photo_3);
                        break;
                    case 5  :
                        changeImage(R.drawable.photo_4);
                        break;
                    case 6  :
                        changeImage(R.drawable.photo_5);
                        break;
                    case 7  :
                        changeImage(R.drawable.photo_6);
                        break;
                    case 8  :
                        changeImage(R.drawable.photo_7);
                        break;
                    case 9  :
                        changeImage(R.drawable.photo_8);
                        break;
                    case 10 :
                        changeImage(R.drawable.sample_1);
                        break;
                    default:
                        changeImage(R.drawable.sample_2);
                        break;
                }

                chart_spinner.setSelection(0);
                method_spinner.setSelection(0);

                if (i > 1) {
                    showLoading();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            processBitmap(1);
                        }
                    }).start();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        chart_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0  :
                        visualizeHistogram('K');
                        break;
                    case 1  :
                        visualizeHistogram('R');
                        break;
                    case 2  :
                        visualizeHistogram('G');
                        break;
                    default :
                        visualizeHistogram('B');
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        method_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                if (i > 0){
                    showLoading();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            processBitmap(i);
                        }
                    }).start();

                    chart_spinner.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        weightBar = (SeekBar) findViewById(R.id.weight_seekbar);
        buttonApply = (Button) findViewById(R.id.button_apply);
        buttonApply.setVisibility(View.GONE);
        imageView = (ImageView) findViewById(R.id.iv_photo);
        processedView = (ImageView) findViewById(R.id.iv_processed);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        beforeChart = (BarChart) findViewById(R.id.before_chart);
        afterChart = (BarChart) findViewById(R.id.after_chart);

        beforeChart.getAxisLeft().setDrawLabels(false);
        afterChart.getAxisLeft().setDrawLabels(false);

        beforeChart.getAxisRight().setDrawLabels(false);
        afterChart.getAxisRight().setDrawLabels(false);

        weightBar.setProgress(50);

        // proses intent dari activity sebelah

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            showLoading();
            //Toast.makeText(this, "Here we go.", Toast.LENGTH_LONG).show();

            int [] numArr = extras.getIntArray("specNumArr");
            int imageId = extras.getInt("imageID");
            Bitmap temp;


            if (imageId == 1) {
                String imgAddr = extras.getString("imageAddr");
                temp = BitmapFactory.decodeFile(imgAddr);
                imageView.setImageBitmap(temp);
            } else {
                switch (imageId) {
                    case 2  :
                        changeImage(R.drawable.photo_1);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.photo_1);
                        break;
                    case 3  :
                        changeImage(R.drawable.photo_2);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.photo_2);
                        break;
                    case 4  :
                        changeImage(R.drawable.photo_3);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.photo_3);
                        break;
                    case 5  :
                        changeImage(R.drawable.photo_4);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.photo_4);
                        break;
                    case 6  :
                        changeImage(R.drawable.photo_5);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.photo_5);
                        break;
                    case 7  :
                        changeImage(R.drawable.photo_6);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.photo_6);
                        break;
                    case 8  :
                        changeImage(R.drawable.photo_7);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.photo_7);
                        break;
                    case 9  :
                        changeImage(R.drawable.photo_8);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.photo_8);
                        break;
                    case 10 :
                        changeImage(R.drawable.sample_1);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.sample_1);
                        break;
                    default:
                        changeImage(R.drawable.sample_2);
                        temp = BitmapFactory.decodeResource(getResources(), R.drawable.sample_2);
                        break;
                }
            }

            // Hasil Ekualisasi Histogram
            originalHistogram = HistogramUtils.generateBitmapHistogramData(temp, COLOR_BOUNDARIES);

            final Bitmap result = HistogramUtils.HistogramMatcher.matchHistogram(temp, numArr, COLOR_BOUNDARIES);

            equalizedHistogram = HistogramUtils.generateBitmapHistogramData(result, COLOR_BOUNDARIES);


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    changeImageWithBitmap(result);
                    hideLoading();
                }
            });
        }
    }



    private void hideLoading() {
        progressBar.setVisibility(View.GONE);
        showAllView();
    }

    private void showLoading() {
        hideAllView();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideAllView() {
        imageView.setVisibility(View.GONE);
        processedView.setVisibility(View.GONE);
        beforeChart.setVisibility(View.GONE);
        afterChart.setVisibility(View.GONE);/*
        blueChart.setVisibility(View.GONE);
        grayChart.setVisibility(View.GONE);*/
    }

    private void showAllView() {
        imageView.setVisibility(View.VISIBLE);
        processedView.setVisibility(View.VISIBLE);
        beforeChart.setVisibility(View.VISIBLE);
        afterChart.setVisibility(View.VISIBLE);/*
        blueChart.setVisibility(View.VISIBLE);
        grayChart.setVisibility(View.VISIBLE);*/
    }

    private void changeImage(int newImageId) {
        Drawable newImage = ResourcesCompat.getDrawable(
                getResources(), newImageId, null);
        imageView.setImageDrawable(newImage);
    }


    private void changeImageWithBitmap(Bitmap bitmap) {
        processedView.setImageBitmap(bitmap);
    }

    private void processBitmap(int i) {
        BitmapDrawable bd = (BitmapDrawable) imageView.getDrawable();
        Bitmap originalBitmap = bd.getBitmap();
        this.originalHistogram = HistogramUtils.generateBitmapHistogramData(originalBitmap, COLOR_BOUNDARIES);
        final Bitmap newBitmap;
        switch (i) {
            case 1  :
                newBitmap = HistogramUtils.HistogramEqualizer.averageEqualization(originalBitmap, COLOR_BOUNDARIES, ((float) weightBar.getProgress() / (float) 50));
                break;
            case 2  :
//        Histogram Equalization on each RGB Channel
                newBitmap = HistogramUtils.HistogramEqualizer.rgbEqualization(
                            originalBitmap, COLOR_BOUNDARIES, ((float) weightBar.getProgress() / (float) 50));
                break;
            default  :
//        Histogram Stretching
                newBitmap = HistogramUtils.HistogramStretcher.stretchHistogram(
                            originalBitmap, COLOR_BOUNDARIES);
                break;
        }

//        Histogram Equalization on each RGB Channel
//        final Bitmap newBitmap = HistogramUtils.rgbEqualization(
//                                    originalBitmap, COLOR_BOUNDARIES);

//        Histogram Stretching
//        final Bitmap newBitmap = HistogramUtils.stretchHistogram(
//                originalBitmap, COLOR_BOUNDARIES);

        this.equalizedHistogram = HistogramUtils.generateBitmapHistogramData(
                newBitmap, COLOR_BOUNDARIES);
        visualizeHistogram('K');

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                changeImageWithBitmap(newBitmap);
                hideLoading();
            }
        });
    }

    private void visualizeHistogram(char code) {
        List<BarEntry> beforeEntries = new ArrayList<>();
        List<BarEntry> afterEntries = new ArrayList<>();/*
        List<BarEntry> blueEntries = new ArrayList<>();
        List<BarEntry> grayEntries = new ArrayList<>();*/

        if (originalHistogram != null) {
            int[] beforePixel;
            int[] afterPixel;

            switch (code) {
                case 'R'    :
                    beforePixel = originalHistogram.getRedPixelHistogram();
                    afterPixel = equalizedHistogram.getRedPixelHistogram();
                    break;
                case 'G'    :
                    beforePixel = originalHistogram.getGreenPixelHistogram();
                    afterPixel = equalizedHistogram.getGreenPixelHistogram();
                    break;
                case 'B'    :
                    beforePixel = originalHistogram.getBluePixelHistogram();
                    afterPixel = equalizedHistogram.getBluePixelHistogram();
                    break;
                default     :
                    beforePixel = originalHistogram.getGrayPixelHistogram();
                    afterPixel = equalizedHistogram.getGrayPixelHistogram();
                    break;
            }/*
        int[] bluePixel = histogramData.getGreenPixelHistogram();
        int[] grayPixel = histogramData.getGrayPixelHistogram();*/

            for (int i = 0; i < COLOR_BOUNDARIES; ++i) {
                beforeEntries.add(new BarEntry(i, beforePixel[i]));
                afterEntries.add(new BarEntry(i, afterPixel[i]));/*
            blueEntries.add(new BarEntry(i, bluePixel[i]));
            grayEntries.add(new BarEntry(i, grayPixel[i]));*/
            }

            BarDataSet beforeDataSet = new BarDataSet(beforeEntries, "Before");
            BarDataSet afterDataSet = new BarDataSet(afterEntries, "After");

            switch (code) {
                case 'R'    :
                    beforeDataSet.setColor(Color.RED);
                    afterDataSet.setColor(Color.RED);
                    break;
                case 'G'    :
                    beforeDataSet.setColor(Color.GREEN);
                    afterDataSet.setColor(Color.GREEN);
                    break;
                case 'B'    :
                    beforeDataSet.setColor(Color.BLUE);
                    afterDataSet.setColor(Color.BLUE);
                    break;
                default    :
                    beforeDataSet.setColor(Color.GRAY);
                    afterDataSet.setColor(Color.GRAY);
                    break;
            }/*
        blueDataSet.setColor(Color.BLUE);
        grayDataSet.setColor(Color.GRAY);*/

            BarData beforeData = new BarData(beforeDataSet);
            BarData afterData = new BarData(afterDataSet);/*
        BarData blueData = new BarData(blueDataSet);
        BarData grayData = new BarData(grayDataSet);
*/
            beforeChart.setData(beforeData);
            afterChart.setData(afterData);/*
        blueChart.setData(blueData);
        grayChart.setData(grayData);*/

            beforeChart.invalidate();
            afterChart.invalidate();/*
        blueChart.invalidate();
        grayChart.invalidate();*/
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp =
                DateFormat.getDateTimeInstance().format(new Date());//new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //ex.printStackTrace();
                Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.tanto.imagehistogram.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            } else {
                Toast.makeText(this, "photoFile is null!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            galleryAddPic();

            imageView.setImageBitmap(BitmapFactory.decodeFile(mCurrentPhotoPath));

            showLoading();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    processBitmap(1);
                }
            }).start();
        }
    }
}
