package com.tanto.HandwritingDetection;

import android.graphics.Point;

import com.tanto.SkeletonDetection.Skeleton;

public class SymbolFeatureVector {
    private char symbol;
    private int numOfEndpoints;
    private int numOfIntersections;
    private int numOfEndpointQ1;
    private int numOfEndpointQ2;
    private int numOfEndpointQ3;
    private int numOfEndpointQ4;
    private int numOfIntersectionQ1;
    private int numOfIntersectionQ2;
    private int numOfIntersectionQ3;
    private int numOfIntersectionQ4;
    private double q1Ratio;
    private double q2Ratio;
    private double q3Ratio;
    private double q4Ratio;
    private double boundaryRatio;

    public SymbolFeatureVector(char symbol,
                               int ep, int ip,
                               int epq1, int epq2, int epq3, int epq4,
                               int ipq1, int ipq2, int ipq3, int ipq4,
                               double rq1, double rq2, double rq3, double rq4,
                               double boundaryRatio)
    {
        this.symbol = symbol;
        numOfEndpoints = ep;
        numOfIntersections = ip;
        numOfEndpointQ1 = epq1;
        numOfEndpointQ2 = epq2;
        numOfEndpointQ3 = epq3;
        numOfEndpointQ4 = epq4;
        numOfIntersectionQ1 = ipq1;
        numOfIntersectionQ2 = ipq2;
        numOfIntersectionQ3 = ipq3;
        numOfIntersectionQ4 = ipq4;
        q1Ratio = rq1;
        q2Ratio = rq2;
        q3Ratio = rq3;
        q4Ratio = rq4;
        this.boundaryRatio = boundaryRatio;

    }
    public SymbolFeatureVector(Skeleton sk) {
        this.symbol = ' ';
        numOfEndpoints = sk.getEndPoints().size();
        numOfIntersections = sk.getIntersections().size();
        numOfEndpointQ1 = 0;
        numOfEndpointQ2 = 0;
        numOfEndpointQ3 = 0;
        numOfEndpointQ4 = 0;
        numOfIntersectionQ1 = 0;
        numOfIntersectionQ2 = 0;
        numOfIntersectionQ3 = 0;
        numOfIntersectionQ4 = 0;

        Point origin = sk.getObjectOriginPoint();
        for (Point p: sk.getEndPoints()) {
            int qLocation = checkQuadrant(p, origin);
            switch(qLocation) {
                case 1:
                    numOfEndpointQ1++;
                    break;
                case 2:
                    numOfEndpointQ2++;
                    break;
                case 3:
                    numOfEndpointQ3++;
                    break;
                case 4:
                    numOfEndpointQ4++;
                    break;
            }
        }

        for (Point p: sk.getIntersections()) {
            int qLocation = checkQuadrant(p, origin);
            switch(qLocation) {
                case 1:
                    numOfIntersectionQ1++;
                    break;
                case 2:
                    numOfIntersectionQ2++;
                    break;
                case 3:
                    numOfIntersectionQ3++;
                    break;
                case 4:
                    numOfIntersectionQ4++;
                    break;
            }
        }

        double q1Count = 0;
        double q2Count = 0;
        double q3Count = 0;
        double q4Count = 0;
        for (Point p: sk.getSkeleton()) {
            int qLocation = checkQuadrant(p, origin);
            switch(qLocation) {
                case 1:
                    q1Count++;
                    break;
                case 2:
                    q2Count++;
                    break;
                case 3:
                    q3Count++;
                    break;
                case 4:
                    q4Count++;
                    break;
            }
        }
        q1Ratio = q1Count / sk.getSkeleton().size();
        q2Ratio = q2Count / sk.getSkeleton().size();
        q3Ratio = q3Count / sk.getSkeleton().size();
        q4Ratio = q4Count / sk.getSkeleton().size();

        boundaryRatio = sk.getBoundaryWidthHeightRatio();
    }

    public double compareSimilarityDistance(SymbolFeatureVector B) {
        double distance = 0;
        distance += Math.abs(this.numOfEndpoints - B.numOfEndpoints);
        distance += Math.abs(this.numOfIntersections - B.numOfIntersections);

        distance += Math.abs(this.numOfEndpointQ1 - B.numOfEndpointQ1);
        distance += Math.abs(this.numOfEndpointQ2 - B.numOfEndpointQ2);
        distance += Math.abs(this.numOfEndpointQ3 - B.numOfEndpointQ3);
        distance += Math.abs(this.numOfEndpointQ4 - B.numOfEndpointQ4);

        distance += Math.abs(this.numOfIntersectionQ1 - B.numOfIntersectionQ1);
        distance += Math.abs(this.numOfIntersectionQ2 - B.numOfIntersectionQ2);
        distance += Math.abs(this.numOfIntersectionQ3 - B.numOfIntersectionQ3);
        distance += Math.abs(this.numOfIntersectionQ4 - B.numOfIntersectionQ4);

        distance += Math.abs(this.q1Ratio - B.q1Ratio);
        distance += Math.abs(this.q2Ratio - B.q2Ratio);
        distance += Math.abs(this.q3Ratio - B.q3Ratio);
        distance += Math.abs(this.q4Ratio - B.q4Ratio);

        distance += Math.abs(this.boundaryRatio - B.boundaryRatio);
        return distance;
    }

    private int checkQuadrant(Point p, Point origin) {
        if (p.x >= origin.x && p.y <= origin.y) {
            return 1;
        } else if (p.x < origin.x && p.y <= origin.y) {
            return 2;
        } else if (p.x < origin.x && p.y > origin.y) {
            return 3;
        }
        return 4;
    }

    public char getSymbol() {
        return symbol;
    }
}
