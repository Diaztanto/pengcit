package com.tanto.HandwritingDetection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class HandwritingClassifier {
    private ArrayList<ArrayList<SymbolFeatureVector>> vectorCollections;
    public HandwritingClassifier(){
        ArrayList<SymbolFeatureVector> char_0 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_1 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_2 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_3 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_4 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_5 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_6 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_7 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_8 = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_9 = new ArrayList<>();

        ArrayList<SymbolFeatureVector> char_A = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_B = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_C = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_D = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_E = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_F = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_G = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_H = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_I = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_J = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_K = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_L = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_M = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_N = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_O = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_P = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_Q = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_R = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_S = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_T = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_U = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_V = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_W = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_X = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_Y = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_Z = new ArrayList<>();

        ArrayList<SymbolFeatureVector> char_a = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_b = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_c = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_d = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_e = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_f = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_g = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_h = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_i = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_j = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_k = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_l = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_m = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_n = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_o = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_p = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_q = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_r = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_s = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_t = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_u = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_v = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_w = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_x = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_y = new ArrayList<>();
        ArrayList<SymbolFeatureVector> char_z = new ArrayList<>();

        // handwriting set 1
        char_0.add(new SymbolFeatureVector('0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.27, 0.27, 0.28, 0.175, 0.66));
        char_1.add(new SymbolFeatureVector('1', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.51, 0.0, 0.44, 0.05, 0.03));
        char_2.add(new SymbolFeatureVector('2', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.35, 0.19, 0.11, 0.342, 0.74));
        char_3.add(new SymbolFeatureVector('3', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.337, 0.26, 0.067, 0.337, 0.5));
        char_4.add(new SymbolFeatureVector('4', 3, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0.328, 0.423, 0.0, 0.248, 0.672));
        char_5.add(new SymbolFeatureVector('5', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.231, 0.346, 0.192, 0.231, 0.769));
        char_6.add(new SymbolFeatureVector('6', 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0.09, 0.201, 0.372, 0.333, 0.323));
        char_7.add(new SymbolFeatureVector('7', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.507, 0.172, 0.133, 0.188, 0.666));
        char_8.add(new SymbolFeatureVector('8', 0, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0.304, 0.181, 0.33, 0.185, 0.643));
        char_9.add(new SymbolFeatureVector('9', 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0.427, 0.287, 0.047, 0.24, 0.448));

        char_A.add(new SymbolFeatureVector('A', 3, 3, 0, 0, 1, 2, 0, 0, 1, 2, 0.297, 0.124, 0.265, 0.314, 0.761));
        char_B.add(new SymbolFeatureVector('B', 3, 5, 0, 2, 1, 0, 0, 2, 3, 0, 0.253, 0.257, 0.308, 0.181, 0.575));
        char_C.add(new SymbolFeatureVector('C', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.192, 0.298, 0.351, 0.158, 0.574));
        char_D.add(new SymbolFeatureVector('D', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.142, 0.292, 0.345, 0.221, 0.691));
        char_E.add(new SymbolFeatureVector('E', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.08, 0.364, 0.364, 0.186, 0.69));
        char_F.add(new SymbolFeatureVector('F', 3, 1, 2, 0, 1, 0, 0, 1, 0, 0, 0.254, 0.531, 0.215, 0.0, 0.857));
        char_G.add(new SymbolFeatureVector('G', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.155, 0.276, 0.345, 0.224, 0.721));
        char_H.add(new SymbolFeatureVector('H', 4, 2, 1, 1, 1, 1, 1, 1, 0, 0, 0.336, 0.289, 0.171, 0.204, 0.695));
        char_I.add(new SymbolFeatureVector('I', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.397, 0.109, 0.301, 0.192, 0.028));
        char_J.add(new SymbolFeatureVector('J', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.317, 0.0, 0.292, 0.392, 0.453));
        char_K.add(new SymbolFeatureVector('K', 4, 2, 1, 1, 1, 1, 0, 1, 1, 0, 0.147, 0.353, 0.258, 0.242, 0.607));
        char_L.add(new SymbolFeatureVector('L', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.0, 0.317, 0.452, 0.23, 0.764));
        char_M.add(new SymbolFeatureVector('M', 2, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0.384, 0.297, 0.162, 0.157, 1.103));
        char_N.add(new SymbolFeatureVector('N', 3, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0.313, 0.247, 0.203, 0.236, 1.182));
        char_O.add(new SymbolFeatureVector('O', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.271, 0.261, 0.251, 0.216, 0.649));
        char_P.add(new SymbolFeatureVector('P', 2, 2, 0, 1, 1, 0, 0, 1, 1, 0, 0.346, 0.352, 0.302, 0.0, 0.412));
        char_Q.add(new SymbolFeatureVector('Q', 2, 2, 0, 1, 0, 1, 0, 1, 0, 1, 0.187, 0.315, 0.245, 0.253, 0.92));
        char_R.add(new SymbolFeatureVector('R', 2, 2, 0, 0, 1, 1, 1, 1, 0, 0, 0.268, 0.399, 0.132, 0.201, 0.844));
        char_S.add(new SymbolFeatureVector('S', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.204, 0.34, 0.184, 0.272, 0.525));
        char_T.add(new SymbolFeatureVector('T', 3, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0.207, 0.469, 0.323, 0.0, 0.699));
        char_U.add(new SymbolFeatureVector('U', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.236, 0.236, 0.286, 0.243, 0.593));
        char_V.add(new SymbolFeatureVector('V', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.236, 0.24, 0.256, 0.24, 0.683));
        char_W.add(new SymbolFeatureVector('W', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.175, 0.236, 0.362, 0.228, 1.269));
        char_X.add(new SymbolFeatureVector('X', 4, 2, 1, 1, 1, 1, 2, 0, 0, 0, 0.291, 0.231, 0.225, 0.253, 0.654));
        char_Y.add(new SymbolFeatureVector('Y', 3, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0.284, 0.368, 0.348, 0.0, 0.688));
        char_Z.add(new SymbolFeatureVector('Z', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.254, 0.225, 0.337, 0.183, 0.983));

        char_a.add(new SymbolFeatureVector('a', 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0.348, 0.168, 0.248, 0.226, 0.6));
        char_b.add(new SymbolFeatureVector('b', 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0.115, 0.318, 0.292, 0.274, 0.511));
        char_c.add(new SymbolFeatureVector('c',2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.145, 0.289, 0.349, 0.217, 0.641 ));
        char_d.add(new SymbolFeatureVector('d', 3, 1, 2, 0, 0, 1, 0, 0, 0, 1, 0.258, 0.008, 0.383, 0.352, 0.542));
        char_e.add(new SymbolFeatureVector('e', 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0.176, 0.387, 0.261, 0.176, 1.121));
        char_f.add(new SymbolFeatureVector('f', 4, 2, 2, 1, 1, 0, 0, 2, 0, 0, 0.312, 0.409, 0.28, 0.0, 0.531));
        char_g.add(new SymbolFeatureVector('g', 3, 1, 2, 0, 1, 0, 1, 0, 0, 0, 0.283, 0.221, 0.159, 0.336, 0.49));
        char_h.add(new SymbolFeatureVector('h', 3, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0.179, 0.274, 0.358, 0.189, 0.51));
        char_i.add(new SymbolFeatureVector('i', 4, 0, 2, 1, 0, 1, 0, 0, 0, 0, 0.324, 0.027, 0.0, 0.649,0.064));
        char_j.add(new SymbolFeatureVector('j', 4, 0, 3, 0, 1, 0, 0, 0, 0, 0, 0.344, 0.0, 109.0, 0.547,0.203));
        char_k.add(new SymbolFeatureVector('k', 4, 2, 0, 1, 1, 2, 0, 0, 2, 0, 0.0, 0.309, 0.391, 0.299,0.414));
        char_l.add(new SymbolFeatureVector('l', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.431, 0.062, 0.0, 0.508,0.048));
        char_m.add(new SymbolFeatureVector('m', 3, 1, 0, 0, 2, 1, 0, 1, 0, 0, 0.276, 0.367, 0.228, 0.131, 1.237));
        char_n.add(new SymbolFeatureVector('n', 3, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0.336, 0.303, 0.168, 0.193, 0.674));
        char_o.add(new SymbolFeatureVector('o', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.308, 0.248, 0.256, 0.19, 0.78));
        char_p.add(new SymbolFeatureVector('p', 2, 2, 0, 1, 1, 0, 0, 2, 0, 0, 0.387, 0.37, 0.244, 0.0, 0.547));
        char_q.add(new SymbolFeatureVector('q', 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0.377, 0.34, 0.0, 0.283, 0.411));
        char_r.add(new SymbolFeatureVector('r', 3, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0.265, 0.429, 0.306, 0.0,0.633));
        char_s.add(new SymbolFeatureVector('s', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.088, 0.353, 0.118, 0.441, 0.459));
        char_t.add(new SymbolFeatureVector('t', 3, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0.118, 0.382, 0.382, 0.118, 0.356));
        char_u.add(new SymbolFeatureVector('u', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.145, 0.237, 0.303, 0.316, 0.743));
        char_v.add(new SymbolFeatureVector('v', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.254, 0.206, 0.286, 0.254, 1.11));
        char_w.add(new SymbolFeatureVector('w', 3, 1, 2, 1, 0, 0, 0, 0, 1, 0, 0.22, 0.156, 0.362, 0.259, 1.05));
        char_x.add(new SymbolFeatureVector('x', 4, 2, 1, 1, 1, 1, 1, 0, 0, 1, 0.224, 0.286, 0.153, 0.337, 1.158));
        char_y.add(new SymbolFeatureVector('y', 3, 3, 2, 1, 0, 0, 2, 0, 0, 1, 0.278, 0.214, 0.254, 0.254, 0.49));
        char_z.add(new SymbolFeatureVector('z', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.292, 0.219, 0.218, 0.271, 1.357));

        // Arial
        /*
        char_0.add(new SymbolFeatureVector('0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.245, 0.256, 0.245, 0.254, 0.573));
        char_1.add(new SymbolFeatureVector('1', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.486, 0.174, 0.0, 0.341, 0.367));
        char_2.add(new SymbolFeatureVector('2', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.308, 0.166, 0.308, 0.217, 0.563));
        char_3.add(new SymbolFeatureVector('3', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.308, 0.166, 0.308, 0.217, 0.563));
        char_4.add(new SymbolFeatureVector('4', 3, 2, 1, 0, 0, 2, 1, 0, 0, 1, 0.276, 0.161, 0.236, 0.326, 0.645));
        char_5.add(new SymbolFeatureVector('5', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.218, 0.343, 0.165, 0.274, 0.575));
        char_6.add(new SymbolFeatureVector('6', 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0.221, 0.292, 0.25, 0.236, 0.586));
        char_7.add(new SymbolFeatureVector('7', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.516, 0.164, 0.28, 0.04, 0.552));
        char_8.add(new SymbolFeatureVector('8', 0, 2, 0, 0, 0, 0, 1, 1, 0, 0, 0.266, 0.27, 0.0225, 0.238, 0.577));
        char_9.add(new SymbolFeatureVector('9', 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0.254, 0.223, 0.22, 0.303, 0.582));
        */

        // handwriting set 2
        char_0.add(new SymbolFeatureVector('0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.269, 0.236, 0.229, 0.265, 0.469));
        char_1.add(new SymbolFeatureVector('1', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.503, 0.08, 0.0, 0.411, 0.153));
        char_2.add(new SymbolFeatureVector('2', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.357, 0.125, 0.298, 0.22, 0.424));
        char_3.add(new SymbolFeatureVector('3', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.438, 0.128, 0.109, 0.324, 0.397));
        char_4.add(new SymbolFeatureVector('4', 3, 1, 2, 0, 0, 1, 1, 0, 0, 0, 0.356, 0.392, 0.0, 0.252, 0.545));
        char_5.add(new SymbolFeatureVector('5', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.209, 0.215, 0.322, 0.254, 0.76));
        char_6.add(new SymbolFeatureVector('6', 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0.112, 0.188, 0.402, 0.299, 0.389));
        char_7.add(new SymbolFeatureVector('7', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.476, 0.163, 0.265, 0.096, 0.526));
        char_8.add(new SymbolFeatureVector('8', 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0.293, 0.254, 0.241, 0.211, 0.476));
        char_9.add(new SymbolFeatureVector('9', 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0.408, 0.118, 0.231, 0.243, 0.492));

        char_A.add(new SymbolFeatureVector('A', 4, 4, 1, 0, 1, 2, 1, 0, 1, 2, 0.3, 0.162, 0.231, 0.306, 0.41));
        char_B.add(new SymbolFeatureVector('B', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.297, 0.203, 0.252, 0.248, 0.632));
        char_C.add(new SymbolFeatureVector('C', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.142, 0.292, 0.345, 0.221, 0.691));
        char_D.add(new SymbolFeatureVector('D', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.253, 0.27, 0.241, 0.236, 0.65));
        char_E.add(new SymbolFeatureVector('E', 3, 1, 2, 0, 0, 1, 0, 0, 1, 0, 0.141, 0.318, 0.359, 0.182, 0.535));
        char_F.add(new SymbolFeatureVector('F', 3, 1, 2, 0, 1, 0, 0, 1, 0, 0, 0.246, 0.486, 0.268, 0.0, 0.56));
        char_G.add(new SymbolFeatureVector('G', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.136, 0.252, 0.301, 0.311, 0.537));
        char_H.add(new SymbolFeatureVector('H', 4, 2, 1, 1, 1, 1, 1, 0, 1, 0, 0.33, 0.26, 0.18, 0.23, 0.539));
        char_I.add(new SymbolFeatureVector('I', 5, 2, 1, 1, 2, 1, 1, 0, 1, 0, 0.388, 0.1, 0.394, 0.119, 0.358));
        char_J.add(new SymbolFeatureVector('J', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.401, 0.082, 0.19, 0.327, 0.506));
        char_K.add(new SymbolFeatureVector('K', 4, 2, 1, 1, 1, 1, 0, 0, 2, 0, 0.233, 0.27, 0.339, 0.159, 0.644));
        char_L.add(new SymbolFeatureVector('L', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.0, 0.374, 0.421, 0.206, 0.545));
        char_M.add(new SymbolFeatureVector('M', 2, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0.364, 0.345, 0.126, 0.165, 0.75));
        char_N.add(new SymbolFeatureVector('N', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.17, 0.34, 0.303, 0.186, 0.893));
        char_O.add(new SymbolFeatureVector('O', 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0.243, 0.266, 0.251, 0.24, 0.795));
        char_P.add(new SymbolFeatureVector('P', 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0.33, 0.286, 0.316, 0.068, 0.57));
        char_Q.add(new SymbolFeatureVector('Q', 2, 2, 0, 0, 1, 1, 0, 0, 0, 2, 0.222, 0.211, 0.226, 0.342, 0.772));
        char_R.add(new SymbolFeatureVector('R', 2, 2, 0, 0, 1, 1, 0, 0, 2, 0, 0.218, 0.251, 0.294, 0.237, 0.575));
        char_S.add(new SymbolFeatureVector('S', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.209, 0.284, 0.214, 0.293, 0.511));
        char_T.add(new SymbolFeatureVector('T', 3, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0.486, 0.254, 0.0, 0.261, 0.859));
        char_U.add(new SymbolFeatureVector('U', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.197, 0.237, 0.283, 0.283, 0.513));
        char_V.add(new SymbolFeatureVector('V', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.296, 0.191, 0.368, 0.145, 0.838));
        char_W.add(new SymbolFeatureVector('W', 3, 1, 2, 1, 0, 0, 0, 1, 0, 0, 0.149, 0.211, 0.395, 0.246, 1.161));
        char_X.add(new SymbolFeatureVector('X', 4, 2, 1, 1, 1, 1, 1, 0, 0, 1, 0.234, 0.139, 0.303, 0.323, 0.754));
        char_Y.add(new SymbolFeatureVector('Y', 3, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0.284, 0.19, 0.19, 0.336, 0.824));
        char_Z.add(new SymbolFeatureVector('Z', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.341, 0.152, 0.299, 0.207, 0.739));

        char_a.add(new SymbolFeatureVector('a', 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0.315, 0.145, 0.275, 0.265, 0.397));
        char_b.add(new SymbolFeatureVector('b', 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0.0, 0.299, 0.372, 0.328,0.269));
        char_c.add(new SymbolFeatureVector('c', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.259, 0.235, 0.309, 0.198, 0.844));
        char_d.add(new SymbolFeatureVector('d', 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0.221, 0.0, 0.414, 0.364,0.557));
        char_e.add(new SymbolFeatureVector('e', 2, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0.31, 0.256, 0.287, 0.147, 0.708));
        char_f.add(new SymbolFeatureVector('f', 4, 2, 1, 0, 2, 1, 0, 0, 2, 0, 0.187, 0.234, 0.427, 0.152, 0.603));
        char_g.add(new SymbolFeatureVector('g', 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0.4, 0.229, 0.129, 0.241,0.4));
        char_h.add(new SymbolFeatureVector('h', 3, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0.0, 0.309, 0.35, 0.341, 0.417));
        char_i.add(new SymbolFeatureVector('i', 4, 0, 1, 2, 0, 1, 0, 0, 0, 0, 0.185, 0.07, 0.0, 0.74, 0.103));
        char_j.add(new SymbolFeatureVector('j', 3, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0.219, 0.0, 0.192, 0.589,0.338));
        char_k.add(new SymbolFeatureVector('k', 4, 2, 1, 1, 1, 1, 0, 0, 2, 0, 0.133, 0.247, 0.413, 0.207, 0.449));
        char_l.add(new SymbolFeatureVector('l', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.542, 0.0, 0.417, 0.042,0.266));
        char_m.add(new SymbolFeatureVector('m', 3, 1, 0, 0, 2, 1, 0, 1, 0, 0, 0.352, 0.31, 0.183, 0.155, 1.179));
        char_n.add(new SymbolFeatureVector('n', 2, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0.329, 0.282, 0.212, 0.176, 0.714));
        char_o.add(new SymbolFeatureVector('o', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.298, 0.228, 0.272, 0.202, 0.842));
        char_p.add(new SymbolFeatureVector('p', 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0.379, 0.359, 0.262, 0.0,0.442));
        char_q.add(new SymbolFeatureVector('q', 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0.0, 0.526, 0.295, 0.179,0.762));
        char_r.add(new SymbolFeatureVector('r', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.322, 0.372, 0.205, 0.0,0.917));
        char_s.add(new SymbolFeatureVector('s', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.183, 0.312, 0.22, 0.284, 0.361));
        char_t.add(new SymbolFeatureVector('t', 4, 2, 2, 1, 0, 1, 0, 2, 0, 0, 0.223, 0.25, 0.295, 0.232, 0.536));
        char_u.add(new SymbolFeatureVector('u', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.152, 0.242, 0.303, 0.303, 0.6));
        char_v.add(new SymbolFeatureVector('v', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.343, 0.03, 0.418, 0.209, 1.0));
        char_w.add(new SymbolFeatureVector('w', 3, 1, 2, 1, 0, 0, 0, 0, 1, 0, 0.241, 0.103, 0.379, 0.276, 1.667));
        char_x.add(new SymbolFeatureVector('x', 4, 2, 1, 1, 1, 1, 0, 2, 0, 0, 0.181, 0.381, 0.114, 0.324, 0.816));
        char_y.add(new SymbolFeatureVector('y', 3, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0.373, 0.173, 0.093, 0.36, 0.605));
        char_z.add(new SymbolFeatureVector('z', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.187, 0.275, 0.352, 0.187, 0.889));


        // handwriting set 3
        char_0.add(new SymbolFeatureVector('0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.261, 0.265, 0.249, 0.224, 0.596));
        char_1.add(new SymbolFeatureVector('1', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.483, 0.022, 0.0,0.494, 0.057));
        char_2.add(new SymbolFeatureVector('2', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.335, 0.117, 0.314, 0.234, 0.426));
        char_3.add(new SymbolFeatureVector('3', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.413, 0.146, 0.113, 0.329, 0.466));
        char_4.add(new SymbolFeatureVector('4', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.11, 0.174, 0.349, 0.367, 1.189));
        char_5.add(new SymbolFeatureVector('5', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.254, 0.363, 0.113, 0.271, 0.72));
        char_6.add(new SymbolFeatureVector('6', 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0.037, 0.268, 0.384, 0.311, 0.44));
        char_7.add(new SymbolFeatureVector('7', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.469, 0.172, 0.166, 0.193, 0.581));
        char_8.add(new SymbolFeatureVector('8', 0, 2, 0, 0, 0, 0, 1, 1, 0, 0, 0.286, 0.238, 0.223, 0.253, 0.671));
        char_9.add(new SymbolFeatureVector('9', 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0.409, 0.241, 0.112, 0.237, 0.571));

        char_A.add(new SymbolFeatureVector('A', 2, 2, 0, 0, 1, 1, 1, 1, 0, 0, 0.367, 0.259, 0.205, 0.169, 0.788));
        char_B.add(new SymbolFeatureVector('B', 1, 3, 0, 0, 1, 0, 0, 0, 2, 1, 0.278, 0.208, 0.335, 0.18, 0.78));
        char_C.add(new SymbolFeatureVector('C', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.149, 0.296, 0.367, 0.19, 0.642));
        char_D.add(new SymbolFeatureVector('D', 2, 2, 0, 2, 0, 0, 0, 2, 0, 0, 0.229, 0.303, 0.294, 0.174, 0.585));
        char_E.add(new SymbolFeatureVector('E', 3, 1, 2, 0, 0, 1, 0, 1, 0, 0, 0.21, 0.395, 0.303, 0.092, 0.506));
        char_F.add(new SymbolFeatureVector('F', 3, 1, 2, 0, 1, 0, 0, 1, 0, 0, 0.272, 0.479, 0.248, 0.0,0.56));
        char_G.add(new SymbolFeatureVector('G', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.11, 0.211, 0.306, 0.373, 0.625));
        char_H.add(new SymbolFeatureVector('H', 4, 2, 1, 1, 1, 1, 0, 0, 1, 1, 0.223, 0.207, 0.299, 0.271, 0.603));
        char_I.add(new SymbolFeatureVector('I', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.449, 0.058, 0.0, 0.493,0.074));
        char_J.add(new SymbolFeatureVector('J', 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0.455, 0.049, 0.133, 0.364, 0.422));
        char_K.add(new SymbolFeatureVector('K', 4, 2, 1, 1, 1, 1, 0, 2, 0, 0, 0.136, 0.443, 0.233, 0.1875, 0.75));
        char_L.add(new SymbolFeatureVector('L', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.0, 0.297, 0.484, 0.21875, 0.693));
        char_M.add(new SymbolFeatureVector('M', 3, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0.318, 0.365, 0.176, 0.141, 0.915));
        char_N.add(new SymbolFeatureVector('N', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.233, 0.3125, 0.176, 0.278, 0.79));
        char_O.add(new SymbolFeatureVector('O', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.26, 0.247, 0.253, 0.24,0.784));
        char_P.add(new SymbolFeatureVector('P', 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0.398, 0.352, 0.25, 0.0, 0.556));
        char_Q.add(new SymbolFeatureVector('Q', 2, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0.236, 0.223, 0.248, 0.323, 0.857));
        char_R.add(new SymbolFeatureVector('R', 2, 2, 0, 0, 1, 1, 0, 0, 2, 0, 0.213, 0.311, 0.3244, 0.151, 0.632));
        char_S.add(new SymbolFeatureVector('S', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.085, 0.327, 0.176, 0.412, 0.581));
        char_T.add(new SymbolFeatureVector('T', 3, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0.26, 0.488, 0.252, 0.0, 0.984));
        char_U.add(new SymbolFeatureVector('U', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.226, 0.219, 0.271, 0.284, 0.716));
        char_V.add(new SymbolFeatureVector('V', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.287, 0.243, 0.209, 0.261, 1.058));
        char_W.add(new SymbolFeatureVector('W', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.337, 0.161, 0.264, 0.238, 1.14));
        char_X.add(new SymbolFeatureVector('X', 4, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0.265, 0.247, 0.194, 0.294, 0.937));
        char_Y.add(new SymbolFeatureVector('Y', 2, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0.347, 0.336, 0.276, 0.042, 0.941));
        char_Z.add(new SymbolFeatureVector('Z', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.258, 0.225, 0.311, 0.205, 1.204));

        char_a.add(new SymbolFeatureVector('a', 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0.337, 0.174, 0.216, 0.274, 0.776));
        char_b.add(new SymbolFeatureVector('b', 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0.026, 0.253, 0.383, 0.338, 0.506));
        char_c.add(new SymbolFeatureVector('c', 2, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0.125, 0.2875, 0.3875, 0.2, 0.79));
        char_d.add(new SymbolFeatureVector('d', 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0.263, 0.093, 0.395, 0.248, 0.772));
        char_e.add(new SymbolFeatureVector('e', 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0.359, 0.226, 0.277, 0.139, 0.8125));
        char_f.add(new SymbolFeatureVector('f', 3, 1, 2, 0, 0, 1, 0, 1, 0, 0, 0.329, 0.249, 0.276, 0.046, 0.598));
        char_g.add(new SymbolFeatureVector('g', 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0.374, 0.338, 0.007, 0.28, 0.5));
        char_h.add(new SymbolFeatureVector('h', 3, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0.126, 0.283, 0.315, 0.275, 0.5));
        char_i.add(new SymbolFeatureVector('i', 4, 0, 1, 2, 0, 1, 0, 0, 0, 0, 0.069, 0.172, 0.172, 0.586, 0.015));
        char_j.add(new SymbolFeatureVector('j', 4, 0, 3, 0, 1, 0, 0, 0, 0, 0, 0.284, 0.0, 0.164, 0.552,0.353));
        char_k.add(new SymbolFeatureVector('k', 4, 2, 0, 1, 1, 2, 0, 0, 2, 0, 0.0, 0.267, 0.389, 0.344,0.606));
        char_l.add(new SymbolFeatureVector('l', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.052, 0.455, 0.169, 0.325, 0.053));
        char_m.add(new SymbolFeatureVector('m', 3, 1, 0, 0, 2, 1, 0, 0, 1, 0, 0.382, 0.25, 0.279, 0.088, 1.371));
        char_n.add(new SymbolFeatureVector('n', 2, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0.36, 0.315, 0.213, 0.112, 0.816));
        char_o.add(new SymbolFeatureVector('o', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.263, 0.23, 0.248, 0.256, 1.079));
        char_p.add(new SymbolFeatureVector('p', 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0.353, 0.317, 0.324, 0.007, 0.446));
        char_q.add(new SymbolFeatureVector('q', 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0.407, 0.333, 0.0, 0.259,0.4));
        char_r.add(new SymbolFeatureVector('r', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 3293.0, 0.413, 0.293, 0.0, 0.795));
        char_s.add(new SymbolFeatureVector('s', 2, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0.15, 0.325, 0.15, 0.375,0.455));
        char_t.add(new SymbolFeatureVector('t', 4, 2, 1, 1, 1, 1, 0, 1, 1, 0, 0.12, 0.333, 0.293, 0.253, 0.4375));
        char_u.add(new SymbolFeatureVector('u', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.156, 0.221, 0.299, 0.325, 0.875));
        char_v.add(new SymbolFeatureVector('v', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0.197, 0.329, 0.079, 0.395, 1.306));
        char_w.add(new SymbolFeatureVector('w', 3, 1, 2, 1, 0, 0, 0, 0, 0, 1, 0.204, 0.12, 0.296, 0.38,1.667));
        char_x.add(new SymbolFeatureVector('x', 4, 2, 1, 1, 1, 1, 0, 2, 0, 0, 0.219, 0.247, 0.232, 0.301, 0.8));
        char_y.add(new SymbolFeatureVector('y', 3, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0.414, 0.272, 0.04, 0.272, 0.52));
        char_z.add(new SymbolFeatureVector('z', 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0.0, 0.94, 0.365, 0.24, 1.516));

        vectorCollections = new ArrayList<>();
        vectorCollections.add(char_0);
        vectorCollections.add(char_1);
        vectorCollections.add(char_2);
        vectorCollections.add(char_3);
        vectorCollections.add(char_4);
        vectorCollections.add(char_5);
        vectorCollections.add(char_6);
        vectorCollections.add(char_7);
        vectorCollections.add(char_8);
        vectorCollections.add(char_9);

        vectorCollections.add(char_A);
        vectorCollections.add(char_B);
        vectorCollections.add(char_C);
        vectorCollections.add(char_D);
        vectorCollections.add(char_E);
        vectorCollections.add(char_F);
        vectorCollections.add(char_G);
        vectorCollections.add(char_H);
        vectorCollections.add(char_I);
        vectorCollections.add(char_J);
        vectorCollections.add(char_K);
        vectorCollections.add(char_L);
        vectorCollections.add(char_M);
        vectorCollections.add(char_N);
        vectorCollections.add(char_O);
        vectorCollections.add(char_P);
        vectorCollections.add(char_Q);
        vectorCollections.add(char_R);
        vectorCollections.add(char_S);
        vectorCollections.add(char_T);
        vectorCollections.add(char_U);
        vectorCollections.add(char_V);
        vectorCollections.add(char_W);
        vectorCollections.add(char_X);
        vectorCollections.add(char_Y);
        vectorCollections.add(char_Z);

        vectorCollections.add(char_a);
        vectorCollections.add(char_b);
        vectorCollections.add(char_c);
        vectorCollections.add(char_d);
        vectorCollections.add(char_e);
        vectorCollections.add(char_f);
        vectorCollections.add(char_g);
        vectorCollections.add(char_h);
        vectorCollections.add(char_i);
        vectorCollections.add(char_j);
        vectorCollections.add(char_k);
        vectorCollections.add(char_l);
        vectorCollections.add(char_m);
        vectorCollections.add(char_n);
        vectorCollections.add(char_o);
        vectorCollections.add(char_p);
        vectorCollections.add(char_q);
        vectorCollections.add(char_r);
        vectorCollections.add(char_s);
        vectorCollections.add(char_t);
        vectorCollections.add(char_u);
        vectorCollections.add(char_v);
        vectorCollections.add(char_w);
        vectorCollections.add(char_x);
        vectorCollections.add(char_y);
        vectorCollections.add(char_z);
    }

    public char findMatch(SymbolFeatureVector input, int kValue) {
        ArrayList<Score> scoreList = new ArrayList<>();
        for(ArrayList<SymbolFeatureVector> c : vectorCollections) {
            for(SymbolFeatureVector data : c) {
                double score = input.compareSimilarityDistance(data);
                scoreList.add(new Score(data.getSymbol(), score));
            }
        }
        // sort
        ArrayList<Score> sortedScoreList = new ScoreSorter(scoreList).getSortedScoreList();
        Map<Character, Integer> symbolRanking = new HashMap<>();
        for (Score c : sortedScoreList) {
            if (!symbolRanking.containsKey(c.getLabel())) {
                symbolRanking.put(c.getLabel(), 1);
            } else {
                int occurrence = symbolRanking.get(c.getLabel());
                if (occurrence < kValue) {
                    symbolRanking.put(c.getLabel(), occurrence++);
                } else {
                    return c.getLabel();
                }
            }
        }
        return sortedScoreList.get(0).getLabel();
    }

    private class Score implements Comparable<Score> {
        char label;
        double score;
        Score(char label, double score) {
            this.label = label;
            this.score = score;
        }
        char getLabel() {
            return label;
        }
        double getScore() {
            return score;
        }
        @Override
        public int compareTo(Score otherScore) {
            return (this.getScore() < otherScore.getScore() ? -1 :
                    (this.getScore() == otherScore.getScore() ? 0 : 1));
        }
        @Override
        public String toString() {
            return "(" + this.label + ", " + this.score + ")";
        }
    }
    private class ScoreSorter {
        ArrayList<Score> scoreList = new ArrayList<>();
        public ScoreSorter(ArrayList<Score> scoreList) {
            this.scoreList = scoreList;
        }

        public ArrayList<Score> getSortedScoreList() {
            Collections.sort(scoreList);
            return scoreList;
        }
    }

}
