package com.tanto.SkeletonDetection;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;

import com.tanto.Utilities.ImageFilter;

import java.util.ArrayList;
import java.util.Collections;

public class Thinning {
    final private static int[][] direction = {
            {0, -1}, //n
            {1, 0}, //e
            {0, 1}, //s
            {-1, 0}, //w
            {1, 1}, //ne
            {1, -1}, //se
            {-1, 1}, //sw
            {-1, -1} //nw
    };
    private Thinning() {}

    public static Bitmap thin(Bitmap originalBitmap) {
        Bitmap resultBitmap = ImageFilter.toMonoBlackAndWhite(originalBitmap);
        ArrayList<Point> flaggedPixels = new ArrayList<>();
        Point startPoint = new Point();
        Point endPoint = new Point(resultBitmap.getWidth(), resultBitmap.getHeight());
        boolean isProcessing = true;

//        findSquareBoundary(originalBitmap, startPoint, endPoint);

        while (isProcessing) {
            boolean didDeleteOnce = false;
            stepOne(resultBitmap, flaggedPixels, startPoint, endPoint);
            if (!flaggedPixels.isEmpty()) {
                deleteFlaggedPixel(resultBitmap, flaggedPixels);
                didDeleteOnce = true;
            }

            stepTwo(resultBitmap, flaggedPixels, startPoint, endPoint);
            if (flaggedPixels.isEmpty() && !didDeleteOnce) {
                isProcessing = false;
            } else {
                deleteFlaggedPixel(resultBitmap, flaggedPixels);
            }
        }
        return resultBitmap;
    }

    public static ArrayList<Skeleton> getSkeletonObject(Bitmap inputImage) {
        Bitmap tempBitmap = ImageFilter.toMonoBlackAndWhite(inputImage);
        ArrayList<Point> skeletonPixels = (ArrayList<Point>) readObjectRegion(tempBitmap).clone();
        ArrayList<Integer> flaggedPixels = new ArrayList<>();

        int deletionCount;
        do {
            deletionCount = 0;
            stepOne(tempBitmap, flaggedPixels, skeletonPixels);
            if (!flaggedPixels.isEmpty()) {
                deleteFlaggedPixel(tempBitmap, flaggedPixels, skeletonPixels);
                deletionCount++;
            }
            stepTwo(tempBitmap, flaggedPixels, skeletonPixels);
            if (!flaggedPixels.isEmpty()) {
                deleteFlaggedPixel(tempBitmap, flaggedPixels, skeletonPixels);
                deletionCount++;
            }
        } while (deletionCount != 0);

        Bitmap skeletonIllustration = inputImage.copy(Bitmap.Config.ARGB_8888, true);

        Skeleton prunedSkeleton = prune(skeletonPixels, tempBitmap);

        prunedSkeleton.height = tempBitmap.getHeight();
        prunedSkeleton.width = tempBitmap.getWidth();

        // testing pruneBeta
        pruneBeta(prunedSkeleton, tempBitmap);

        for (Point p : prunedSkeleton.getSkeleton()) {
            skeletonIllustration.setPixel(p.x, p.y, Color.GREEN);
        }
        for (Point p : prunedSkeleton.getEndPoints()) {
            skeletonIllustration.setPixel(p.x, p.y, Color.RED);
        }
        for (Point p : prunedSkeleton.getIntersections()) {
            skeletonIllustration.setPixel(p.x, p.y, Color.BLUE);
        }

        // proses baru lagi, misahin dua objek yang mungkin ada
        ArrayList<Skeleton> listOfSkeletons = getSeparatedObjectFromSkeleton(prunedSkeleton, tempBitmap);
        //Skeleton newSkeleton = getFirstSkeletonFromStartingPoint(new Skeleton((ArrayList<Point>) prunedSkeleton.getSkeleton().clone(), (ArrayList<Point>) prunedSkeleton.getEndPoints().clone(), (ArrayList<Point>) prunedSkeleton.getIntersections().clone()), tempBitmap, prunedSkeleton.getEndPoints().get(0));

        //char test = SkeletonHeuristics.recognize(listOfSkeletons);

        //return prunedSkeleton;
        return listOfSkeletons;
    }

    public static Skeleton getSkeleton(Bitmap inputImage) {
        Bitmap tempBitmap = ImageFilter.toMonoBlackAndWhite(inputImage);
        ArrayList<Point> skeletonPixels = (ArrayList<Point>) readObjectRegion(tempBitmap).clone();
        ArrayList<Integer> flaggedPixels = new ArrayList<>();

        int deletionCount;
        do {
            deletionCount = 0;
            stepOne(tempBitmap, flaggedPixels, skeletonPixels);
            if (!flaggedPixels.isEmpty()) {
                deleteFlaggedPixel(tempBitmap, flaggedPixels, skeletonPixels);
                deletionCount++;
            }
            stepTwo(tempBitmap, flaggedPixels, skeletonPixels);
            if (!flaggedPixels.isEmpty()) {
                deleteFlaggedPixel(tempBitmap, flaggedPixels, skeletonPixels);
                deletionCount++;
            }
        } while (deletionCount != 0);

        Skeleton prunedSkeleton = prune(skeletonPixels, tempBitmap);
        prunedSkeleton.height = tempBitmap.getHeight();
        prunedSkeleton.width = tempBitmap.getWidth();

        pruneBeta(prunedSkeleton, tempBitmap);
        return prunedSkeleton;
    }

    private static ArrayList<Point> readObjectRegion(Bitmap inputImage) {
        int width = inputImage.getWidth();
        int height = inputImage.getHeight();
        ArrayList<Point> objectPixels = new ArrayList<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (isPixelBlack(inputImage.getPixel(x, y))) {
                    objectPixels.add(new Point(x, y));
                }
            }
        }
        return objectPixels;
    }

    private static boolean isPixelBlack(int pixel) {
        return (Color.rgb(Color.red(pixel), Color.green(pixel), Color.blue(pixel))
                == Color.BLACK);
    }

    private static boolean isColorEqual(int colorA, int colorB) {
        return (Color.rgb(Color.red(colorA), Color.green(colorA), Color.blue(colorA))
                == Color.rgb(Color.red(colorB), Color.green(colorB), Color.blue(colorB)));
    }

    private static void deleteFlaggedPixel(Bitmap resultBitmap, ArrayList<Point> flaggedPixels) {
        for (Point p : flaggedPixels) {
            resultBitmap.setPixel(p.x, p.y, Color.WHITE);
        }
        flaggedPixels.clear();
    }

    private static void deleteFlaggedPixel(Bitmap resultBitmap, ArrayList<Integer> flaggedPixels, ArrayList<Point> objectPixels) {
        Collections.sort(flaggedPixels, Collections.<Integer>reverseOrder());
        for (int idx : flaggedPixels) {
            resultBitmap.setPixel(objectPixels.get(idx).x, objectPixels.get(idx).y, Color.WHITE);
            objectPixels.remove(idx);
        }
        flaggedPixels.clear();
    }

    private static boolean isNeighboursEnough(Bitmap resultBitmap, Point position) {
        int width = resultBitmap.getWidth();
        int height = resultBitmap.getHeight();
        boolean isUpperEdge = (position.y-1 < 0);
        boolean isLowerEdge = (position.y+1 >= height);
        boolean isRightEdge = (position.x+1 >= width);
        boolean isLeftEdge = (position.x-1 < 0);

        int count = 0;
        count += (!isUpperEdge && isPixelBlack(resultBitmap.getPixel(position.x, position.y-1))) ? 1 : 0; // north
        count += (!isUpperEdge && !isRightEdge && isPixelBlack(resultBitmap.getPixel(position.x+1, position.y-1))) ? 1 : 0; // northeast
        count += (!isRightEdge && isPixelBlack(resultBitmap.getPixel(position.x+1, position.y))) ? 1 : 0; // east
        count += (!isLowerEdge && !isRightEdge && isPixelBlack(resultBitmap.getPixel(position.x+1, position.y+1))) ? 1 : 0; //southeast
        count += (!isLowerEdge && isPixelBlack(resultBitmap.getPixel(position.x, position.y+1))) ? 1 : 0; // south
        count += (!isLowerEdge && !isLeftEdge && isPixelBlack(resultBitmap.getPixel(position.x-1, position.y+1))) ? 1 : 0; // southwest
        count += (!isLeftEdge && isPixelBlack(resultBitmap.getPixel(position.x-1, position.y))) ? 1 : 0; // west
        count += (!isLeftEdge && !isUpperEdge && isPixelBlack(resultBitmap.getPixel(position.x-1, position.y-1))) ? 1 : 0; // northwest

        return (2 <= count && count <= 6);
    }

    private static boolean isOnlyOneTransition(Bitmap resultBitmap, Point position) {
        int transitionCount = 0;
        int currentColor = resultBitmap.getPixel(position.x, position.y);
        //north
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x, position.y-1))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }
        // north east
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x+1, position.y-1))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }
        // east
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x+1, position.y))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }
        // south east
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x+1, position.y+1))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }
        // south
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x, position.y+1))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }
        // south west
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x-1, position.y+1))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }
        // west
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x-1, position.y))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }
        // north west
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x-1, position.y-1))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }
        //north
        try {
            if (!isColorEqual(currentColor, resultBitmap.getPixel(position.x, position.y-1))) {
                if (isPixelBlack(currentColor)) {
                    currentColor = Color.WHITE;
                } else {
                    transitionCount += 1;
                    currentColor = Color.BLACK;
                }
            }
        } catch (IllegalArgumentException e) {
            if (isPixelBlack(currentColor)) {
                currentColor = Color.WHITE;
            }
        }

        return transitionCount == 1;
    }

    private static boolean conditionCD(Bitmap resultBitmap, Point position) {
        // syarat c dan d: jika SATU pixel bernilai putih -> true
        boolean c1, c2,c3;
        try {
            c1 = !isPixelBlack(resultBitmap.getPixel(position.x, position.y-1));
        } catch (IllegalArgumentException e){
            c1 = false;
        }
        try {
            c2 = !isPixelBlack(resultBitmap.getPixel(position.x+1, position.y));
        } catch (IllegalArgumentException e){
            c2 = false;
        }
        try {
            c3 = !isPixelBlack(resultBitmap.getPixel(position.x, position.y+1));
        } catch (IllegalArgumentException e){
            c3 = false;
        }

        boolean d1, d2, d3;
        try {
            d1 = !isPixelBlack(resultBitmap.getPixel(position.x+1, position.y));
        } catch (IllegalArgumentException e){
            d1 = false;
        }
        try {
            d2 = !isPixelBlack(resultBitmap.getPixel(position.x, position.y+1));
        } catch (IllegalArgumentException e){
            d2 = false;
        }
        try {
            d3 = !isPixelBlack(resultBitmap.getPixel(position.x-1, position.y));
        } catch (IllegalArgumentException e){
            d3 = false;
        }
        boolean c =  c1 || c2 || c3;// p2 || p4 || p6
        boolean d =  d1 || d2 || d3;// p4 || p6 || p8
        return (c && d);
    }

    private static boolean conditionCD2(Bitmap resultBitmap, Point position) {
        // syarat c dan d: jika SATU pixel bernilai putih -> true
        boolean c1, c2,c3;
        try {
            c1 = !isPixelBlack(resultBitmap.getPixel(position.x, position.y-1));
        } catch (IllegalArgumentException e){
            c1 = false;
        }
        try {
            c2 = !isPixelBlack(resultBitmap.getPixel(position.x+1, position.y));
        } catch (IllegalArgumentException e){
            c2 = false;
        }
        try {
            c3 = !isPixelBlack(resultBitmap.getPixel(position.x-1, position.y));
        } catch (IllegalArgumentException e){
            c3 = false;
        }

        boolean d1, d2, d3;
        try {
            d1 = !isPixelBlack(resultBitmap.getPixel(position.x, position.y-1));
        } catch (IllegalArgumentException e){
            d1 = false;
        }
        try {
            d2 = !isPixelBlack(resultBitmap.getPixel(position.x, position.y+1));
        } catch (IllegalArgumentException e){
            d2 = false;
        }
        try {
            d3 = !isPixelBlack(resultBitmap.getPixel(position.x-1, position.y));
        } catch (IllegalArgumentException e){
            d3 = false;
        }
        boolean c =  c1 || c2 || c3;// p2 || p4 || p8
        boolean d =  d1 || d2 || d3;// p2 || p6 || p8
        return (c && d);
    }

    private static void stepOne(Bitmap bitmap, ArrayList<Point> flaggedPixels, Point start, Point end) {
        for (int y = start.y; y < end.y; y++) {
            for (int x = start.x; x < end.x; x++) {
                if (isPixelBlack(bitmap.getPixel(x,y))) {
                    if (isNeighboursEnough(bitmap, new Point(x, y))) {
                        if (isOnlyOneTransition(bitmap, new Point(x, y))) {
                            if (conditionCD(bitmap, new Point(x, y))) {
                                flaggedPixels.add(new Point(x, y));
                            }
                        }
                    }
                }
            }
        }
    }

    private static void stepTwo(Bitmap bitmap, ArrayList<Point> flaggedPixels, Point start, Point end) {
        for (int y = start.y; y < end.y; y++) {
            for (int x = start.x; x < end.x; x++) {
                if (isPixelBlack(bitmap.getPixel(x, y))) {
                    if (isNeighboursEnough(bitmap, new Point(x, y))) {
                        if (isOnlyOneTransition(bitmap, new Point(x, y))) {
                            if (conditionCD2(bitmap, new Point(x, y))) {
                                flaggedPixels.add(new Point(x, y));
                            }
                        }
                    }
                }
            }
        }
    }

    private static void stepOne(Bitmap bitmap, ArrayList<Integer> flaggedPixels, ArrayList<Point> objectPixels) {
        for (int i = 0; i < objectPixels.size(); i++) {
            if (isPixelBlack(bitmap.getPixel(objectPixels.get(i).x, objectPixels.get(i).y))) {
                if (isNeighboursEnough(bitmap, objectPixels.get(i))) {
                    if (isOnlyOneTransition(bitmap, objectPixels.get(i))) {
                        if (conditionCD(bitmap, objectPixels.get(i))) {
                            flaggedPixels.add(i);
                        }
                    }
                }
            }
        }
    }

    private static void stepTwo(Bitmap bitmap, ArrayList<Integer> flaggedPixels, ArrayList<Point> objectPixels) {
        for (int i = 0; i < objectPixels.size(); i++) {
            if (isPixelBlack(bitmap.getPixel(objectPixels.get(i).x, objectPixels.get(i).y))) {
                if (isNeighboursEnough(bitmap, objectPixels.get(i))) {
                    if (isOnlyOneTransition(bitmap, objectPixels.get(i))) {
                        if (conditionCD2(bitmap, objectPixels.get(i))) {
                            flaggedPixels.add(i);
                        }
                    }
                }
            }
        }
    }

    private static Skeleton prune(ArrayList<Point> originalSkeleton, Bitmap skeletonBitmap) {
        ArrayList<Point> endPoints = new ArrayList<>();
        ArrayList<Point> intersections = new ArrayList<>();
        trace(originalSkeleton, skeletonBitmap, endPoints, intersections);

        return new Skeleton(originalSkeleton, endPoints, intersections, skeletonBitmap.getWidth(), skeletonBitmap.getHeight());
    }

    private static void trace(ArrayList<Point> skeleton, Bitmap skeletonBitmap, ArrayList<Point> endPoints, ArrayList<Point> intersections) {
        endPoints.clear();
        intersections.clear();

        for (Point p : skeleton) {
            int[][] sample = new int[3][3];

            // Create a 3x3 matrix around the Point p
            Bitmap test = Bitmap.createBitmap(3, 3, Bitmap.Config.ARGB_8888);
            test.setPixel(0,0, Color.WHITE);

            int testy = -1;
            for (int y = 0; y < 3; y++) {
                int testx = -1;
                for (int x = 0; x < 3; x++) {
                    try {
                        test.setPixel(1+testx,1+testy, skeletonBitmap.getPixel(p.x+testx, p.y+testy));
                    } catch (IllegalArgumentException e) {
                        test.setPixel(1+testx, 1+testy, Color.RED);
                    }
                    testx++;
                }
                testy++;
            }

            int ytest2 = -1;
            for (int y = 0; y < 3; y++) {
                int xtes2 = -1;
                for (int x = 0; x < 3; x++) {
                    try {
                        sample[y][x] = test.getPixel(1+xtes2 , 1+ytest2);
                        sample[y][x] = (sample[y][x] == Color.WHITE) ? 0 : 1;
                    } catch (IllegalArgumentException e) {
                        sample[y][x] = 0;
                    }
                    xtes2++;
                }
                ytest2++;
            }

            if (StructuringElements.match(sample, "endpoint")) {
                endPoints.add(p);
            } else if (StructuringElements.match(sample, "intersection")) {
                intersections.add(p);
            }
        }
        return;
    }

    private static void trace(Skeleton skeleton, Bitmap skeletonBitmap) {
        // ini maksudnya ngescan endpoint sama intersection skeleton kan???
        ArrayList<Point> endPoints = skeleton.getEndPoints();
        ArrayList<Point> intersections = skeleton.getIntersections();
        endPoints.clear();
        intersections.clear();
        // kalo bukan, hapus aja dua di atas, dan ubah pruneBeta

        for (Point p : skeleton.getSkeleton()) {
            int[][] sample = new int[3][3];

            // Create a 3x3 matrix around the Point p
            int yOffset = -1;
            for (int y = 0; y < 3; y++) {
                int xOffset = -1;
                for (int x = 0; x < 3; x++) {
                    try {
                        sample[y][x] = skeletonBitmap.getPixel(p.x+xOffset , p.y+yOffset);
                        sample[y][x] = (sample[y][x] == Color.WHITE) ? 0 : 1;
                    } catch (IllegalArgumentException e) {
                        sample[y][x] = 0;
                    }
                    xOffset++;
                }
                yOffset++;
            }

            if (StructuringElements.match(sample, "endpoint")) {
                endPoints.add(p);
            } else if (StructuringElements.match(sample, "intersection")) {
                intersections.add(p);
            }
        }
        skeleton.length = skeleton.getSkeleton().size();
        return;
    }

    private static void pruneBeta(Skeleton originalSkeleton, Bitmap originalBitmap) {
        ArrayList<Integer> flaggedBranchIndexes = new ArrayList<>();
        ArrayList<Integer> branchLengths = new ArrayList<>();
        int currentLength;
        int sumLength = 0;
        Point currentPoint = new Point();
        boolean isStopped;

        if (originalSkeleton.getIntersections().size() > 0) {
            for (int i = 0; i < originalSkeleton.getEndPoints().size(); i++) {
                currentLength = 0;
                currentPoint.set(originalSkeleton.getEndPoints().get(i).x, originalSkeleton.getEndPoints().get(i).y);
                Point nextPoint = new Point(currentPoint);
                Point prevPoint = new Point(currentPoint);
                Point prevSecondPoint = new Point(currentPoint);
                isStopped = false;

                // telusuri branch
                while (!isStopped) {
                    currentLength++;
                    // maju ke point berikutnya di branch
                    for (int[] compass : direction) {
                        try {
                            nextPoint.set(currentPoint.x+compass[0], currentPoint.y+compass[1]);
                            if (!nextPoint.equals(prevPoint) && isPixelBlack(originalBitmap.getPixel(nextPoint.x, nextPoint.y))) {
                                if (!nextPoint.equals(prevSecondPoint)) {
                                    prevSecondPoint.set(prevPoint.x, prevPoint.y);
                                    prevPoint.set(currentPoint.x, currentPoint.y);
                                    currentPoint.set(nextPoint.x, nextPoint.y);
                                    break;
                                }
                            }
                        } catch (IllegalArgumentException e) {
                            continue;
                        }
                    }

                    /*if ((float) currentLength > ((float) originalSkeleton.getSkeleton().size() * 0.05)) {
                        isStopped = true;
                        break;
                    }*/

                    for (Point p : originalSkeleton.getIntersections()) {
                        if (currentPoint.equals(p)) {
                            branchLengths.add(currentLength);
                            sumLength += currentLength;
                            isStopped = true;
                            break;
                        }
                    }
                }

                // kalo panjang branch sekarang kurang dari 8.5% total panjang skeleton, flag dulu
                /*if ((float) currentLength < ((float) originalSkeleton.getSkeleton().size() * 0.05)) {
                    flaggedBranchIndexes.add(i);
                }*/
            }

            // check branch length vs 33% avg
            float thirdAvg = (float) (sumLength) / (float) (branchLengths.size() * 3);

            for (int i = 0; i < branchLengths.size(); i++) {
                if ((float) branchLengths.get(i) < thirdAvg) {
                    flaggedBranchIndexes.add(i);
                }
            }

            // delete flagged branch
            for (int i = 0; i < flaggedBranchIndexes.size(); i++) {
                isStopped = false;
                currentPoint.set(originalSkeleton.getEndPoints().get(flaggedBranchIndexes.get(i)).x,
                        originalSkeleton.getEndPoints().get(flaggedBranchIndexes.get(i)).y);
                Point nextPoint = new Point(currentPoint);
                Point prevPoint = new Point(currentPoint);

                while (!isStopped) {
                    // set pixel white
                    originalBitmap.setPixel(currentPoint.x, currentPoint.y, Color.WHITE);
                    originalSkeleton.getSkeleton().remove(currentPoint);

                    // maju ke point berikutnya di branch
                    for (int[] compass : direction) {
                        nextPoint.set(currentPoint.x+compass[0], currentPoint.y+compass[1]);
                        if (!nextPoint.equals(prevPoint) && isPixelBlack(originalBitmap.getPixel(nextPoint.x, nextPoint.y))) {
                            prevPoint.set(currentPoint.x, currentPoint.y);
                            currentPoint.set(nextPoint.x, nextPoint.y);
                            break;
                        }
                    }

                    // cek apakah point yg dicek merupakan intersection
                    for (Point p : originalSkeleton.getIntersections()) {
                        if (currentPoint.equals(p)) {
                            isStopped = true;
                            break;
                        }
                    }
                }
            }

            // cek setiap intersection, apakah masih intersection?
            // hapus endPoint yang dihapus dari daftar endpoints
            trace(originalSkeleton.getSkeleton(), originalBitmap, originalSkeleton.getEndPoints(), originalSkeleton.getIntersections());
            originalSkeleton.length = originalSkeleton.getSkeleton().size();
        }
    }

    private static float calculateDistance(Point A, Point B) {
        double distance = Math.sqrt(Math.pow(Math.abs(A.x - B.x), 2) + Math.pow(Math.abs(A.y - B.y), 2));
        return (float) distance;
    }

    private static ArrayList<Skeleton> getSeparatedObjectFromSkeleton(Skeleton originalSkeleton, Bitmap bitmap) {
        ArrayList<Skeleton> listOfSkeletons = new ArrayList<>();
        Skeleton tempSkeleton = new Skeleton((ArrayList<Point>) originalSkeleton.getSkeleton().clone(), (ArrayList<Point>) originalSkeleton.getEndPoints().clone(), (ArrayList<Point>) originalSkeleton.getIntersections().clone(), originalSkeleton.width, originalSkeleton.height);
        Skeleton newSkeleton;

        // panggil getFirstSkeletonFromStartingPoint berulang kali
        for (Point p : tempSkeleton.getEndPoints()) {
            if (tempSkeleton.getSkeleton().indexOf(p) != -1) {
                newSkeleton = getFirstSkeletonFromStartingPoint(tempSkeleton, bitmap, p, new ArrayList<Point>());
                trace(newSkeleton.getSkeleton(), bitmap, newSkeleton.getEndPoints(), newSkeleton.getIntersections());
                listOfSkeletons.add(newSkeleton);
            }
        }

        // pastikan ga ada objek bundar (tanpa endpoint) yang terlewat
        boolean isNoObjectLeft = false;
        while (!isNoObjectLeft) {
            int i = 0;
            while (i < originalSkeleton.getSkeleton().size() && tempSkeleton.getSkeleton().indexOf(originalSkeleton.getSkeleton().get(i)) == -1) {
                i++;
            }

            if (i >= originalSkeleton.getSkeleton().size()) {
                isNoObjectLeft = true;
            } else {
                newSkeleton = getFirstSkeletonFromStartingPoint(tempSkeleton, bitmap, originalSkeleton.getSkeleton().get(i), new ArrayList<Point>());
                trace(newSkeleton.getSkeleton(), bitmap, newSkeleton.getEndPoints(), newSkeleton.getIntersections());
                listOfSkeletons.add(newSkeleton);
            }

        }


        return listOfSkeletons;
    }

    private static Skeleton getFirstSkeletonFromStartingPoint(Skeleton supposedlyMultipleSkeleton, Bitmap bitmap, Point startingPoint, ArrayList<Point> exceptionPoints) {
        Skeleton resultSkeleton = new Skeleton(new ArrayList<Point>(), new ArrayList<Point>(), new ArrayList<Point>(), supposedlyMultipleSkeleton.width, supposedlyMultipleSkeleton.height);
        ArrayList<Point> skeletonPoints = new ArrayList<>();

        // mulai dari endpoint pertama
        // delete point yang ditelusuri dari arraylist point skeleton
        // kalo nyampe intersection, catet semua tetangga
        // mulai dari tetangga paling awal (dari kiri atas)
        // sambungin arraylist dari tetangga

        Point currentPoint = new Point(startingPoint.x, startingPoint.y);
        Point nextPoint = new Point(currentPoint);
        Point prevPoint = new Point(currentPoint);
        Point prevSecondPoint = new Point(currentPoint);
        boolean isStopped = false;

        if (!(supposedlyMultipleSkeleton.getSkeleton().indexOf(startingPoint) == -1)) {
            while (!isStopped) {
                for (int[] compass : direction) {
                    try {
                        nextPoint.set(currentPoint.x+compass[0], currentPoint.y+compass[1]);
                        if (!nextPoint.equals(prevPoint) && isPixelBlack(bitmap.getPixel(nextPoint.x, nextPoint.y))) {
                            if (!nextPoint.equals(prevSecondPoint) && supposedlyMultipleSkeleton.getSkeleton().indexOf(nextPoint) != -1 && exceptionPoints.indexOf(nextPoint) == -1) {
                                skeletonPoints.add(new Point(currentPoint.x, currentPoint.y));
                                supposedlyMultipleSkeleton.getSkeleton().remove(currentPoint);
                                prevSecondPoint.set(prevPoint.x, prevPoint.y);
                                prevPoint.set(currentPoint.x, currentPoint.y);
                                currentPoint.set(nextPoint.x, nextPoint.y);

                                break;
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        continue;
                    }
                }

                for (Point p : supposedlyMultipleSkeleton.getIntersections()) {
                    if (currentPoint.equals(p)) {
                        ArrayList<Point> shouldNotBeCheckedPoints = new ArrayList<>();
                        skeletonPoints.add(new Point(currentPoint.x, currentPoint.y));
                        supposedlyMultipleSkeleton.getSkeleton().remove(currentPoint);
                        ArrayList<Point> toBeExcluded = new ArrayList<>();
                        isStopped = true;

                        // cek semua tetangga
                        int[][] sample = new int[3][3];

                        // Create a 3x3 matrix around the Point p
                        Bitmap test = Bitmap.createBitmap(3, 3, Bitmap.Config.ARGB_8888);
                        test.setPixel(0,0, Color.WHITE);

                        int testy = -1;
                        for (int y = 0; y < 3; y++) {
                            int testx = -1;
                            for (int x = 0; x < 3; x++) {
                                try {
                                    test.setPixel(1+testx,1+testy, bitmap.getPixel(p.x+testx, p.y+testy));

                                    if (bitmap.getPixel(p.x+testx, p.y+testy) == Color.BLACK) {
                                        toBeExcluded.add(new Point(p.x+testx, p.y+testy));
                                    }
                                } catch (IllegalArgumentException e) {
                                    test.setPixel(1+testx, 1+testy, Color.RED);
                                }
                                testx++;
                            }
                            testy++;
                        }

                        int ytest2 = -1;
                        for (int y = 0; y < 3; y++) {
                            int xtes2 = -1;
                            for (int x = 0; x < 3; x++) {
                                try {
                                    sample[y][x] = test.getPixel(1+xtes2 , 1+ytest2);
                                    sample[y][x] = (sample[y][x] == Color.WHITE) ? 0 : 1;
                                } catch (IllegalArgumentException e) {
                                    sample[y][x] = 0;
                                }
                                xtes2++;
                            }
                            ytest2++;
                        }

                        for (int j = -1; j < 2; j++) {
                            for (int i = -1; i < 2; i++) {
                                if (sample[j+1][i+1] == 1 /*&& !(i == 0 && j == 0) && !(currentPoint.x+i == prevPoint.x && currentPoint.y+i == prevPoint.y) */&& supposedlyMultipleSkeleton.getSkeleton().indexOf(new Point(currentPoint.x+i, currentPoint.y+j)) != -1) {
                                    skeletonPoints.addAll(getFirstSkeletonFromStartingPoint(supposedlyMultipleSkeleton, bitmap, new Point(currentPoint.x+i, currentPoint.y+j), toBeExcluded).getSkeleton());
                                }
                            }
                        }

                        // panggil rekursif fungsi ini u/ setiap tetangga
                        // sambungin hasilnya
                        break;
                    }
                }

                if (!currentPoint.equals(nextPoint)) {
                    skeletonPoints.add(new Point(currentPoint.x, currentPoint.y));
                    supposedlyMultipleSkeleton.getSkeleton().remove(currentPoint);
                    isStopped = true;
                }
            }
        }


        return new Skeleton(skeletonPoints, new ArrayList<Point>(), new ArrayList<Point>(), supposedlyMultipleSkeleton.width, supposedlyMultipleSkeleton.height);
    }
}
