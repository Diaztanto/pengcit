package com.tanto.SkeletonDetection;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;

public class StructuringElements {
    private static int[][][] endPointStructures = {
            // 0
            {{-1, 0, 0},    // [ x 0 0 ]
            {1, 1, 0},     // [ 1 1 0 ]
            {-1, 0, 0}},   // [ x 0 0 ]

            // 1
            {{-1, 1, -1},   // [ x 1 x ]
            {0, 1, 0},      // [ 0 1 0 ]
            {0, 0, 0}},     // [ 0 0 0 ]

            // 2
            {{0, 0, -1},    // [ 0 0 x ]
            {0, 1, 1},      // [ 0 1 1 ]
            {0, 0, -1}},    // [ 0 0 x ]

            // 3
            {{0, 0, 0},     // [ 0 0 0 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {-1, 1, -1}},   // [ x 1 x ]

            // 4
            {{1, 0, 0},     // [ 1 0 0 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {0, 0, 0}},     // [ 0 0 0 ]

            // 5
            {{0, 0, 1},     // [ 0 0 1 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {0, 0, 0}},     // [ 0 0 0 ]

            // 6
            {{0, 0, 0},     // [ 0 0 0 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {0, 0, 1}},     // [ 0 0 1 ]

            // 7
            {{0, 0, 0},     // [ 0 0 0 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {1, 0, 0}},     // [ 1 0 0 ]

            // 8
            {{0, 0, 0},     // [ 0 0 0 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {0, 0, 0}}     // [ 0 0 0 ]
    };

    private static int[][][] intersectionStructures = {
            // 0
            {{0, 1, 0},     // [ 0 1 0 ]
            {1, 1, 1},      // [ 1 1 1 ]
            {-1, -1, -1}},  // [ x x x ]

            // 1
            {{0, 1, -1},   // [ 0 1 x ]
            {1, 1, -1},    // [ 1 1 x ]
            {0, 1, -1}},   // [ 0 1 x ]

            // 2
            {{-1, -1, -1},   // [ x x x ]
            {1, 1, 1},      // [ 1 1 1 ]
            {0, 1, 0}},     // [ 0 1 0 ]

            // 3
            {{-1, 1, 0},     // [ x 1 0 ]
            {-1, 1, 1},      // [ x 1 1 ]
            {-1, 1, 0}},     // [ x 1 0 ]

            // 4
            {{-1, 1, 0},    // [ x 1 0 ]
            {0, 1, 1},      // [ 0 1 1 ]
            {1, 0, -1}},    // [ 1 0 x ]

            // 5
            {{1, 0, -1},    // [ 1 0 x ]
            {0, 1, 1},      // [ 0 1 1 ]
            {-1, 1, 0}},     // [ x 1 0 ]

            // 6
            {{0, 1, -1},    // [ 0 1 x ]
            {1, 1, 0},      // [ 1 1 0 ]
            {-1, 0, 1}},    // [ x 0 1 ]

            // 7
            {{-1, 0, 1},    // [ x 0 1 ]
            {1, 1, 0},      // [ 1 1 0 ]
            {0, 1, -1}},    // [ 0 1 x ]

            // 8
            {{1, 0, 1},     // [ 1 0 1 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {1, 0, -1}},    // [ 1 0 x ]

            // 9
            {{1, 0, 1},     // [ 1 0 1 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {-1, 0, 1}},    // [ x 0 1 ]

            // 10
            {{-1, 0, 1},    // [ x 0 1 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {1, 0, 1}},     // [ 1 0 1 ]

            // 11
            {{1, 0, -1},    // [ 1 0 x ]
            {0, 1, 0},      // [ 0 1 0 ]
            {1, 0, 1}},     // [ 1 0 1 ]

            // 12
            {{1, 0, 1},     // [ 1 0 1 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {-1, 1, 0}},     // [ x 1 0 ]

            // 12
            {{1, 0, 1},     // [ 1 0 1 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {0, 1, -1}},    // [ 0 1 x ]

            // 13
            {{-1, 1, 0},     // [ x 1 0 ]
            {0, 1, 0},      // [ 0 1 0 ]
            {1, 0, 1}},     // [ 1 0 1 ]

            // 14
            {{0, 1, -1},     // [ 0 1 x ]
            {0, 1, 0},      // [ 0 1 0 ]
            {1, 0, 1}},     // [ 1 0 1 ]

            // 15
            {{-1, 0, 1},     // [ x 0 1 ]
            {1, 1, 0},      // [ 1 1 0 ]
            {0, 0, 1}},     // [ 0 0 1 ]

            // 16
            {{0, 0, 1},     // [ 0 0 1 ]
            {1, 1, 0},      // [ 1 1 0 ]
            {-1, 0, 1}},     // [ x 0 1 ]

            // 17
            {{1, 0, -1},    // [ 1 0 x ]
            {0, 1, 1},      // [ 0 1 1 ]
            {1, 0, 0}},     // [ 1 0 0 ]

            // 19
            {{1, 0, 0},    // [ 1 0 0 ]
            {0, 1, 1},      // [ 0 1 1 ]
            {1, 0, -1}},     // [ 1 0 x ]
    };
    private StructuringElements() {};

    public static boolean match(int[][]sample, String inputId) {
        int matchedPixelCount;
        int idx = 0;
        int[][][] B = getStructure(inputId);

        do {
            matchedPixelCount = 0;
            for (int i = 0; i < B[idx].length; i++) {
                boolean isUnmatchedExist = false;
                for (int j = 0; j < B[idx][i].length; j++) {
                    if (B[idx][i][j] == -1 || sample[i][j] == B[idx][i][j]) {
                        matchedPixelCount++;
                    } else {
                        isUnmatchedExist = true;
                        break;
                    }
                }
                if(isUnmatchedExist) {
                    break;
                }
            }
            idx++;
        } while (idx < B.length && matchedPixelCount != 9);

        return matchedPixelCount == 9;
    }

    public static boolean matchBitmap(Point p, Bitmap img, String inputId) {
        int matchedPixelCount;
        int idx = 0;

        int patternCount;
        switch(inputId) {
            case "endpoint":
                patternCount = endPointStructures.length;
                break;
            case "intersection":
                patternCount = intersectionStructures.length;
                break;
            default:
                patternCount = 0;
        }
        do {
            Bitmap el = getStructureBitmap(idx,inputId);
            matchedPixelCount = 0;
            int yOffset = -1;
            for (int y = 0; y < el.getHeight(); y++) {
                int xOffset = -1;
                boolean isUnmatchedExist = false;
                for (int x = 0; x < el.getWidth(); x++) {
                    if (el.getPixel(1+xOffset, 1+yOffset) == Color.RED || el.getPixel(1+xOffset, 1+yOffset) == img.getPixel(p.x+xOffset, p.y+yOffset)) {
                        matchedPixelCount++;
                    } else {
                        isUnmatchedExist = true;
                        break;
                    }
                    xOffset++;
                }
                if(isUnmatchedExist) {
                    break;
                }
                yOffset++;
            }
            idx++;
        } while (idx < patternCount && matchedPixelCount != 9);

        return matchedPixelCount == 9;
    }

    private static int[][][] getStructure(String id) {
        switch(id) {
            case "endpoint":
                return endPointStructures.clone();
            case "intersection":
                return intersectionStructures.clone();
            default:
                return null;
        }
    }

    public static Bitmap getStructureBitmap(int idx, String structureName) {
        int[][][] B = getStructure(structureName);
        if (idx < 0 || idx > B.length) {
            return Bitmap.createBitmap(3, 3, Bitmap.Config.ARGB_8888);
        }
        Bitmap element = Bitmap.createBitmap(3, 3, Bitmap.Config.ARGB_8888);
        int yOffset = 2;
        for (int i = 0; i < B[idx].length; i++) {
            for (int j = 0; j < B[idx][i].length; j++) {
                if (B[idx][i][j] == 0) {
                    element.setPixel(j, i, Color.WHITE);
                } else if (B[idx][i][j] == -1) {
                    element.setPixel(j, i, Color.RED);
                } else {
                    element.setPixel(j, i, Color.BLACK);
                }
            }
            yOffset--;
        }
        return element;
    }
}
