package com.tanto.SkeletonDetection;

import android.graphics.Point;

import java.util.ArrayList;

public class SkeletonHeuristics {
    public static char recognize (ArrayList<Skeleton> toBeRecognized){


        if (toBeRecognized.size() == 3) {
            return '%';
        } else if (toBeRecognized.size() == 2) {
            if (isSkeletonADot(toBeRecognized.get(0),2)) { // apakah objek pertama titik? y = {j i ; :}
                if (isSkeletonADot(toBeRecognized.get(1),2)) {
                    return ':';
                } else { // j i ;
                    if (isVertical(toBeRecognized.get(1).getEndPoints().get(0), toBeRecognized.get(1).getEndPoints().get(1), 2)) {
                        return 'i';
                    } else { // j & ;
                        if (isVertical(toBeRecognized.get(1).getEndPoints().get(0), toBeRecognized.get(1).getEndPoints().get(1), 20)) {
                            return ';';
                        } else {
                            return 'j';
                        }
                    }
                }
            } else { // n = {" ? ! =}
                if (isHorizontal(toBeRecognized.get(0).getEndPoints().get(0),toBeRecognized.get(0).getEndPoints().get(1), 2)) {
                    return '=';
                } else {
                    if (isSkeletonADot(toBeRecognized.get(1),2)) {
                        if (isVertical(toBeRecognized.get(0).getEndPoints().get(0),toBeRecognized.get(0).getEndPoints().get(1), 2)) {
                            return '!';
                        } else {
                            return '?';
                        }
                    } else {
                        return '"';
                    }
                }
            }
        } else if (toBeRecognized.size() == 1) {
            // MASUK PORTAL A
            switch (toBeRecognized.get(0).getEndPoints().size()) {
                case 0: // 8 B 0 O o D
                    if (toBeRecognized.get(0).getIntersections().size() > 0) { // 8 dan B
                        return '8'; // atau B
                    } else { // 0 O o D
                        double threshold = 0.15;
                        if (isBoundarySquare(toBeRecognized.get(0), threshold)) {
                            return 'o'; //atau O
                        } else {
                            return '0'; // atau D
                        }
                    }
                case 1: // . 6 P @ e g 9
                    if (isSkeletonADot(toBeRecognized.get(0), 2)) {
                        return '.';
                    } else {
                        if (toBeRecognized.get(0).getEndPoints().get(0).y < toBeRecognized.get(0).getIntersections().get(0).y) {
                            return '6';
                        } else {
                            if (isVertical(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getIntersections().get(0), 3)) {
                                return 'P';
                            } else {
                                    if (toBeRecognized.get(0).getEndPoints().get(0).x < toBeRecognized.get(0).getIntersections().get(0).x) { // 9 atau g
                                        return '9'; // atau g
                                    } else {
                                        if (numberOfTransitionFromBeam(0,1, toBeRecognized.get(0), toBeRecognized.get(0).getIntersections().get(0)) >= 4) {
                                            return '@';
                                        } else {
                                            return 'e';
                                        }
                                    }
                                }
                        }
                    }
                case 2: // & A Q R a b p d q
                    // MASUK KE PORTAL D
                    if (toBeRecognized.get(0).getIntersections().size() == 4) {
                        return '&';
                    } else if (toBeRecognized.get(0).getIntersections().size() == 2) {
                        if (isHorizontal(toBeRecognized.get(0).getIntersections().get(0), toBeRecognized.get(0).getIntersections().get(1), 3)) { // A Q R
                            if (!isHorizontal(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getEndPoints().get(1), 3)) {
                                return 'Q';
                            } else {
                                for (Point ip : toBeRecognized.get(0).getIntersections()) {
                                    for (Point ep: toBeRecognized.get(0).getEndPoints()) {
                                        if (isVertical(ip, ep, 3)) {
                                            return 'R';
                                        }
                                    }
                                }
                                return 'A';
                            }
                        } else {
                            for (Point ep1 : toBeRecognized.get(0).getEndPoints()) {
                                for (Point ep2 : toBeRecognized.get(0).getEndPoints()) {
                                    if (ep1.equals(ep2)) {continue;}
                                    if (!isVertical(ep1,ep2, 3) && !isHorizontal(ep1, ep2, 3)) {
                                        return 'a';
                                    }
                                }
                            }
                            for (Point ep : toBeRecognized.get(0).getEndPoints()) {
                                if (isOnRightHalf(toBeRecognized.get(0).getIntersections().get(0), toBeRecognized.get(0).width)) {
                                    if (numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0,toBeRecognized.get(0).height/4)) > 2) {
                                        return 'q';
                                    } else {
                                        return 'd';
                                    }
                                } else {
                                    if (numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0,toBeRecognized.get(0).height/4)) > 2) {
                                        return 'p';
                                    } else {
                                        return 'b';
                                    }
                                }
                            }
                        }
                    } else {
                        // MASUK KE PORTAL E
                        if (isHorizontal(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getEndPoints().get(1), 20)) {
                            // - M U u V v W w ^ _ n
                            boolean epBawahSemua = true;
                            for (Point ep : toBeRecognized.get(0).getEndPoints()) {
                                if (!isOnLowerHalf(ep, toBeRecognized.get(0).height)) {
                                    epBawahSemua = false;
                                    break;
                                }
                            }

                            if (epBawahSemua) {
                                // M _ n
                                int transitions = numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0,toBeRecognized.get(0).height/2));
                                if (transitions == 8) {
                                    return 'M';
                                } else if (transitions == 4) {
                                    return 'n';
                                }
                                return '_';
                            } else {
                                // TODO: - U u V v
                                if (numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0, toBeRecognized.get(0).height*3/4)) > 5) {
                                    return 'W';
                                } else {
                                    if (numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0, toBeRecognized.get(0).height/3)) > 5) {
                                        return 'w';
                                    } else {
                                        if (numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0, toBeRecognized.get(0).height/3)) < 3) {
                                            return '-';
                                        }
                                    }
                                }

                                return 'U'; // bisa u U v V
                            }
                        } else if (isVertical(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getEndPoints().get(1), 20)) {
                            boolean epDiAtasSemua = true;
                            for (Point ep : toBeRecognized.get(0).getEndPoints()) {
                                if (isOnLowerHalf(ep, toBeRecognized.get(0).height)) {
                                    epDiAtasSemua = false;
                                    break;
                                }
                            }
                            if (epDiAtasSemua) {
                                return '\'';
                            } else {
                                if (numberOfTransitionFromBeam(1,0, toBeRecognized.get(0),new Point(toBeRecognized.get(0).width*2/3, 0)) > 7) {
                                    return '3';
                                } else {
                                    if (numberOfTransitionFromBeam(1,0, toBeRecognized.get(0),new Point(toBeRecognized.get(0).width/2, 0)) < 3) {
                                        return 'I'; // TODO: bisa guard, I atau l kecil
                                    } else {
                                        Point middlePixel = shootBeamUntilHit(0, 0, toBeRecognized.get(0), new Point(0, toBeRecognized.get(0).height/2));
                                        if (isGradientPositive(middlePixel, toBeRecognized.get(0).getEndPoints().get(0))) {
                                            return '('; // TODO: bisa semua bentuk kurung buka dan C ( { [ <
                                        } else {
                                            return ')'; // TODO: bisa semua bentuk kurung tutup ) } ] >
                                        }
                                    }
                                }
                            }
                        }

                        if (isGradientPositive(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getEndPoints().get(1))) {
                            if (!isOnLowerHalf(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).height) &&
                                    !isOnLowerHalf(toBeRecognized.get(0).getEndPoints().get(1), toBeRecognized.get(0).height)) {
                                return '\'';
                            } else if (isOnLowerHalf(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).height) &&
                                    isOnLowerHalf(toBeRecognized.get(0).getEndPoints().get(1), toBeRecognized.get(0).height)) {
                                return ',';
                            } else {
                                if (numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0,toBeRecognized.get(0).height/2)) == 6) {
                                    return 'N';
                                } else {
                                    if (numberOfTransitionFromBeam(1,0, toBeRecognized.get(0), toBeRecognized.get(0).getEndPoints().get(1)) >= 2 &&
                                            numberOfTransitionFromBeam(1,1, toBeRecognized.get(0), toBeRecognized.get(0).getEndPoints().get(1)) >= 2) {
                                        return 'G';
                                    } else {
                                        if (numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0,toBeRecognized.get(0).height*3/4)) == 4) {
                                            if (numberOfTransitionFromBeam(0,0, toBeRecognized.get(0),new Point(0,toBeRecognized.get(0).height/4)) == 4) {
                                                return 'S';
                                            } else {
                                                if (numberOfTransitionFromBeam(1,0, toBeRecognized.get(0), new Point(toBeRecognized.get(0).width/2, 0)) == 6) {
                                                    //TODO: 5 s
                                                    return '5'; // bisa jadi s juga
                                                } else {
                                                    return 'J';
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            return '/';
                        }

                        if (!isGradientPositive(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getEndPoints().get(1))
                                && !isHorizontal(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getEndPoints().get(1), 3)
                                && !isVertical(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getEndPoints().get(1), 3)) {

                            if (!isOnLowerHalf(toBeRecognized.get(0).getEndPoints().get(0),toBeRecognized.get(0).height) &&
                                    !isOnLowerHalf(toBeRecognized.get(0).getEndPoints().get(1),toBeRecognized.get(0).height)) {
                                return '`';
                            } else {
                                int transitions = numberOfTransitionFromBeam(1,0,toBeRecognized.get(0),new Point(toBeRecognized.get(0).width/2, 0));
                                if (transitions > 5) {
                                    return '2'; // TODO: 2 Z z
                                } else if (transitions > 3) {
                                    return '7';
                                } else {
                                    if (numberOfTransitionFromBeam(0,0,toBeRecognized.get(0),new Point(0,toBeRecognized.get(0).height/3)) > 3) {
                                        return '1';
                                    } else {
                                        Point p = shootBeamUntilHit(1,0,toBeRecognized.get(0),new Point((toBeRecognized.get(0).width/2)-10,0));

                                        if (!p.equals(-1,-1)) {
                                            if (isOnLowerHalf(p, toBeRecognized.get(0).height)) {
                                                return 'L';
                                            }
                                        }
                                    }
                                }
                            }

                            return '\\';
                        }


                    }
                case 3: // banyak juga
                    // MASUK PORTAL B
                    if (toBeRecognized.get(0).getIntersections().size() >= 2) {
                        return '4';
                    } else {
                        if (isVertical(toBeRecognized.get(0).getEndPoints().get(0),toBeRecognized.get(0).getEndPoints().get(1),10) &&
                                isVertical(toBeRecognized.get(0).getEndPoints().get(2),toBeRecognized.get(0).getEndPoints().get(1),10)) {
                            return 'E';
                        } else {
                            int onBelow = 0;
                            for (int i = 0; i < toBeRecognized.get(0).getEndPoints().size(); i++) {
                                if (isPointOnQuadrant(toBeRecognized.get(0).getEndPoints().get(i),3, toBeRecognized.get(0).width,toBeRecognized.get(0).height) ||
                                        isPointOnQuadrant(toBeRecognized.get(0).getEndPoints().get(i),4, toBeRecognized.get(0).width,toBeRecognized.get(0).height)) {
                                    onBelow++;
                                }
                            }

                            if (onBelow == 2) {
                                return 'h';
                            } else if (onBelow == 3) {
                                return 'm';
                            } else {
                                int onSide = 0;
                                for (int i = 0; i < toBeRecognized.get(0).getEndPoints().size(); i++) {
                                    if (isPointOnQuadrant(toBeRecognized.get(0).getEndPoints().get(i),1, toBeRecognized.get(0).width,toBeRecognized.get(0).height) ||
                                            isPointOnQuadrant(toBeRecognized.get(0).getEndPoints().get(i),4, toBeRecognized.get(0).width,toBeRecognized.get(0).height)) {
                                        onSide++;
                                    }
                                }

                                if (onSide == 2) {
                                    return 'F';
                                } else { // T t Y y r
                                    // here
                                    int sejajarDgnEndpoint = 0;
                                    for (int i = 0; i < toBeRecognized.get(0).getEndPoints().size(); i++) {
                                        if (isHorizontal(toBeRecognized.get(0).getEndPoints().get(i),toBeRecognized.get(0).getIntersections().get(0), 3)){
                                            sejajarDgnEndpoint++;
                                        }
                                    }

                                    if (sejajarDgnEndpoint > 1) {
                                        return 'T';
                                    } else {
                                        boolean adaYangSejajar = false;

                                        if (isHorizontal(toBeRecognized.get(0).getEndPoints().get(0),toBeRecognized.get(0).getEndPoints().get(1), 3)){
                                            adaYangSejajar = true;
                                        }

                                        if (isHorizontal(toBeRecognized.get(0).getEndPoints().get(0),toBeRecognized.get(0).getEndPoints().get(2), 3) && !adaYangSejajar){
                                            adaYangSejajar = true;
                                        }

                                        if (isHorizontal(toBeRecognized.get(0).getEndPoints().get(1),toBeRecognized.get(0).getEndPoints().get(2), 3) && !adaYangSejajar){
                                            adaYangSejajar = true;
                                        }

                                        if (!adaYangSejajar) {
                                            return 't';
                                        } else {
                                            adaYangSejajar = false;

                                            if (isVertical(toBeRecognized.get(0).getEndPoints().get(0),toBeRecognized.get(0).getEndPoints().get(1), 3)){
                                                adaYangSejajar = true;
                                            }

                                            if (isVertical(toBeRecognized.get(0).getEndPoints().get(0),toBeRecognized.get(0).getEndPoints().get(2), 3) && !adaYangSejajar){
                                                adaYangSejajar = true;
                                            }

                                            if (isVertical(toBeRecognized.get(0).getEndPoints().get(1),toBeRecognized.get(0).getEndPoints().get(2), 3) && !adaYangSejajar){
                                                adaYangSejajar = true;
                                            }

                                            if (!adaYangSejajar){
                                                return 'Y';
                                            } else {
                                                sejajarDgnEndpoint = 0;
                                                for (int i = 0; i < toBeRecognized.get(0).getEndPoints().size(); i++) {
                                                    if (isVertical(toBeRecognized.get(0).getEndPoints().get(i),toBeRecognized.get(0).getIntersections().get(0), 3)){
                                                        sejajarDgnEndpoint++;
                                                    }
                                                }

                                                if (sejajarDgnEndpoint > 1) {
                                                    return 'r';
                                                } else {
                                                    return 'y';
                                                }
                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }
                case 4: // $ + H K X f k x
                    // MASUK PORTAL C
                    if (toBeRecognized.get(0).getIntersections().size() > 3) {
                        return '$';
                    } else if (isHorizontal(toBeRecognized.get(0).getIntersections().get(0), toBeRecognized.get(0).getEndPoints().get(1),3)) {
                        return '+'; // bisa + ato f, sementara +
                    } else {
                        if (isHorizontal(toBeRecognized.get(0).getIntersections().get(0), toBeRecognized.get(0).getIntersections().get(1), 3)) {
                            return 'H';
                        } else if (isVertical(toBeRecognized.get(0).getIntersections().get(0), toBeRecognized.get(0).getIntersections().get(1), 3)) {
                            // X x
                            return 'X';
                        }
                        if (isHorizontal(toBeRecognized.get(0).getEndPoints().get(0), toBeRecognized.get(0).getEndPoints().get(1), 3)) {
                            return 'K';
                        } else {
                            return 'k';
                        }
                    }
                case 5:
                case 6:
                    return '*';
                default:
                    return '#';
            }
        } else {
            return ' ';
        }
    }

    public static boolean isEssentiallyDot (Point p1, Point p2, int threshold) {
        return (Math.sqrt(Math.pow((float)p1.x - p2.x, 2) + Math.pow((float)p1.y - p2.y, 2)) < threshold);
    }

    public static boolean isSkeletonADot (Skeleton toBeExamined, int threshold) {
        int decrement = toBeExamined.getSkeleton().size() - toBeExamined.getEndPoints().size();
        return (decrement) <= threshold;
    }

    public static boolean isHorizontal (Point p1, Point p2, int threshold) {
        return (Math.abs(p1.y - p2.y) <= threshold);
    }

    public static boolean isVertical (Point p1, Point p2, int threshold) {
        return (Math.abs(p1.x - p2.x) <= threshold);
    }

    public static boolean isPointOnQuadrant(Point p, int quadrant, int width, int height) {
        switch (quadrant) {
            case 1:
                return (p.x >= (width/2)) && (p.y < (height/2));
            case 2:
                return (p.x < (width/2)) && (p.y < (height/2));
            case 3:
                return (p.x < (width/2)) && (p.y >= (height/2));
            default:
                return (p.x >= (width/2)) && (p.y >= (height/2));
        }
    }

    public static int numberOfTransitionFromBeam(int mode, // mode 0 u/ horizontal, 1 u/ vertikal
                                                 int direction, // 0 u/ bertambah, 1 u/ berkurang
                                                 Skeleton toBeSlicedWithBeam,
                                                 Point startingPoint)

    {
        int transitions = 0;
        Point currentPoint = new Point(startingPoint.x, startingPoint.y);
        boolean currentStatus = (toBeSlicedWithBeam.getSkeleton().indexOf(currentPoint) != -1); // false u/ putih, true u/ hitam

        switch (mode) {
            case 0:
                switch (direction) {
                    case 0:
                        for (int i = 1; currentPoint.x+i < toBeSlicedWithBeam.width; i++) {
                            if (currentStatus) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x+i, currentPoint.y)) == -1) {
                                    transitions++;
                                    currentStatus = false;
                                }
                            } else {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x+i, currentPoint.y)) != -1) {
                                    transitions++;
                                    currentStatus = true;
                                }
                            }
                        }
                        break;
                    default:
                        for (int i = 1; currentPoint.x-i >= 0; i++) {
                            if (currentStatus) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x-i, currentPoint.y)) == -1) {
                                    transitions++;
                                    currentStatus = false;
                                }
                            } else {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x-i, currentPoint.y)) != -1) {
                                    transitions++;
                                    currentStatus = true;
                                }
                            }
                        }
                        break;
                }
                break;
            default:
                switch (direction) {
                    case 0:
                        for (int i = 1; currentPoint.y+i < toBeSlicedWithBeam.height; i++) {
                            if (currentStatus) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x, currentPoint.y+i)) == -1) {
                                    transitions++;
                                    currentStatus = false;
                                }
                            } else {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x, currentPoint.y+i)) != -1) {
                                    transitions++;
                                    currentStatus = true;
                                }
                            }
                        }
                        break;
                    default:
                        for (int i = 1; currentPoint.y-i >= 0; i++) {
                            if (currentStatus) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x, currentPoint.y-i)) == -1) {
                                    transitions++;
                                    currentStatus = false;
                                }
                            } else {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x, currentPoint.y-i)) != -1) {
                                    transitions++;
                                    currentStatus = !true;
                                }
                            }
                        }
                        break;
                }
                break;
        }

        return transitions;
    }

    public static Point shootBeamUntilHit (int mode, // mode 0 u/ horizontal, 1 u/ vertikal
                                                 int direction, // 0 u/ bertambah, 1 u/ berkurang
                                                 Skeleton toBeSlicedWithBeam,
                                                 Point startingPoint)

    {
        int transitions = 0;
        Point currentPoint = new Point(startingPoint.x, startingPoint.y);
        boolean currentStatus = (toBeSlicedWithBeam.getSkeleton().indexOf(currentPoint) != -1); // false u/ putih, true u/ hitam
        boolean readyToBreak = !currentStatus;
        int i = 1;

        switch (mode) {
            case 0:
                switch (direction) {
                    case 0:
                        for (i = 1; currentPoint.x+i < toBeSlicedWithBeam.width; i++) {
                            if (currentStatus && !readyToBreak) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x+i, currentPoint.y)) == -1) {
                                    currentStatus = !currentStatus;
                                    readyToBreak = !readyToBreak;
                                }
                            } else if (!currentStatus && readyToBreak) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x+i, currentPoint.y)) != -1) {
                                    currentStatus = !currentStatus;
                                    break;
                                }
                            }
                        }
                        break;
                    default:
                        for (i = 1; currentPoint.x-i >= 0; i++) {
                            if (currentStatus && !readyToBreak) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x-i, currentPoint.y)) == -1) {
                                    currentStatus = !currentStatus;
                                    readyToBreak = !readyToBreak;
                                }
                            } else if (!currentStatus && readyToBreak) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x-i, currentPoint.y)) != -1) {
                                    currentStatus = !currentStatus;
                                    break;
                                }
                            }
                        }
                        break;
                }

                if (currentStatus && readyToBreak) {
                    return new Point(currentPoint.x+i, currentPoint.y);
                } else {
                    return  new Point(-1, -1);
                }

            default:
                switch (direction) {
                    case 0:
                        for (i = 1; currentPoint.y+i < toBeSlicedWithBeam.height; i++) {
                            if (currentStatus && !readyToBreak) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x, currentPoint.y+i)) == -1) {
                                    currentStatus = !currentStatus;
                                    readyToBreak = !readyToBreak;
                                }
                            } else if (!currentStatus && readyToBreak) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x, currentPoint.y+i)) != -1) {
                                    currentStatus = !currentStatus;
                                    break;
                                }
                            }
                        }
                        break;
                    default:
                        for (i = 1; currentPoint.y+i < toBeSlicedWithBeam.height; i++) {
                            if (currentStatus && !readyToBreak) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x, currentPoint.y-i)) == -1) {
                                    currentStatus = !currentStatus;
                                    readyToBreak = !readyToBreak;
                                }
                            } else if (!currentStatus && readyToBreak) {
                                if (toBeSlicedWithBeam.getSkeleton().indexOf(new Point(currentPoint.x, currentPoint.y-i)) != -1) {
                                    currentStatus = !currentStatus;
                                    break;
                                }
                            }
                        }
                        break;
                }

                if (currentStatus && readyToBreak) {
                    return new Point(currentPoint.x, currentPoint.y+i);
                } else {
                    return  new Point(-1, -1);
                }
        }
    }

    public static boolean isGradientPositive(Point p1, Point p2) {
        // 0,0 ada di kiri atas bitmap
        return ((p1.y < p2.y && p1.x > p2.x) || (p2.y < p1.y && p2.x > p1.x));
    }

    public static boolean isOnLowerHalf(Point p1, int height) {
        return p1.y > (height/2);
    }

    public static boolean isOnRightHalf(Point p1, int width) {
        return p1.x > (width/2);
    }

    public static double getBoundaryWidthOnHeightRatio(Skeleton sk) {
        int xMin = Integer.MAX_VALUE;
        int xMax = 0;
        int yMin = Integer.MAX_VALUE;
        int yMax = 0;

        for (Point p : sk.getSkeleton()) {
            xMin = (p.x < xMin) ? p.x : xMin;
            xMax = (p.x > xMax) ? p.x : xMax;
            yMin = (p.y < yMin) ? p.y : yMin;
            yMax = (p.y > yMax) ? p.y : yMax;
        }

        double ratio = (float)Math.abs(xMax - xMin) / Math.abs(yMax - yMin);
        return ratio;
    }

    public static boolean isBoundarySquare(Skeleton sk, double threshold) {
        double ratio = getBoundaryWidthOnHeightRatio(sk);
        return (ratio <= 1 + threshold) && (ratio >= 1 - threshold);
    }
}
