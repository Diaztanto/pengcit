package com.tanto.SkeletonDetection;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import java.util.ArrayList;

public class Skeleton {
    public int length;
    private ArrayList<Point> skeleton;
    private ArrayList<Point> endPoints;
    private ArrayList<Point> intersections;
    public int width;
    public int height;
    private Boundary objectBoundary;

    public Skeleton(ArrayList<Point> skeleton, ArrayList<Point> endPoints, ArrayList<Point> intersections) {
        this.skeleton = (ArrayList<Point>) skeleton.clone();
        this.endPoints= (ArrayList<Point>) endPoints.clone();
        this.intersections = (ArrayList<Point>) intersections.clone();

        int xMin = Integer.MAX_VALUE;
        int xMax = 0;
        int yMin = Integer.MAX_VALUE;
        int yMax = 0;

        for (Point p : this.skeleton) {
            xMin = (p.x < xMin) ? p.x : xMin;
            xMax = (p.x > xMax) ? p.x : xMax;
            yMin = (p.y < yMin) ? p.y : yMin;
            yMax = (p.y > yMax) ? p.y : yMax;
        }
        objectBoundary = new Boundary(xMin, yMin, xMax, yMax);
        length = skeleton.size();
        width = 0;
        height = 0;
    }

    public Skeleton(ArrayList<Point> skeleton, ArrayList<Point> endPoints, ArrayList<Point> intersections, int picWidth, int picHeight) {
        this.skeleton = (ArrayList<Point>) skeleton.clone();
        this.endPoints= (ArrayList<Point>) endPoints.clone();
        this.intersections = (ArrayList<Point>) intersections.clone();
        length = skeleton.size();
        width = picWidth;
        height = picHeight;

        int xMin = Integer.MAX_VALUE;
        int xMax = 0;
        int yMin = Integer.MAX_VALUE;
        int yMax = 0;

        for (Point p : this.skeleton) {
            xMin = (p.x < xMin) ? p.x : xMin;
            xMax = (p.x > xMax) ? p.x : xMax;
            yMin = (p.y < yMin) ? p.y : yMin;
            yMax = (p.y > yMax) ? p.y : yMax;
        }
        objectBoundary = new Boundary(xMin, yMin, xMax, yMax);
    }

    public ArrayList<Point> getSkeleton() {
        return skeleton;
    }

    public ArrayList<Point> getEndPoints() {
        return endPoints;
    }

    public ArrayList<Point> getIntersections() {
        return intersections;
    }

    public Bitmap drawOver(Bitmap img) {
        Bitmap newImg = img.copy(Bitmap.Config.ARGB_8888, true);
        int skeletonColor = Color.YELLOW;
        int endColor = Color.RED;
        int branchColor = Color.BLUE;

        for (Point p : skeleton) {
            try {
                newImg.setPixel(p.x, p.y, skeletonColor);
            } catch (IllegalArgumentException e) {
                continue;
            }
        }
        for (Point p : endPoints) {
            try {
                newImg.setPixel(p.x, p.y, endColor);
            } catch (IllegalArgumentException e) {
                continue;
            }
        }
        for (Point p : intersections) {
            try {
                newImg.setPixel(p.x, p.y, branchColor);
            } catch (IllegalArgumentException e) {
                continue;
            }
        }
        return newImg;
    }

    public Point getObjectOriginPoint() {
        return objectBoundary.getOriginPoint();
    }

    public double getBoundaryWidthHeightRatio() {
        return objectBoundary.getWidthHeightRatio();
    }

    private class Boundary {
        int xMin;
        int yMin;
        int xMax;
        int yMax;
        public Boundary (int _xMin, int _yMin, int _xMax, int _yMax) {
            xMin = _xMin;
            yMin = _yMin;
            xMax = _xMax;
            yMax = _yMax;
        }

        public Point getOriginPoint() {
            int xOrigin = xMin + Math.abs(xMax - xMin)/2;
            int yOrigin = yMin + Math.abs(yMax - yMin)/2;
            return new Point(xOrigin, yOrigin);
        }

        public double getWidthHeightRatio() {
            double ratio = (float) Math.abs(xMax - xMin) / Math.abs(yMax - yMin);
            if (Double.isInfinite(ratio)) {
                return Double.MAX_VALUE;
            } else if (Double.isNaN(ratio)) {
                return 0;
            }
            return  ratio;
        }
    }
}
